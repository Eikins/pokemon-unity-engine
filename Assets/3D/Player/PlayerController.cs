﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Controls;

[RequireComponent(typeof(CharacterController))]
public class PlayerController : MonoBehaviour, PlayerControls.IOverworldActions
{
    private PlayerControls inputs;
    

    [Header("Camera Settings")]
    [SerializeField] private new Camera camera;
    [SerializeField] private CinemachineFreeLook cinemachineFreeLook;
    [SerializeField] [Range(0, 180)] private float baseFieldOfView = 40f;
    [SerializeField] [Range(0, 180)] private float sprintFieldOfView = 50f;
    [SerializeField] private float cameraHorizontalSpeed = 0.2f;
    [SerializeField] private float cameraVerticalSpeed = 0.4f;

    [Header("Physics Settings")]
    [SerializeField] private float walkSpeed = 10;
    [SerializeField] private float sprintSpeed = 15;
    [SerializeField] private float rotateSpeed = 50;
    [SerializeField] private float snapDistance = 1f;
    [SerializeField] private float snapOffset = 0.05f;

    private CharacterController controller;
    private Transform modelTransform;
    private Animator animator;

    private Vector3 moveDirection;

    private Vector2 moveInputValue;
    private Vector2 cameraInputValue;
    private bool sprintInputValue;
    private bool interact;

    public bool Controllable { get; set; }
    public bool Interact { 
        get {
            var temp = interact;
            interact = false;
            return temp;
        } 
    }
    public Transform ModelTransform => modelTransform;

    private void Start()
    {
        controller = GetComponent<CharacterController>();
        modelTransform = transform.GetChild(0);
        animator = modelTransform.GetComponent<Animator>();
        Cursor.lockState = CursorLockMode.Locked;
        inputs = new PlayerControls();
        inputs.Overworld.SetCallbacks(this);
        inputs.Overworld.Enable();
        CinemachineCore.GetInputAxis = GetCameraAxis;
        Controllable = true;
    }

    public void OnInteract(InputAction.CallbackContext context) {
        interact = context.started;
    }

    public void OnMove(InputAction.CallbackContext context) {
        moveInputValue = context.ReadValue<Vector2>();
    }

    public void OnSprint(InputAction.CallbackContext context) {
        sprintInputValue = context.performed;
    }

    public void OnCamera(InputAction.CallbackContext context) {
        cameraInputValue = context.ReadValue<Vector2>();
    }

    public float GetCameraAxis(string axisName){
        if (axisName == "Horizontal")
        {
            return cameraInputValue.x * cameraHorizontalSpeed;
        }
        else if (axisName == "Vertical")
        {
            return cameraInputValue.y * cameraVerticalSpeed;
        }

        return 0;
    }

    private void Update()
    {
        if(Controllable) {
            UpdateController();
        } else {
            moveDirection = Vector2.zero;
        }
        UpdateAnimator();
        UpdateCamera();
    }

    private void UpdateController() {

        float y = moveDirection.y;
        var speed = sprintInputValue ? sprintSpeed : walkSpeed; 
        moveDirection = (camera.transform.forward * moveInputValue.y + camera.transform.right * moveInputValue.x) * speed;
        moveDirection.y = y;

        if(controller.isGrounded) {
            moveDirection.y = 0f;
        } else {
            moveDirection += Physics.gravity * Time.deltaTime;
        }

        controller.Move(moveDirection * Time.deltaTime);

     
        RaycastHit hitInfo;
        if (Physics.Raycast(new Ray(transform.position, Vector3.down), out hitInfo, snapDistance)) {
            transform.position = new Vector3(transform.position.x, hitInfo.point.y + snapOffset, transform.position.z);
        }

        if(moveInputValue != Vector2.zero) {
            var newRotation = Quaternion.LookRotation(new Vector3(moveDirection.x, 0f, moveDirection.z));
            modelTransform.rotation = Quaternion.Slerp(modelTransform.rotation, newRotation, rotateSpeed * Time.deltaTime);
        }
    }

    private void UpdateAnimator() {
        float speed = new Vector2(moveDirection.x, moveDirection.z).magnitude;
        animator.SetFloat("Speed", speed);
        animator.SetBool("OnGround", controller.isGrounded);
    }

    private void UpdateCamera() {
        if(sprintInputValue) {
            cinemachineFreeLook.m_Lens.FieldOfView = Mathf.Lerp(cinemachineFreeLook.m_Lens.FieldOfView, sprintFieldOfView, 3 * Time.deltaTime);
        } else {
            cinemachineFreeLook.m_Lens.FieldOfView = Mathf.Lerp(cinemachineFreeLook.m_Lens.FieldOfView, baseFieldOfView, 3 * Time.deltaTime);
        }
    }
}
