// GENERATED AUTOMATICALLY FROM 'Assets/3D/Player/PlayerControls.inputactions'

using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

public class PlayerControls : IInputActionCollection
{
    private InputActionAsset asset;
    public PlayerControls()
    {
        asset = InputActionAsset.FromJson(@"{
    ""name"": ""PlayerControls"",
    ""maps"": [
        {
            ""name"": ""Overworld"",
            ""id"": ""dbba999d-643a-47c3-90ae-ae3e0d1ede88"",
            ""actions"": [
                {
                    ""name"": ""Interact"",
                    ""type"": ""Button"",
                    ""id"": ""42bb9efe-3158-4905-ab63-9bb94cbdfb82"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Move"",
                    ""type"": ""Value"",
                    ""id"": ""fe482a06-0f3c-4813-8922-028fbebc97eb"",
                    ""expectedControlType"": ""Vector2"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Sprint"",
                    ""type"": ""Button"",
                    ""id"": ""186319ec-6c14-4583-8935-106b0b6d0d01"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Camera"",
                    ""type"": ""Value"",
                    ""id"": ""17378788-cf90-4688-b0e9-9c656151da30"",
                    ""expectedControlType"": ""Vector2"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""eab42f11-1241-4cca-a2fc-c2c1f1cb67f4"",
                    ""path"": ""<Keyboard>/space"",
                    ""interactions"": ""Tap"",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Interact"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""da3e0017-24cf-4c84-a2da-8cb7999baf99"",
                    ""path"": ""<Gamepad>/buttonEast"",
                    ""interactions"": ""Tap"",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Interact"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""0c73175d-923b-4c0b-bbdf-52164eddd75d"",
                    ""path"": ""<HID::Saitek PLC Saitek P2600 Rumble Force Pad Joystick>/button3"",
                    ""interactions"": ""Tap"",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Interact"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""WASD"",
                    ""id"": ""da8ef64c-fd3f-4484-920b-ba3697b820de"",
                    ""path"": ""2DVector"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""up"",
                    ""id"": ""8403391d-6063-4b22-b399-d57c678adf9a"",
                    ""path"": ""<Keyboard>/w"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""down"",
                    ""id"": ""af9aaa0d-50f1-4356-95a8-62c9d36bedb8"",
                    ""path"": ""<Keyboard>/s"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""left"",
                    ""id"": ""852bc5a0-b053-4dcd-9d60-4fe3c6229553"",
                    ""path"": ""<Keyboard>/a"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""right"",
                    ""id"": ""5e141430-d671-4564-b818-d1be09d7f5eb"",
                    ""path"": ""<Keyboard>/d"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": """",
                    ""id"": ""5ba4a923-0ba8-4e47-a6cf-a02aebbe52bd"",
                    ""path"": ""<Gamepad>/leftStick"",
                    ""interactions"": """",
                    ""processors"": ""NormalizeVector2"",
                    ""groups"": """",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""b2c6a8ea-a7b4-470e-b39b-d7d0c3ec539c"",
                    ""path"": ""<HID::Saitek PLC Saitek P2600 Rumble Force Pad Joystick>/Stick"",
                    ""interactions"": """",
                    ""processors"": ""InvertVector2(invertX=false)"",
                    ""groups"": """",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""22078382-7d14-4700-940f-9ae5d4e100ce"",
                    ""path"": ""<Keyboard>/leftShift"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Sprint"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""b6e5c954-dd16-42eb-9fb8-a42ae50d3293"",
                    ""path"": ""<Gamepad>/buttonSouth"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Sprint"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""dd7663cb-7155-4af4-9fe1-11fcdc7468f7"",
                    ""path"": ""<HID::Saitek PLC Saitek P2600 Rumble Force Pad Joystick>/button2"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Sprint"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""fe8af140-4132-4af2-abea-f443b6b520d5"",
                    ""path"": ""<Gamepad>/rightStick"",
                    ""interactions"": """",
                    ""processors"": ""NormalizeVector2"",
                    ""groups"": """",
                    ""action"": ""Camera"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""arrows"",
                    ""id"": ""ca9d99ea-88e2-4072-944e-41e51c1f999a"",
                    ""path"": ""2DVector"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Camera"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""up"",
                    ""id"": ""082b707c-1909-450a-948e-3a7aaa56b468"",
                    ""path"": ""<Keyboard>/upArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Camera"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""down"",
                    ""id"": ""64a2a313-5ce4-4f45-a885-06ca56140f6b"",
                    ""path"": ""<Keyboard>/downArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Camera"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""left"",
                    ""id"": ""a842608b-03c4-4537-a1b0-78e1a3835f1f"",
                    ""path"": ""<Keyboard>/leftArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Camera"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""right"",
                    ""id"": ""cc84eca5-c21d-4bc5-a5de-449a22d56464"",
                    ""path"": ""<Keyboard>/rightArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Camera"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": """",
                    ""id"": ""74cc93d5-6b2d-4a65-b7f8-d3b72a661498"",
                    ""path"": ""<HID::Saitek PLC Saitek P2600 Rumble Force Pad Joystick>/dpad"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Camera"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        },
        {
            ""name"": ""Battle"",
            ""id"": ""e8459a23-5fdc-449b-ab70-46dd6688250e"",
            ""actions"": [
                {
                    ""name"": ""Continue"",
                    ""type"": ""Button"",
                    ""id"": ""2f0d5c63-edac-4a4f-a6d1-5006352ab83e"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Back"",
                    ""type"": ""Button"",
                    ""id"": ""a19e5329-df35-468f-a6df-95c0d3fea6dd"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Down"",
                    ""type"": ""Button"",
                    ""id"": ""d8214acf-f5f7-4d99-9acb-f58812e39759"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Up"",
                    ""type"": ""Button"",
                    ""id"": ""0af4ae0c-5629-4318-b6a6-9373c8c3680b"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""fdede75f-22e5-4ca4-9a14-04d7db9af115"",
                    ""path"": ""<Keyboard>/space"",
                    ""interactions"": ""Tap"",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Continue"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""6be37658-a753-4661-be3e-54a7593b2682"",
                    ""path"": ""<Keyboard>/enter"",
                    ""interactions"": ""Tap"",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Continue"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""47db3931-721e-4317-a4d0-db831959a98e"",
                    ""path"": ""<Keyboard>/downArrow"",
                    ""interactions"": ""Tap"",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Down"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""ac47a2bc-f151-4c37-8a5c-c6c842bcc947"",
                    ""path"": ""<Gamepad>/leftStick/down"",
                    ""interactions"": ""Tap"",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Down"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""d66bcc69-ee3f-4cc9-846b-0e57f8fa7e06"",
                    ""path"": ""<Keyboard>/upArrow"",
                    ""interactions"": ""Tap"",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Up"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""a83503ae-cd04-41e4-aba6-8ce4b6821e34"",
                    ""path"": ""<Gamepad>/leftStick/up"",
                    ""interactions"": ""Tap"",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Up"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""089b54a1-6bcd-49ec-b232-35818827924b"",
                    ""path"": ""<Keyboard>/escape"",
                    ""interactions"": ""Tap"",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Back"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""c027878f-f8a3-45f6-8dd9-502b9099a0c4"",
                    ""path"": ""<Gamepad>/buttonSouth"",
                    ""interactions"": ""Tap"",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Back"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        }
    ],
    ""controlSchemes"": []
}");
        // Overworld
        m_Overworld = asset.GetActionMap("Overworld");
        m_Overworld_Interact = m_Overworld.GetAction("Interact");
        m_Overworld_Move = m_Overworld.GetAction("Move");
        m_Overworld_Sprint = m_Overworld.GetAction("Sprint");
        m_Overworld_Camera = m_Overworld.GetAction("Camera");
        // Battle
        m_Battle = asset.GetActionMap("Battle");
        m_Battle_Continue = m_Battle.GetAction("Continue");
        m_Battle_Back = m_Battle.GetAction("Back");
        m_Battle_Down = m_Battle.GetAction("Down");
        m_Battle_Up = m_Battle.GetAction("Up");
    }

    ~PlayerControls()
    {
        UnityEngine.Object.Destroy(asset);
    }

    public InputBinding? bindingMask
    {
        get => asset.bindingMask;
        set => asset.bindingMask = value;
    }

    public ReadOnlyArray<InputDevice>? devices
    {
        get => asset.devices;
        set => asset.devices = value;
    }

    public ReadOnlyArray<InputControlScheme> controlSchemes => asset.controlSchemes;

    public bool Contains(InputAction action)
    {
        return asset.Contains(action);
    }

    public IEnumerator<InputAction> GetEnumerator()
    {
        return asset.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    public void Enable()
    {
        asset.Enable();
    }

    public void Disable()
    {
        asset.Disable();
    }

    // Overworld
    private readonly InputActionMap m_Overworld;
    private IOverworldActions m_OverworldActionsCallbackInterface;
    private readonly InputAction m_Overworld_Interact;
    private readonly InputAction m_Overworld_Move;
    private readonly InputAction m_Overworld_Sprint;
    private readonly InputAction m_Overworld_Camera;
    public struct OverworldActions
    {
        private PlayerControls m_Wrapper;
        public OverworldActions(PlayerControls wrapper) { m_Wrapper = wrapper; }
        public InputAction @Interact => m_Wrapper.m_Overworld_Interact;
        public InputAction @Move => m_Wrapper.m_Overworld_Move;
        public InputAction @Sprint => m_Wrapper.m_Overworld_Sprint;
        public InputAction @Camera => m_Wrapper.m_Overworld_Camera;
        public InputActionMap Get() { return m_Wrapper.m_Overworld; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(OverworldActions set) { return set.Get(); }
        public void SetCallbacks(IOverworldActions instance)
        {
            if (m_Wrapper.m_OverworldActionsCallbackInterface != null)
            {
                Interact.started -= m_Wrapper.m_OverworldActionsCallbackInterface.OnInteract;
                Interact.performed -= m_Wrapper.m_OverworldActionsCallbackInterface.OnInteract;
                Interact.canceled -= m_Wrapper.m_OverworldActionsCallbackInterface.OnInteract;
                Move.started -= m_Wrapper.m_OverworldActionsCallbackInterface.OnMove;
                Move.performed -= m_Wrapper.m_OverworldActionsCallbackInterface.OnMove;
                Move.canceled -= m_Wrapper.m_OverworldActionsCallbackInterface.OnMove;
                Sprint.started -= m_Wrapper.m_OverworldActionsCallbackInterface.OnSprint;
                Sprint.performed -= m_Wrapper.m_OverworldActionsCallbackInterface.OnSprint;
                Sprint.canceled -= m_Wrapper.m_OverworldActionsCallbackInterface.OnSprint;
                Camera.started -= m_Wrapper.m_OverworldActionsCallbackInterface.OnCamera;
                Camera.performed -= m_Wrapper.m_OverworldActionsCallbackInterface.OnCamera;
                Camera.canceled -= m_Wrapper.m_OverworldActionsCallbackInterface.OnCamera;
            }
            m_Wrapper.m_OverworldActionsCallbackInterface = instance;
            if (instance != null)
            {
                Interact.started += instance.OnInteract;
                Interact.performed += instance.OnInteract;
                Interact.canceled += instance.OnInteract;
                Move.started += instance.OnMove;
                Move.performed += instance.OnMove;
                Move.canceled += instance.OnMove;
                Sprint.started += instance.OnSprint;
                Sprint.performed += instance.OnSprint;
                Sprint.canceled += instance.OnSprint;
                Camera.started += instance.OnCamera;
                Camera.performed += instance.OnCamera;
                Camera.canceled += instance.OnCamera;
            }
        }
    }
    public OverworldActions @Overworld => new OverworldActions(this);

    // Battle
    private readonly InputActionMap m_Battle;
    private IBattleActions m_BattleActionsCallbackInterface;
    private readonly InputAction m_Battle_Continue;
    private readonly InputAction m_Battle_Back;
    private readonly InputAction m_Battle_Down;
    private readonly InputAction m_Battle_Up;
    public struct BattleActions
    {
        private PlayerControls m_Wrapper;
        public BattleActions(PlayerControls wrapper) { m_Wrapper = wrapper; }
        public InputAction @Continue => m_Wrapper.m_Battle_Continue;
        public InputAction @Back => m_Wrapper.m_Battle_Back;
        public InputAction @Down => m_Wrapper.m_Battle_Down;
        public InputAction @Up => m_Wrapper.m_Battle_Up;
        public InputActionMap Get() { return m_Wrapper.m_Battle; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(BattleActions set) { return set.Get(); }
        public void SetCallbacks(IBattleActions instance)
        {
            if (m_Wrapper.m_BattleActionsCallbackInterface != null)
            {
                Continue.started -= m_Wrapper.m_BattleActionsCallbackInterface.OnContinue;
                Continue.performed -= m_Wrapper.m_BattleActionsCallbackInterface.OnContinue;
                Continue.canceled -= m_Wrapper.m_BattleActionsCallbackInterface.OnContinue;
                Back.started -= m_Wrapper.m_BattleActionsCallbackInterface.OnBack;
                Back.performed -= m_Wrapper.m_BattleActionsCallbackInterface.OnBack;
                Back.canceled -= m_Wrapper.m_BattleActionsCallbackInterface.OnBack;
                Down.started -= m_Wrapper.m_BattleActionsCallbackInterface.OnDown;
                Down.performed -= m_Wrapper.m_BattleActionsCallbackInterface.OnDown;
                Down.canceled -= m_Wrapper.m_BattleActionsCallbackInterface.OnDown;
                Up.started -= m_Wrapper.m_BattleActionsCallbackInterface.OnUp;
                Up.performed -= m_Wrapper.m_BattleActionsCallbackInterface.OnUp;
                Up.canceled -= m_Wrapper.m_BattleActionsCallbackInterface.OnUp;
            }
            m_Wrapper.m_BattleActionsCallbackInterface = instance;
            if (instance != null)
            {
                Continue.started += instance.OnContinue;
                Continue.performed += instance.OnContinue;
                Continue.canceled += instance.OnContinue;
                Back.started += instance.OnBack;
                Back.performed += instance.OnBack;
                Back.canceled += instance.OnBack;
                Down.started += instance.OnDown;
                Down.performed += instance.OnDown;
                Down.canceled += instance.OnDown;
                Up.started += instance.OnUp;
                Up.performed += instance.OnUp;
                Up.canceled += instance.OnUp;
            }
        }
    }
    public BattleActions @Battle => new BattleActions(this);
    public interface IOverworldActions
    {
        void OnInteract(InputAction.CallbackContext context);
        void OnMove(InputAction.CallbackContext context);
        void OnSprint(InputAction.CallbackContext context);
        void OnCamera(InputAction.CallbackContext context);
    }
    public interface IBattleActions
    {
        void OnContinue(InputAction.CallbackContext context);
        void OnBack(InputAction.CallbackContext context);
        void OnDown(InputAction.CallbackContext context);
        void OnUp(InputAction.CallbackContext context);
    }
}
