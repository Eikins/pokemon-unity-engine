﻿Shader "Pokemon/Cel Shading"
{
    Properties
    {
        _Color ("Color", Color) = (1,1,1,1)
        _MainTex ("Albedo (RGB)", 2D) = "white" {}
        _MiddleColor ("Middle Color", Range(0, 1)) = 0.5
        _EdgeSmoothness ("Edge Smoothness", Range(0,0.5)) = 0.025
        _LowThreshold ("Low Threshold", Range(0, 1)) = 0.1
        _HighThreshold ("High Threshold", Range(0, 1)) = 0.9
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 200

        CGPROGRAM
        // Physically based Standard lighting model, and enable shadows on all light types
        #pragma surface surf NPCCelShading

        // Use shader model 3.0 target, to get nicer looking lighting
        #pragma target 3.0

        half _EdgeSmoothness;
        half _LowThreshold;
        half _HighThreshold;
        half _MiddleColor;

        half4 LightingNPCCelShading(SurfaceOutput output, half3 lightDirection, half atten) {
            half normalDotLight = dot(output.Normal, lightDirection);

            if(normalDotLight < _LowThreshold - _EdgeSmoothness) {
                normalDotLight = 0.1;
            } else if(normalDotLight < _LowThreshold + _EdgeSmoothness) {
                normalDotLight = smoothstep(_LowThreshold - _EdgeSmoothness, _LowThreshold + _EdgeSmoothness, normalDotLight) * (_MiddleColor - 0.1) + 0.1;
            } else if(normalDotLight < _HighThreshold - _EdgeSmoothness) {
                normalDotLight = _MiddleColor;
            } else if(normalDotLight < _HighThreshold + _EdgeSmoothness) {
                normalDotLight = smoothstep(_HighThreshold - _EdgeSmoothness, _HighThreshold + _EdgeSmoothness, normalDotLight) * (0.9 - _MiddleColor) + _MiddleColor;
            } else {
                normalDotLight = 0.9;
            }

            normalDotLight = (normalDotLight - 0.5) / 2 + 0.5;

            half4 color;
            color.rgb = output.Albedo * _LightColor0.rgb * (normalDotLight * atten * 2);
            color.a = output.Alpha;
            return color;
        }

        sampler2D _MainTex;

        struct Input
        {
            float2 uv_MainTex;
        };


        fixed4 _Color;

        // Add instancing support for this shader. You need to check 'Enable Instancing' on materials that use the shader.
        // See https://docs.unity3d.com/Manual/GPUInstancing.html for more information about instancing.
        // #pragma instancing_options assumeuniformscaling
        UNITY_INSTANCING_BUFFER_START(Props)
            // put more per-instance properties here
        UNITY_INSTANCING_BUFFER_END(Props)

        void surf (Input IN, inout SurfaceOutput o)
        {
            // Albedo comes from a texture tinted by color
            fixed4 c = tex2D (_MainTex, IN.uv_MainTex) * _Color;
            o.Albedo = c.rgb;
            o.Alpha = c.a;

        }
        ENDCG
    }
    FallBack "Diffuse"
}
