﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using PokemonEngine.BattleSystem;
using UnityEngine.AI;

public class NPCDialog : MonoBehaviour
{

    public float textSpeed = 30;
    public RectTransform textBox;
    public GameObject interactTooltip;
    
    [TextArea]
    public string[] dialog;

    public Transform[] dialogPoints;

    private PlayerController playerController;

    private NavMeshAgent agent;
    private Animator animator;
    

    void Start() {
        agent = GetComponent<NavMeshAgent>();
        animator = GetComponentInChildren<Animator>();
    }

    private void OnTriggerStay(Collider collider) {
        if(playerController == null && collider.CompareTag("Player")) {
            playerController = collider.gameObject.GetComponent<PlayerController>();
            if (playerController.Interact) {
                interactTooltip.SetActive(false);
                StartCoroutine(ShowDialog());
            } else {
                playerController = null;
            }
        }

    }

    void Update() {
        if(agent != null) {
            animator.SetFloat("Speed", agent.velocity.magnitude);
        }
    }

    private void OnTriggerEnter(Collider collider) {
       if(playerController == null && collider.CompareTag("Player")) {
            interactTooltip.SetActive(true);
        }
    }

    private void OnTriggerExit(Collider collider) {
        if(playerController == null && collider.CompareTag("Player")) {
            interactTooltip.SetActive(false);
        }
    }

    private IEnumerator ShowDialog() {
        playerController.Controllable = false;
        float time = 0.5f;
        var look = transform.position - playerController.ModelTransform.position;
        look.y = 0;
        var playerRotation = Quaternion.LookRotation(look, Vector3.up);
        var npcRotation = Quaternion.LookRotation(-look, Vector3.up);
        while(time > 0) {
            transform.rotation = Quaternion.Slerp(transform.rotation, npcRotation, Time.deltaTime * 10);
            playerController.ModelTransform.rotation = Quaternion.Slerp(playerController.ModelTransform.rotation, playerRotation, Time.deltaTime * 10);
            time -= Time.deltaTime;
            yield return null;
        }
        playerController.ModelTransform.rotation = playerRotation;
        transform.rotation = npcRotation;
        int pointIndex = 0;
        foreach (var line in dialog) {
            if(line.Equals("::MoveNext")) {
                if(agent != null) {
                    yield return RotateToward(dialogPoints[pointIndex].position);
                    agent.destination = dialogPoints[pointIndex].position;
                    pointIndex++;
                    yield return new WaitUntil(PathComplete);
                }
            } else {
                yield return ShowText(line);
            }
        }
        var battle = GetComponent<BattleHandler>();
        if(battle != null) {
            playerController = null;
            battle.StartBattle();
        } else {
            playerController.Controllable = true;
            interactTooltip.SetActive(true);
            playerController = null;
        }
        
    }

    private IEnumerator RotateToward(Vector3 position) {
        float time = 0.5f;
        var look = position - transform.position;
        look.y = 0;
        var npcRotation = Quaternion.LookRotation(look, Vector3.up);
        while(time > 0) {
            transform.rotation = Quaternion.Slerp(transform.rotation, npcRotation, Time.deltaTime * 10);
            time -= Time.deltaTime;
            yield return null;
        }
        transform.rotation = npcRotation;
    }


    private IEnumerator ShowText(string text)
    {
        textBox.transform.gameObject.SetActive(true);
        TextMeshProUGUI textUI = textBox.Find("Text").GetComponent<TextMeshProUGUI>();
        
        for (int i = 0; i < text.Length; i++)
        {
            textUI.text = text.Substring(0, i + 1);
            yield return new WaitForSeconds(1f / textSpeed);
        }
        yield return new NPCDialog.WaitForInput(playerController);
        textUI.text = "";
        textBox.transform.gameObject.SetActive(false);
    }

    protected bool PathComplete()
    {
        if ( Vector3.Distance( agent.destination, agent.transform.position) <= agent.stoppingDistance)
        {
            if (!agent.hasPath || agent.velocity.sqrMagnitude == 0f)
            {
                return true;
            }
        }

        return false;
    }

    public class WaitForInput : CustomYieldInstruction {

        private PlayerController playerController;

        public override bool keepWaiting
        {
            get
            {
                return !playerController.Interact;
            }
        }

        public WaitForInput(PlayerController playerController)
        {
            this.playerController = playerController;
        }
    }
}
