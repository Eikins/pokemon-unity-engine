﻿using PokemonEngine.BattleSystem;
using PokemonEngine.BattleSystem.Events;
using PokemonEngine.BattleSystem.Moves;
using PokemonEngine.Data;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using PokemonEngine.Localization;
using PokemonEngine.Provider;
using UnityEngine.Playables;
using UnityEngine.Timeline;
using UnityEngine.InputSystem;

public class BattleUI : MonoBehaviour, BattleCallbackReceiver, BattleActor, PlayerControls.IBattleActions
{

    public GameObject actionsPanel;
    public GameObject attackPanel;
    public GameObject teamPanel;

    public Sprite moveBackgroundPlaceholder;

    public GameObject[] attackButtons;

    public Animator allyAnimator;
    public Animator ennemyAnimator;

    public RectTransform allyRect;
    public RectTransform ennemyRect;

    public RectTransform textBox;
    public float textSpeed;

    public Color healthGreen;
    public Color healthYellow;
    public Color healthRed;

    public TypeTable typeTable;

    [Header("UI")]
    public Sprite[] actionSprites;
    public Sprite[] actionHoverSprites;

    public Material fontMaterial;
    public Material hoverFontMaterial;

    public Sprite pokeballSprite;
    public Sprite pokeballStatusSprite;
    public Sprite pokeballKOSprite;

    public Sprite femaleIcon;
    public Sprite maleIcon;

    [Header("Pokemons")]

    public Transform allyTransform;
    public Transform ennemyTransform;

    [Header("Effects")]
    public GameObject statRiseVFXObject;
    public GameObject statFallVFXObject;

    [Header("Status")]
    public Sprite statusBurnSprite;
    public Sprite statusFreezeSprite;
    public Sprite statusParalysisSprite;
    public Sprite statusPoisonSprite;
    public Sprite statusSleepSprite;
    public Sprite statusToxicitySprite;

    public Color burnTint;
    public Color freezeTint;
    public Color paralysisTint;
    public Color poisonTint;
    public Color sleepTint;
    public Color toxicityTint;

    private Battle battle;

    private Battle Battle => battle;

    private BattleAction chosedAction;
    private Team allyTeam;
    private Trainer ennemy;
    private Pokemon allyPokemon;
    private Pokemon ennemyPokemon;

    public PlayableDirector playableDirector;
    public TimelineAsset defaultMoveAnimationTimeline;

    [Header("Juiciness")]
    public AnimationCurve buttonResponse;

    private Coroutine allyColorPingPong;
    private Coroutine ennemyColorPingPong;

    private int actionIndex;
    private bool waitContinue;

    private PlayerControls inputs;


    void Awake() {
        var handler = BattleHandler.Instance;
        allyTeam = handler.allyTeam;
        ennemy = handler.trainer;
    }
    
    // Use this for initialization
    void Start()
    {

        I18n.LoadLang("fr-FR");
        inputs = new PlayerControls();
        inputs.Battle.SetCallbacks(this);
        inputs.Battle.Enable();
        battle = new Battle(this, this, this, new BattleAISimple(ennemy.Team), typeTable);
    }


    private void Update()
    {
        battle.Update();
    }

    #region Inputs
    public void OnBack(InputAction.CallbackContext context) {
        if(context.performed) {
            if (!actionsPanel.activeSelf) {
                if (attackPanel.activeSelf)
                {
                    attackPanel.SetActive(false);
                    actionsPanel.SetActive(true);
                    RefreshActionsPanel();
                }

                if (teamPanel.activeSelf && !forceSwitch)
                {
                    teamPanel.SetActive(false);
                    actionsPanel.SetActive(true);
                    RefreshActionsPanel();
                }
            }
        }
    }

    public void OnContinue(InputAction.CallbackContext context) {
        if(context.performed) {
            if(actionsPanel.activeSelf) {
                switch(actionIndex) {
                    case 0:
                        AttackButtonClicked();
                        break;
                    case 1:
                        TeamButtonClicked();
                        break;
                    case 2:
                        break;
                    case 3:
                        break;
                }
            } else if(attackPanel.activeSelf) {
                OnAttackClicked(actionIndex);
            }
            if(waitContinue) waitContinue = false;
        }
    }

    public void OnDown(InputAction.CallbackContext context) {
        if(context.performed) {
            if(actionsPanel.activeSelf) {
                actionIndex = Mathf.Clamp(actionIndex + 1, 0, 3);
                RefreshActionsPanel();
            } else if(attackPanel.activeSelf) {
                actionIndex = Mathf.Clamp(actionIndex + 1, 0, 3);
                var outlineRect = attackPanel.transform.Find("Outline").GetComponent<RectTransform>();
                var selectorRect = attackPanel.transform.Find("Selector").GetComponent<RectTransform>();        

                var selectionOffset = 104f * actionIndex;
                outlineRect.anchoredPosition = new Vector2(0f, -52f - selectionOffset);
                selectorRect.anchoredPosition = new Vector2(-20f, -52f - selectionOffset);
            }
        }

    }

    public void OnUp(InputAction.CallbackContext context) {
        if(context.performed) {
            if(actionsPanel.activeSelf) {
                actionIndex = Mathf.Clamp(actionIndex - 1, 0, 3);
                RefreshActionsPanel();
            } else if(attackPanel.activeSelf) {
                actionIndex = Mathf.Clamp(actionIndex - 1, 0, 3);
                var outlineRect = attackPanel.transform.Find("Outline").GetComponent<RectTransform>();
                var selectorRect = attackPanel.transform.Find("Selector").GetComponent<RectTransform>();        

                var selectionOffset = 104f * actionIndex;
                outlineRect.anchoredPosition = new Vector2(0f, -52f - selectionOffset);
                selectorRect.anchoredPosition = new Vector2(-20f, -52f - selectionOffset);
            }
        }
    }
    #endregion

    public Team GetTeam() {
        return allyTeam;
    }

    public BattleAction GetAction(Battle battle) {
        if(chosedAction == null) return null;
        return chosedAction;
    }

    public void OnSelectionStart() {
        chosedAction = null;
        actionsPanel.SetActive(true);
        actionIndex = 0;
        RefreshActionsPanel();
    }

    private int switchChoice = -1;
    private bool forceSwitch = false;

    public int GetNextPokemon() {
        if(!forceSwitch) {
            forceSwitch = true;
            TeamButtonClicked();
        }
        if(switchChoice != -1) {
            var temp = switchChoice;
            switchChoice = -1;
            forceSwitch = false;
            return temp;
        }
        return switchChoice;
    }


    public void NotifyInvalidAction(BattleAction action) {
        chosedAction = null;
        actionsPanel.SetActive(true);
        RefreshActionsPanel();
    }

    #region BattleCallbacks 
    public IEnumerator ShowText(string text)
    {
        textBox.transform.gameObject.SetActive(true);
        TextMeshProUGUI textUI = textBox.Find("Text").GetComponent<TextMeshProUGUI>();
        for (int i = 0; i < text.Length; i++)
        {
            textUI.text = text.Substring(0, i + 1);
            yield return new WaitForSeconds(1f / textSpeed);
        }
        waitContinue = true;
        yield return new WaitUntil(() => !waitContinue);
        textUI.text = "";
        textBox.transform.gameObject.SetActive(false);
    }

    public IEnumerator ShowFormattedText(string text, params string[] formats)
    {
        return ShowText(string.Format(text, formats));
    }

    public IEnumerator PerformMoveAnimation(Move move, Pokemon attacker, Pokemon target)
    {
        var timeline = move.AnimationTimeline;

        if (timeline == null) {
            timeline = defaultMoveAnimationTimeline;
        }
        foreach (var output in timeline.outputs) {
            if(output.streamName.Equals("Attacker")) {
                playableDirector.SetGenericBinding(output.sourceObject, attacker == allyPokemon ? allyAnimator : ennemyAnimator);
            } else if(output.streamName.Equals("Target")) {
                playableDirector.SetGenericBinding(output.sourceObject, attacker == allyPokemon ? ennemyAnimator : allyAnimator);
            } else if(output.outputTargetType == typeof(Transform)) {
                playableDirector.SetGenericBinding(output.sourceObject, attacker == allyPokemon ? allyTransform : ennemyTransform);   
            }
        }
        playableDirector.playableAsset = timeline;
        playableDirector.Play();
        yield return new WaitForTimeline(playableDirector);
    }

    public IEnumerator PerformHurtAnimation(Pokemon target)
    {
        for (int i = 0; i < 4; i++)
        {
            if (target == ennemyPokemon)
            {
                foreach (var renderer in ennemyAnimator.transform.GetComponentsInChildren<SkinnedMeshRenderer>()) {
                    renderer.enabled = false;
                }
                yield return new WaitForSeconds(0.1f);
                foreach (var renderer in ennemyAnimator.transform.GetComponentsInChildren<SkinnedMeshRenderer>()) {
                    renderer.enabled = true;
                }
                yield return new WaitForSeconds(0.1f);
            }
            else
            {
                foreach (var renderer in allyAnimator.transform.GetComponentsInChildren<SkinnedMeshRenderer>()) {
                    renderer.enabled = false;
                }
                yield return new WaitForSeconds(0.1f);
                foreach (var renderer in allyAnimator.transform.GetComponentsInChildren<SkinnedMeshRenderer>()) {
                    renderer.enabled = true;
                }
                yield return new WaitForSeconds(0.1f);
            }
        }


        yield return SmoothHealth(target);
    }

    IEnumerator SmoothHealth(Pokemon pokemon)
    {
        Transform transform = pokemon == allyPokemon ? allyRect.Find("Health") : ennemyRect.Find("Health");
        RectTransform rectTransform = transform.GetComponent<RectTransform>();
        Image image = transform.GetComponent<Image>();

        float targetHealth = (float) pokemon.CurrentHealth;
        float currentHealth;
        do
        {
            currentHealth = rectTransform.sizeDelta.x / 400 * pokemon.Stats.HP;
            float newProgress = Mathf.Lerp(currentHealth, targetHealth, Time.deltaTime * pokemon.Stats.HP / 10);

            if (newProgress < 0.2f * pokemon.Stats.HP)
            {
                image.color = healthRed;
            }
            else if (newProgress < 0.5f * pokemon.Stats.HP)
            {
                image.color = healthYellow;
            }
            else
            {
                image.color = healthGreen;
            }

            rectTransform.sizeDelta = new Vector2(newProgress / pokemon.Stats.HP * 400, 14);
            if(pokemon == allyPokemon) {
                allyRect.Find("Health_Text").GetComponent<TextMeshProUGUI>().text = (int) currentHealth + "/" + pokemon.Stats.HP;
            }
            yield return null;
        } while (Mathf.Abs(currentHealth - targetHealth) > 0.1f);
        RefreshHealth(pokemon == allyPokemon ? allyRect : ennemyRect, pokemon);
    }

    public IEnumerator PerformDeathAnimation(Pokemon target)
    {
        var animator = target == allyPokemon ? allyAnimator : ennemyAnimator;
        float time = 1f;
        Vector3 scale = animator.transform.localScale;
        animator.SetTrigger("Knockout");
        yield return new WaitForSeconds(0.8f);
        while (time > 0) {
            time -= Time.deltaTime;
            animator.transform.localScale = scale * time * time;
            yield return null;
        }
        yield return ShowFormattedText(I18n.Translate("Battle.Knockout"), target.DisplayName);
        if(target == allyPokemon) {
            if(allyColorPingPong != null) StopCoroutine(allyColorPingPong);
            Destroy(allyAnimator.gameObject);
            allyAnimator = null;
        } else {
            if(ennemyColorPingPong != null) StopCoroutine(ennemyColorPingPong);
            Destroy(ennemyAnimator.gameObject);
            ennemyAnimator = null;
        }
    }

    public IEnumerator PerformStatModificationAnimation(Pokemon pokemon, StatModifier[] modifiers) {
        Transform position = pokemon == allyPokemon ? allyTransform : ennemyTransform;

        bool playParticleAnimaton = true;
        bool lastWasRising = false;
        foreach (var modifier in modifiers) {

            if(!playParticleAnimaton && lastWasRising != modifier.IsRising()) {
                playParticleAnimaton = true;
            } 

            if(playParticleAnimaton) {
                if(modifier.IsRising()) {
                    Destroy(Instantiate(statRiseVFXObject, position), 3f);
                } else {
                    Destroy(Instantiate(statFallVFXObject, position), 3f);
                }
                yield return new WaitForSeconds(2.5f);
                playParticleAnimaton = false;
            }
            string key = "Battle.StatChange." + modifier.Value;
            string statDisplay = I18n.Translate("Battle.StatChange." + modifier.Statistic.ToString());
            yield return ShowFormattedText(I18n.Translate(key), statDisplay, pokemon.DisplayName);
            lastWasRising = modifier.IsRising();
        }
    }

    public IEnumerator PerformSwitchAnimation(BattleActor actor, int index, Pokemon newPokemon) {
        
        if (allyAnimator != null && actor == (BattleActor) this) {
            yield return ShowFormattedText(I18n.Translate("Battle.Switch.Withdraw.Player"), allyPokemon.DisplayName);
            allyRect.gameObject.SetActive(false);
            float time = 0.75f;
            Vector3 scale = allyAnimator.transform.localScale;
            while (time > 0) {
                time -= Time.deltaTime;
                allyAnimator.transform.localScale = scale * time;
                yield return null;
            }
            if(allyColorPingPong != null) StopCoroutine(allyColorPingPong);
            Destroy(allyAnimator.gameObject);

        } else if (ennemyAnimator != null && actor != (BattleActor) this) {
            yield return ShowFormattedText(I18n.Translate("Battle.Switch.Withdraw.Trainer"), ennemy.DisplayName, ennemyPokemon.DisplayName);
            ennemyRect.gameObject.SetActive(false);
            float time = 0.75f;
            Vector3 scale = ennemyAnimator.transform.localScale;
            while (time > 0) {
                time -= Time.deltaTime;
                ennemyAnimator.transform.localScale = scale * time;
                yield return null;
            }
            if(ennemyColorPingPong != null) StopCoroutine(ennemyColorPingPong);
            Destroy(ennemyAnimator.gameObject);
        }

        if(actor == (BattleActor) this) {
            yield return ShowFormattedText(I18n.Translate("Battle.Switch.Sending.Player"), newPokemon.DisplayName);
            var allyGo = Instantiate(PokemonProvider.PokemonRenderAsset(newPokemon).Prefab, allyTransform) as GameObject;
            allyAnimator = allyGo.GetComponent<Animator>();
            allyPokemon = newPokemon;
            RefreshPokemonInfoBox(allyRect, allyPokemon, allyTeam);
            float time = 0f;
            Vector3 scale = allyAnimator.transform.localScale;
            while (time < 0.75f) {
                time += Time.deltaTime;
                allyAnimator.transform.localScale = scale * time;
                yield return null;
            }
            allyRect.gameObject.SetActive(true);
            RefreshAttackButtons();
        } else {
            yield return ShowFormattedText(I18n.Translate("Battle.Switch.Sending.Trainer"), ennemy.DisplayName, newPokemon.DisplayName);
            var ennemyGo = Instantiate(PokemonProvider.PokemonRenderAsset(newPokemon).Prefab, ennemyTransform) as GameObject;
            ennemyAnimator = ennemyGo.GetComponent<Animator>();
            ennemyPokemon = newPokemon;
            RefreshPokemonInfoBox(ennemyRect, ennemyPokemon, ennemy.Team);
            float time = 0f;
            Vector3 scale = ennemyAnimator.transform.localScale;
            while (time < 0.75f) {
                time += Time.deltaTime;
                ennemyAnimator.transform.localScale = scale * time;
                yield return null;
            }
            ennemyRect.gameObject.SetActive(true);
        }
    }

    #endregion

    private void RefreshAttackButtons()
    {
        var outlineRect = attackPanel.transform.Find("Outline").GetComponent<RectTransform>();
        var selectorRect = attackPanel.transform.Find("Selector").GetComponent<RectTransform>();        

        var selectionOffset = 104f * actionIndex;
        outlineRect.anchoredPosition = new Vector2(0f, -52f - selectionOffset);
        selectorRect.anchoredPosition = new Vector2(selectorRect.anchoredPosition.x, -52f - selectionOffset);

        for (int i = 0; i < attackButtons.Length; i++)
        {
            attackButtons[i].SetActive(allyPokemon.Moveset[i].NotNull);
            if (allyPokemon.Moveset[i].NotNull)
            {
                Move move = allyPokemon.Moveset[i].Move;
                int pp = allyPokemon.Moveset[i].PowerPoints;
                int maxPP = allyPokemon.Moveset[i].MaxPowerPoints;
                attackButtons[i].GetComponent<Image>().color = move.Type.Color;
                attackButtons[i].transform.Find("Type").GetComponent<Image>().sprite = move.Type.AlternativeIcon;
                attackButtons[i].transform.Find("MoveName").GetComponent<TextMeshProUGUI>().text = I18n.Translate(move.UnlocalizedName);
                var powerPointTMP =  attackButtons[i].transform.Find("PowerPoints").GetComponent<TextMeshProUGUI>();
               powerPointTMP.text = pp + " / " + maxPP;

                if (allyPokemon.Moveset[i].PowerPoints == 0)
                {
                    attackButtons[i].GetComponent<Button>().interactable = false;
                }
                else
                {
                    attackButtons[i].GetComponent<Button>().interactable = true;
                }

                if (allyPokemon.Moveset[i].PowerPoints <= 0.2f * allyPokemon.Moveset[i].MaxPowerPoints)
                {
                    powerPointTMP.color = healthRed;
                }
                else if (allyPokemon.Moveset[i].PowerPoints <= 0.5f * allyPokemon.Moveset[i].MaxPowerPoints)
                {
                    powerPointTMP.color = healthYellow;
                }
                else
                {
                    powerPointTMP.color = Color.white;
                }
            }
            else
            {
                attackButtons[i].transform.Find("MoveName").GetComponent<TextMeshProUGUI>().text = i % 2 == 0 ? "Attac" : "Protecc";
                attackButtons[i].transform.Find("PowerPoints").GetComponent<TextMeshProUGUI>().text = "1 / 1";
            }
        }
    }

    private void RefreshPokemonInfoBox(RectTransform infoBox, Pokemon pokemon, Team team) {
        var name = infoBox.Find("Name").GetComponent<TextMeshProUGUI>();
        var level = infoBox.Find("Lv");
        var levelRect = level.GetComponent<RectTransform>();
        var levelPos = levelRect.anchoredPosition;


        infoBox.Find("Level").GetComponent<TextMeshProUGUI>().text = pokemon.Level.ToString();
        name.text = GetDisplayName(pokemon);

        if(pokemon.Level >= 100) {
            levelPos.x = -147;
        } else if(pokemon.Level >= 10) {
            levelPos.x = -125;
        } else {
            levelPos.x = -103;
        }
        if(infoBox == ennemyRect) {
            levelPos.x += 25;

        }
        levelRect.anchoredPosition = levelPos;

        infoBox.Find("Gender").GetComponent<Image>().sprite = pokemon.Female ? femaleIcon : maleIcon;

        RefreshPokemonStatus(pokemon);
        RefreshHealth(infoBox, pokemon);
    }

    private void RefreshPokeballs(Transform pokeballPanel, Team team) {
        for (int i = 0; i < 6; i++) {
            var child = pokeballPanel.GetChild(i);
            if(i < team.Count) {
                var pokemon = team[i];
                var img = child.GetComponent<Image>();
                child.gameObject.SetActive(true);
                if(pokemon.IsKnockout()) {
                    img.sprite = pokeballKOSprite;
                } else if (pokemon.Status != Status.None) {
                    img.sprite = pokeballStatusSprite;
                } else {
                    img.sprite = pokeballSprite;
                }
            } else {
                child.gameObject.SetActive(false);
            }
        }
    }

    public void RefreshPokemonStatus(Pokemon pokemon) {
        bool ally = pokemon == allyPokemon;
        var rect = ally ? allyRect : ennemyRect;
        var spriteTransform = rect.Find("Status");
        var color = Color.white;
        if(pokemon.Status == Status.None) {
            spriteTransform.gameObject.SetActive(false);
        } else {
            spriteTransform.gameObject.SetActive(true);
            Sprite sprite;
            switch (pokemon.Status) {
                case Status.Burn: 
                    color = burnTint;
                    sprite = statusBurnSprite;
                    break;
                case Status.Freeze: 
                    color = freezeTint;
                    sprite = statusFreezeSprite;
                    break;
                case Status.Paralysis: 
                    color = paralysisTint;
                    sprite = statusParalysisSprite;
                    break;
                case Status.Poison: 
                    color = poisonTint;
                    sprite = statusPoisonSprite;
                    break;
                case Status.Sleep: 
                    color = sleepTint;
                    sprite = statusSleepSprite;
                    break;
                case Status.Toxicity: 
                    color = toxicityTint;
                    sprite = statusToxicitySprite;
                    break;
                default: 
                    sprite = null;
                    break;
            }
            spriteTransform.GetComponent<Image>().sprite = sprite;
        }
        var transform = ally ? allyAnimator.transform : ennemyAnimator.transform;
        
        if(ally && allyColorPingPong != null) {
            StopCoroutine(allyColorPingPong);
        } else if(ennemyColorPingPong != null) {
            StopCoroutine(ennemyColorPingPong);
        }

        if(color != Color.white) {
            var coroutine = StartCoroutine(ColorPingPong(transform, color));
            if(ally) {
                allyColorPingPong = coroutine;
            } else {
                ennemyColorPingPong = coroutine;
            }
        } else {
            var renderers = transform.GetComponentsInChildren<Renderer>();
            foreach (var renderer in renderers) {
                foreach(var mat in renderer.materials) {
                    mat.SetColor("_Tint", Color.white);
                }
            }    
        }

        if(ally) {
            RefreshPokeballs(allyRect.Find("Pokeballs"), allyTeam);
        } else {
            RefreshPokeballs(ennemyRect.Find("Pokeballs"), ennemy.Team);
        }

    }

    private IEnumerator ColorPingPong(Transform transform, Color targetColorolor) {
        var renderers = transform.GetComponentsInChildren<Renderer>();
        while(true) {
            var pingPong = Mathf.PingPong(Time.time / 3, 1);
            var color = Color.Lerp(Color.white, targetColorolor, pingPong);
            foreach (var renderer in renderers) {
                foreach(var mat in renderer.materials) {
                    mat.SetColor("_Tint", color);
                }
            }    
            yield return null;
        }
    }

    private void RefreshHealth(RectTransform infoBox, Pokemon pokemon) {
        int maxHP = pokemon.Stats.HP;
        infoBox.Find("Health").GetComponent<RectTransform>().sizeDelta = new Vector2((float)pokemon.CurrentHealth / maxHP * 400, 14);

        if (pokemon.CurrentHealth < 0.2f * maxHP) {
            infoBox.Find("Health").GetComponent<Image>().color = healthRed;
        } else if (pokemon.CurrentHealth < 0.5f * maxHP) {
            infoBox.Find("Health").GetComponent<Image>().color = healthYellow;
        } else {
            infoBox.Find("Health").GetComponent<Image>().color = healthGreen;
        }

        if(pokemon == allyPokemon) {
            infoBox.Find("Health_Text").GetComponent<TextMeshProUGUI>().text = pokemon.CurrentHealth + "/" + maxHP;
        }

    }

    public void AttackButtonClicked()
    {
        var rect = actionsPanel.transform.GetChild(0).GetComponent<RectTransform>();
        StartCoroutine(AttackButtonClickedAnim(rect));
    }

    private IEnumerator AttackButtonClickedAnim(RectTransform rect) {
        yield return ButtonResponeAnim(rect);
        RefreshAttackButtons();
        actionsPanel.SetActive(false);
        attackPanel.SetActive(true);
    }

    private IEnumerator ButtonResponeAnim(params RectTransform[] rects) {
        float duration = buttonResponse.keys[buttonResponse.length - 1].time;
        float time = 0;
        while(time < duration) {
            var scale = buttonResponse.Evaluate(time);
            foreach(var rect in rects) rect.localScale = new Vector3(scale, scale, 1f);
            yield return null;
            time += Time.deltaTime;
        }
        foreach(var rect in rects) rect.localScale = new Vector3(1f, 1f, 1f);
    }

    public void TeamButtonClicked() {
        var rect = actionsPanel.transform.GetChild(1).GetComponent<RectTransform>();
        StartCoroutine(TeamButtonClickedAnim(rect));
    }

    private IEnumerator TeamButtonClickedAnim(RectTransform rect) {
        yield return ButtonResponeAnim(rect);
        actionsPanel.SetActive(false);
        teamPanel.SetActive(true);
        RefreshTeamPanel();
    }

    public void RefreshActionsPanel() {
        var selectorRect = actionsPanel.transform.Find("Selector").GetComponent<RectTransform>();
        selectorRect.anchoredPosition = new Vector3(-20f, -58f - (116f * actionIndex));
        for (int i = 0; i < 4; i++) {
            var button = actionsPanel.transform.GetChild(i);
            var img = button.GetComponent<Image>();
            var text = button.GetComponentInChildren<TextMeshProUGUI>();
            if(i == actionIndex) {
                img.sprite = actionHoverSprites[i];
                text.fontMaterial = hoverFontMaterial;
            } else {
                img.sprite = actionSprites[i];
                text.fontMaterial = fontMaterial;
            }
        }
    }

    public void OnAttackClicked(int id)
    {
        var rect = attackButtons[id].GetComponent<RectTransform>();
        StartCoroutine(OnAttackClickedAnim(rect, id));
    }

    private IEnumerator OnAttackClickedAnim(RectTransform rect, int id) {
        var rect2 = attackPanel.transform.Find("Outline").GetComponent<RectTransform>();
        yield return ButtonResponeAnim(rect, rect2);
        if(allyPokemon.Moveset[id] == null) yield break;
        attackPanel.SetActive(false);
        TargetPoint target = null;
        foreach (var targetPoint in battle.TargetPoints) {
            if(targetPoint.Actor != (BattleActor) this) {
                target = targetPoint;
            }
        }
        chosedAction = new UseMoveAction(id, target);
    }

    public void OnTeamPokemonClicked(int id)
    {
        if(battle.CanSwitch(this, battle.ActorTargetPoints[this][0], id)) {
            teamPanel.SetActive(false);
            if(forceSwitch) {
                switchChoice = id;
            } else {
                chosedAction = new SwitchPokemonAction(battle.ActorTargetPoints[this][0], id);
            }
        } else {
            StartCoroutine(ShowText("Vous ne pouvez pas échanger ce pokémon."));
        }

    }

    private string GetDisplayName(Pokemon p)
    {
        return p.HasNickname ? p.Nickname : I18n.Translate(p.PokemonData.UnlocalizedName);
    }

    private string UseMoveText(Pokemon pokemon, Move move)
    {
        return string.Format(I18n.Translate("Battle.UseMove"), GetDisplayName(pokemon), I18n.Translate(move.UnlocalizedName));
    }

    private void RefreshTeamPanel() {
        for(int i = 0; i < 6; i++) {
            var button = teamPanel.transform.Find("Pokemon" + (i + 1));
            if(i < allyTeam.Count) {
                RefreshTeamPokemonButton(i, button);
            } else {
                button.gameObject.SetActive(false);
            }
        }
    }

    private void RefreshTeamPokemonButton(int index, Transform button) {
        var pokemon = allyTeam[index];
        button.Find("Sprite").GetComponent<Image>().sprite = PokemonProvider.PokemonRenderAsset(pokemon).Icon;
        button.Find("Name").GetComponent<TextMeshProUGUI>().text = pokemon.DisplayName;
        button.Find("Level").GetComponent<TextMeshProUGUI>().text = "Lv. " + pokemon.Level;
    
        button.Find("Health").GetComponent<RectTransform>().sizeDelta = new Vector2((float)pokemon.CurrentHealth / pokemon.Stats.HP * 100, 10);
 
        if (pokemon.CurrentHealth < 0.2f * pokemon.Stats.HP) {
            button.Find("Health").GetComponent<Image>().color = healthRed;
        } else if (pokemon.CurrentHealth < 0.5f * pokemon.Stats.HP) {
            button.Find("Health").GetComponent<Image>().color = healthYellow;
        } else {
            button.Find("Health").GetComponent<Image>().color = healthGreen;
        }

        button.Find("Health_Text").GetComponent<TextMeshProUGUI>().text = pokemon.CurrentHealth + "/" + pokemon.Stats.HP;

    }

}
