﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(RectTransform))]
public class SelectorAnim : MonoBehaviour
{

    private RectTransform selectorRect;

    public float speed;
    public float minX, maxX;

    private float amplitude;

    void Start()
    {
        selectorRect = GetComponent<RectTransform>();
        amplitude = (maxX - minX) / 2f;
        speed = Mathf.PI * speed;
    }

    void Update()
    {
        var x = minX + amplitude + amplitude * Mathf.Sin(speed * Time.time);
        selectorRect.anchoredPosition = new Vector2(x, selectorRect.anchoredPosition.y);
    }
}
