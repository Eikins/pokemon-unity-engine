using UnityEngine;
using UnityEngine.Playables;

public class WaitForTimeline : CustomYieldInstruction
{

    private PlayableDirector playableDirector;

    public override bool keepWaiting
    {
        get
        {
            return playableDirector.state == PlayState.Playing;
        }
    }

    public WaitForTimeline(PlayableDirector playableDirector)
    {
        this.playableDirector = playableDirector;
    }
}