﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Pokemon3D.Data {

    [CreateAssetMenu(menuName = "Pokemon Engine/3D/Pokemon Render Asset", fileName = "New Render", order = 0)]
    public class PokemonRenderAsset : ScriptableObject
    {

        [SerializeField] private Sprite icon;
        [SerializeField] private GameObject prefab;

        public Sprite Icon => icon;
        public GameObject Prefab => prefab;

    }

}

