﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using PokemonEngine.Data;
using PokemonEngine.BattleSystem;
using PokemonEngine.BattleSystem.Moves;

[CustomEditor(typeof(Move))]
public class MoveEditor : Editor {

    Move move;

    private bool showProperties = true;

    private void OnEnable()
    {
        move = target as Move;
    }

    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        string behaviourClass = serializedObject.FindProperty("behaviourClass").stringValue;
        System.Type moveClassType = System.Type.GetType("PokemonEngine.BattleSystem.Moves." + behaviourClass + ", Assembly-CSharp");
        if(moveClassType == null) {
            EditorGUILayout.HelpBox("This class does not exists.", MessageType.Error);
            EditorGUILayout.HelpBox("Move class should be part of the namespace PokemonEngine.BattleSystem.Moves", MessageType.Info);
            SerializedProperty keyList = serializedObject.FindProperty("moveProperties.keys");
            SerializedProperty valueList = serializedObject.FindProperty("moveProperties.values");
            keyList.arraySize = 0;
            valueList.arraySize = 0;
        } else if(!(typeof(MoveBehaviour)).IsAssignableFrom(moveClassType)) {
            EditorGUILayout.HelpBox("This class is not a subclass of MoveBehaviour.", MessageType.Error);
            EditorGUILayout.HelpBox("Move class should be part of the namespace PokemonEngine.BattleSystem.Moves", MessageType.Info);
            SerializedProperty keyList = serializedObject.FindProperty("moveProperties.keys");
            SerializedProperty valueList = serializedObject.FindProperty("moveProperties.values");
            keyList.arraySize = 0;
            valueList.arraySize = 0;
        } else {
            EditorGUILayout.BeginHorizontal();
            showProperties = EditorGUILayout.Foldout(showProperties, "Move Properties", true);
            if (GUILayout.Button(new GUIContent("Reset", "Reset properties"), EditorStyles.miniButtonRight)) {
                SerializedProperty keyList = serializedObject.FindProperty("moveProperties.keys");
                SerializedProperty valueList = serializedObject.FindProperty("moveProperties.values");
                keyList.arraySize = 0;
                valueList.arraySize = 0;
            }
            EditorGUILayout.EndHorizontal();
            if (showProperties)
            {
                EditorGUI.indentLevel++;
                PropertiesLayout(moveClassType);
                EditorGUI.indentLevel--;
            }
        }

        serializedObject.ApplyModifiedProperties();
    }

    private void PropertiesLayout(System.Type moveClassType) {

        SerializedProperty keyList = serializedObject.FindProperty("moveProperties.keys");
        SerializedProperty valueList = serializedObject.FindProperty("moveProperties.values");

        int index = 0;
        foreach(var field in moveClassType.GetFields()) {

            var attributes = field.GetCustomAttributes(typeof(MovePropertyAttribute), true);
            if(attributes.Length > 0) {
                var attribute = attributes[0] as MovePropertyAttribute;

                if(index >= keyList.arraySize) {
                    keyList.InsertArrayElementAtIndex(keyList.arraySize);
                    valueList.InsertArrayElementAtIndex(valueList.arraySize);
                    DefaultValue(attribute, field.FieldType, valueList.GetArrayElementAtIndex(keyList.arraySize - 1).FindPropertyRelative("value"));
                }
                var key = keyList.GetArrayElementAtIndex(index);
                var value = valueList.GetArrayElementAtIndex(index).FindPropertyRelative("value");

                string name = attribute.Name == null ? field.Name : attribute.Name;
                var type = field.FieldType;
                key.stringValue = field.Name;
                if(type == typeof(string)) {
                    value.stringValue = EditorGUILayout.TextField(name, value.stringValue);
                } else if(type == typeof(float)) {
                    value.stringValue = EditorGUILayout.FloatField(name, float.Parse(value.stringValue)).ToString();
                } else if(type == typeof(int)) {
                    value.stringValue = EditorGUILayout.IntField(name, int.Parse(value.stringValue)).ToString();
                } else if(type == typeof(bool)) {
                    value.stringValue = EditorGUILayout.Toggle(name, bool.Parse(value.stringValue)).ToString();
                } else if(typeof(Enum).IsAssignableFrom(type)) {
                    Enum enumValue = (Enum) Enum.ToObject(type, int.Parse(value.stringValue));
                    value.stringValue = Convert.ToInt32(EditorGUILayout.EnumPopup(name, enumValue)).ToString();
                } else {
                    EditorGUILayout.HelpBox("Type (" + type.Name + ") of property " + name + " not supported.\n Types supported are string, int, float and bool.", MessageType.Error);
                }
                index++;
            }
        }
    }

    private void DefaultValue(MovePropertyAttribute attribute, System.Type type, SerializedProperty property) {
        if(attribute.Default != null) {
            property.stringValue = attribute.Default;
            return;
        }

        if(type == typeof(string)) {
            property.stringValue = "";
        } else if(type == typeof(float) || type == typeof(int) || typeof(Enum).IsAssignableFrom(type)) {
            property.stringValue = "0";
        } else if(type == typeof(bool)) {
            property.stringValue = "false";
        }
    }

}
