﻿using UnityEngine;
using UnityEditor;

[CustomPropertyDrawer(typeof(NamedArrayAttribute))]
public class NamedArrayDrawer : PropertyDrawer
{
    public override void OnGUI(Rect rect, SerializedProperty property, GUIContent label)
    {
        NamedArrayAttribute namedArrayAttribute = attribute as NamedArrayAttribute;
        int pos = int.Parse(property.propertyPath.Split('[', ']')[1]) + 1;
        EditorGUI.PropertyField(rect, property, new GUIContent(namedArrayAttribute.name + " " + pos));
    }
}
