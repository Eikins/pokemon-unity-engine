﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using PokemonEngine.Data;

[CustomEditor(typeof(PokemonData))]
public class PokemonDataEditor : Editor
{

    PokemonData pokemonData;

    private void OnEnable()
    {
        pokemonData = target as PokemonData;
    }

    #region GUI Preview
    public override Texture2D RenderStaticPreview(string assetPath, UnityEngine.Object[] subAssets, int width, int height)
    {
        Texture2D icon = AssetDatabase.LoadAssetAtPath("Assets/Textures/Pokemons/Icons/" + pokemonData.InternalName + ".png", typeof(Texture2D)) as Texture2D;
        if (icon != null)
        {
            Texture2D newIcon = new Texture2D(width, height);
            EditorUtility.CopySerialized(icon, newIcon);
            return newIcon;
        }
        return base.RenderStaticPreview(assetPath, subAssets, width, height);
    }

    public override bool HasPreviewGUI()
    {
        return true;
    }

    public override void OnInteractivePreviewGUI(Rect r, GUIStyle background)
    {
        Texture2D icon = AssetDatabase.LoadAssetAtPath("Assets/Textures/Pokemons/Icons/" + pokemonData.InternalName + ".png", typeof(Texture2D)) as Texture2D;
        if (icon == null)
        {
            base.OnInteractivePreviewGUI(r, background);
        }
        else
        {
            GUI.DrawTexture(r, icon, ScaleMode.ScaleToFit);
        }
    }

    public override void OnPreviewGUI(Rect r, GUIStyle background)
    {
        Texture2D icon = AssetDatabase.LoadAssetAtPath("Assets/Textures/Pokemons/Icons/" + pokemonData.InternalName + ".png", typeof(Texture2D)) as Texture2D;
        if (icon == null)
        {
            base.OnPreviewGUI(r, background);
        }
        else
        {
            GUI.DrawTexture(r, icon, ScaleMode.ScaleToFit);
        }
    }
    #endregion

    private bool showTypes = true;
    private bool showAbilities = true;
    private bool showHatching = true;
    private bool showBaseStats = true;
    private bool showEVYields = true;

    private bool showLevelLearnSet = false;
    private bool showTMLearnSet = false;
    private bool showEggLearnSet = false;
    private bool showTutorLearnSet = false;
    private bool showDreamWorldLearnSet = false;
    private bool showEventLearnSet = false;

    public override void OnInspectorGUI()
    {
        serializedObject.Update();
        EditorGUILayout.LabelField("Pokemon Data");
        EditorGUILayout.Separator();
        EditorGUILayout.PropertyField(serializedObject.FindProperty("unlocalizedName"));
        EditorGUILayout.PropertyField(serializedObject.FindProperty("nationalId"));
        DrawUILine(Color.grey);
        showTypes = EditorGUILayout.Foldout(showTypes, "Types", true);
        if (showTypes)
        {
            EditorGUI.indentLevel++;
            EditorGUILayout.PropertyField(serializedObject.FindProperty("primaryType"), new GUIContent("Primary"));
            EditorGUILayout.PropertyField(serializedObject.FindProperty("secondaryType"), new GUIContent("Secondary"));
            EditorGUI.indentLevel--;
        }
        DrawUILine(Color.grey);
        showAbilities = EditorGUILayout.Foldout(showAbilities, "Abilities", true);
        if (showAbilities)
        {
            EditorGUI.indentLevel++;
            EditorGUILayout.PropertyField(serializedObject.FindProperty("primaryAbility"), new GUIContent("Primary"));
            EditorGUILayout.PropertyField(serializedObject.FindProperty("secondaryAbility"), new GUIContent("Secondary"));
            EditorGUILayout.PropertyField(serializedObject.FindProperty("hiddenAbility"), new GUIContent("Hidden"));
            EditorGUI.indentLevel--;
        }
        DrawUILine(Color.grey);
        EditorGUILayout.PropertyField(serializedObject.FindProperty("height"));
        EditorGUILayout.PropertyField(serializedObject.FindProperty("weight"));
        EditorGUILayout.PropertyField(serializedObject.FindProperty("genderless"));
        EditorGUI.BeginDisabledGroup(serializedObject.FindProperty("genderless").boolValue);
        EditorGUILayout.PropertyField(serializedObject.FindProperty("femaleRatio"));
        EditorGUI.EndDisabledGroup();
        EditorGUILayout.PropertyField(serializedObject.FindProperty("catchRate"));
        DrawUILine(Color.grey);
        showHatching = EditorGUILayout.Foldout(showHatching, "Hatching", true);
        if (showHatching)
        {
            EditorGUI.indentLevel++;
            EditorGUILayout.PropertyField(serializedObject.FindProperty("primaryEggGroup"));
            EditorGUILayout.PropertyField(serializedObject.FindProperty("secondaryEggGroup"));
            EditorGUILayout.PropertyField(serializedObject.FindProperty("hatchCycles"));
            EditorGUI.indentLevel--;
        }
        DrawUILine(Color.grey);
        EditorGUILayout.PropertyField(serializedObject.FindProperty("baseExp"));
        EditorGUILayout.PropertyField(serializedObject.FindProperty("levelingRate"));
        EditorGUILayout.PropertyField(serializedObject.FindProperty("baseFriendship"));
        DrawUILine(Color.grey);
        showBaseStats = EditorGUILayout.Foldout(showBaseStats, "Base Stats", true);
        if (showBaseStats)
        {
            EditorGUI.indentLevel++;
            serializedObject.FindProperty("baseStats.hp").intValue = EditorGUILayout.IntSlider("HP", serializedObject.FindProperty("baseStats.hp").intValue, 1, 255);
            serializedObject.FindProperty("baseStats.attack").intValue = EditorGUILayout.IntSlider("Attack", serializedObject.FindProperty("baseStats.attack").intValue, 1, 255);
            serializedObject.FindProperty("baseStats.defense").intValue = EditorGUILayout.IntSlider("Defense", serializedObject.FindProperty("baseStats.defense").intValue, 1, 255);
            serializedObject.FindProperty("baseStats.spAtk").intValue = EditorGUILayout.IntSlider("SpAtk", serializedObject.FindProperty("baseStats.spAtk").intValue, 1, 255);
            serializedObject.FindProperty("baseStats.spDef").intValue = EditorGUILayout.IntSlider("SpDef", serializedObject.FindProperty("baseStats.spDef").intValue, 1, 255);
            serializedObject.FindProperty("baseStats.speed").intValue = EditorGUILayout.IntSlider("Speed", serializedObject.FindProperty("baseStats.speed").intValue, 1, 255);
            EditorGUI.BeginDisabledGroup(true);
            EditorGUILayout.IntField("Total", pokemonData.BaseStats.GetTotal());
            EditorGUI.EndDisabledGroup();
            EditorGUI.indentLevel--;
        }
        DrawUILine(Color.grey);
        showEVYields = EditorGUILayout.Foldout(showEVYields, "EV Yields", true);
        if (showEVYields)
        {
            EditorGUI.indentLevel++;
            serializedObject.FindProperty("evYields.hp").intValue = EditorGUILayout.IntSlider("HP", serializedObject.FindProperty("evYields.hp").intValue, 0, 3);
            serializedObject.FindProperty("evYields.attack").intValue = EditorGUILayout.IntSlider("Attack", serializedObject.FindProperty("evYields.attack").intValue, 0, 3);
            serializedObject.FindProperty("evYields.defense").intValue = EditorGUILayout.IntSlider("Defense", serializedObject.FindProperty("evYields.defense").intValue, 0, 3);
            serializedObject.FindProperty("evYields.spAtk").intValue = EditorGUILayout.IntSlider("SpAtk", serializedObject.FindProperty("evYields.spAtk").intValue, 0, 3);
            serializedObject.FindProperty("evYields.spDef").intValue = EditorGUILayout.IntSlider("SpDef", serializedObject.FindProperty("evYields.spDef").intValue, 0, 3);
            serializedObject.FindProperty("evYields.speed").intValue = EditorGUILayout.IntSlider("Speed", serializedObject.FindProperty("evYields.speed").intValue, 0, 3);
            EditorGUI.indentLevel--;
        }

        DrawUILine(Color.grey);
        showLevelLearnSet = EditorGUILayout.Foldout(showLevelLearnSet, "Level Learn Set", true);
        if (showLevelLearnSet)
        {
            EditorGUI.indentLevel++;
            var list = serializedObject.FindProperty("learnSet.levelLearnSet.moves");
            int removeIndex = -1;
            for(int i = 0; i < list.arraySize; i++) {
                var element = list.GetArrayElementAtIndex(i);
                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.PropertyField(element.FindPropertyRelative("level"), new GUIContent("Level"));
                EditorGUILayout.PropertyField(element.FindPropertyRelative("move"), GUIContent.none);
                if(GUILayout.Button(new GUIContent("x", "Remove item"), EditorStyles.miniButtonRight)) {
                    removeIndex = i;
                }
                EditorGUILayout.EndHorizontal();
            }
            if(removeIndex != -1) {
                list.DeleteArrayElementAtIndex(removeIndex);
            }
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.Space();
            if(GUILayout.Button(new GUIContent("+", "Add item"), GUILayout.Width(40))) {
                list.arraySize++;
            }
            EditorGUILayout.EndHorizontal();

            EditorGUI.indentLevel--;
        }

        showTMLearnSet = EditorGUILayout.Foldout(showTMLearnSet, "TM Learn Set", true);
        if (showTMLearnSet)
        {
            DrawBasicList(serializedObject.FindProperty("learnSet.tmLearnSet.moves"), "Move");
        }

        showEggLearnSet = EditorGUILayout.Foldout(showEggLearnSet, "Egg Learn Set", true);
        if (showEggLearnSet)
        {
            DrawBasicList(serializedObject.FindProperty("learnSet.eggLearnSet.moves"), "Move");
        }

        showTutorLearnSet = EditorGUILayout.Foldout(showTutorLearnSet, "Tutor Learn Set", true);
        if (showTutorLearnSet)
        {
            DrawBasicList(serializedObject.FindProperty("learnSet.tutorLearnSet.moves"), "Move");
        }

        showDreamWorldLearnSet = EditorGUILayout.Foldout(showDreamWorldLearnSet, "DreamWorld Learn Set", true);
        if (showDreamWorldLearnSet)
        {
            DrawBasicList(serializedObject.FindProperty("learnSet.dreamWorldLearnSet.moves"), "Move");
        }

        showEventLearnSet = EditorGUILayout.Foldout(showEventLearnSet, "Event Learn Set", true);
        if (showEventLearnSet)
        {
            DrawBasicList(serializedObject.FindProperty("learnSet.eventLearnSet.moves"), "Move");
        }


        
        serializedObject.ApplyModifiedProperties();
    }

    public static void DrawBasicList(SerializedProperty list, string display = "Element") {
        EditorGUI.indentLevel++;
        int removeIndex = -1;
        for(int i = 0; i < list.arraySize; i++) {
            var element = list.GetArrayElementAtIndex(i);
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.PropertyField(element, new GUIContent(display));
            if(GUILayout.Button(new GUIContent("x", "Remove item"), EditorStyles.miniButtonRight, GUILayout.Width(18))) {
                removeIndex = i;
            }
            EditorGUILayout.EndHorizontal();
        }
        if(removeIndex != -1) {
            list.DeleteArrayElementAtIndex(removeIndex);
        }
        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.Space();
        if(GUILayout.Button(new GUIContent("+", "Add item"), GUILayout.Width(40))) {
            list.arraySize++;
        }
        EditorGUILayout.EndHorizontal();

        EditorGUI.indentLevel--;
    }

    public static void DrawUILine(Color color, int thickness = 2, int padding = 10)
    {
        Rect r = EditorGUILayout.GetControlRect(GUILayout.Height(padding + thickness));
        r.height = thickness;
        r.y += padding / 2;
        r.x -= 2;
        r.width += 6;
        EditorGUI.DrawRect(r, color);
    }


}
