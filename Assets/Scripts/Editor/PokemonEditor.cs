﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using PokemonEngine.Data;

[CustomEditor(typeof(Pokemon))]
public class PokemonEditor : Editor
{

    Pokemon pokemon;

    private void OnEnable()
    {
        pokemon = target as Pokemon;
    }

    #region GUI Preview
    public override Texture2D RenderStaticPreview(string assetPath, UnityEngine.Object[] subAssets, int width, int height)
    {
        if(pokemon.PokemonData == null) return base.RenderStaticPreview(assetPath, subAssets, width, height);
        Texture2D icon = AssetDatabase.LoadAssetAtPath("Assets/Textures/Pokemons/Icons/" + pokemon.PokemonData.InternalName + ".png", typeof(Texture2D)) as Texture2D;
        if (icon != null)
        {
            Texture2D newIcon = new Texture2D(width, height);
            EditorUtility.CopySerialized(icon, newIcon);
            return newIcon;
        }
        return base.RenderStaticPreview(assetPath, subAssets, width, height);
    }

    public override bool HasPreviewGUI()
    {
        return true;
    }

    public override void OnInteractivePreviewGUI(Rect r, GUIStyle background)
    {
        if(pokemon.PokemonData == null)
        {
            base.OnInteractivePreviewGUI(r, background);
        } else
        {
            Texture2D icon = AssetDatabase.LoadAssetAtPath("Assets/Textures/Pokemons/Icons/" + pokemon.PokemonData.InternalName + ".png", typeof(Texture2D)) as Texture2D;
            if (icon == null)
            {
                base.OnInteractivePreviewGUI(r, background);
            }
            else
            {
                GUI.DrawTexture(r, icon, ScaleMode.ScaleToFit);
            }
        }

    }

    public override void OnPreviewGUI(Rect r, GUIStyle background)
    {
        if (pokemon.PokemonData == null)
        {
            base.OnPreviewGUI(r, background);
        }
        else
        {
            Texture2D icon = AssetDatabase.LoadAssetAtPath("Assets/Textures/Pokemons/Icons/" + pokemon.PokemonData.InternalName + ".png", typeof(Texture2D)) as Texture2D;
            if (icon == null)
            {
                base.OnInteractivePreviewGUI(r, background);
            }
            else
            {
                GUI.DrawTexture(r, icon, ScaleMode.ScaleToFit);
            }
        }
    }
    #endregion

    private int selectedAbility;
    private bool showMoveset = true;
    private bool showEVs = true;
    private bool showIVs = true;
    private bool showStats = false;

    public override void OnInspectorGUI()
    {
        pokemon.RecalculateStats();
        serializedObject.Update();
        EditorGUILayout.LabelField("Pokemon");
        EditorGUILayout.Separator();
        EditorGUILayout.PropertyField(serializedObject.FindProperty("pokemonData"));

        if(pokemon.PokemonData != null)
        {
            DrawUILine(Color.grey);
            if(serializedObject.FindProperty("ability").objectReferenceValue == null) {
                selectedAbility = 0;
            } else if (serializedObject.FindProperty("ability").objectReferenceValue.Equals(pokemon.PokemonData.SecondaryAbility)) {
                selectedAbility = 1;
            } else if (serializedObject.FindProperty("ability").objectReferenceValue.Equals(pokemon.PokemonData.HiddenAbility))
            {
                selectedAbility = 2;
            } else
            {
                selectedAbility = 0;
            }
            EditorGUILayout.PropertyField(serializedObject.FindProperty("hasNickname"));
            EditorGUI.BeginDisabledGroup(!serializedObject.FindProperty("hasNickname").boolValue);
            EditorGUILayout.PropertyField(serializedObject.FindProperty("nickname"));
            EditorGUI.EndDisabledGroup();
            int level = EditorGUILayout.IntField("Level", serializedObject.FindProperty("level").intValue);
            int currentExp = EditorGUILayout.IntField("Current Exp", serializedObject.FindProperty("currentExp").intValue);
            pokemon.Level = level;
            pokemon.CurrentExp = currentExp;

            EditorGUI.BeginDisabledGroup(true);
            EditorGUILayout.PropertyField(serializedObject.FindProperty("totalExp"));
            EditorGUI.EndDisabledGroup();

            EditorGUILayout.PropertyField(serializedObject.FindProperty("friendship"));
            EditorGUILayout.PropertyField(serializedObject.FindProperty("female"));
            EditorGUILayout.PropertyField(serializedObject.FindProperty("isShiny"));

            string[] abilityOptions = new string[] {
                pokemon.PokemonData.PrimaryAbility != null ? pokemon.PokemonData.PrimaryAbility.name : "None",
                pokemon.PokemonData.SecondaryAbility != null ? pokemon.PokemonData.SecondaryAbility.name : "None",
                pokemon.PokemonData.HiddenAbility != null ? pokemon.PokemonData.HiddenAbility.name : "None"
            };
            selectedAbility = EditorGUILayout.Popup("Ability", selectedAbility, abilityOptions);
            if (selectedAbility == 0)
            {
                serializedObject.FindProperty("ability").objectReferenceValue = pokemon.PokemonData.PrimaryAbility;
            }
            else if (selectedAbility == 1)
            {
                serializedObject.FindProperty("ability").objectReferenceValue = pokemon.PokemonData.SecondaryAbility;
            }
            else
            {
                serializedObject.FindProperty("ability").objectReferenceValue = pokemon.PokemonData.HiddenAbility;
            }

            DrawUILine(Color.grey);
            EditorGUILayout.PropertyField(serializedObject.FindProperty("status"));
            serializedObject.FindProperty("currentHealth").intValue = EditorGUILayout.IntSlider("Current Health", serializedObject.FindProperty("currentHealth").intValue, 0, pokemon.Stats.HP);

            if(GUILayout.Button("Heal")) {
                serializedObject.FindProperty("currentHealth").intValue = pokemon.Stats.HP;
                serializedObject.FindProperty("status").enumValueIndex = 0;
            }

            DrawUILine(Color.grey);
            showMoveset = EditorGUILayout.Foldout(showMoveset, "Moveset", true);
            if (showMoveset)
            {
                EditorGUI.indentLevel++;
                var array = serializedObject.FindProperty("moveset");
                if(array.arraySize != 4) array.arraySize = 4;
                for(int i = 0; i < 4; i++) {
                    var element = array.GetArrayElementAtIndex(i);
                    element.isExpanded = EditorGUILayout.Foldout(element.isExpanded, "Move " + (i + 1), true);
                    if(element.isExpanded) {
                        EditorGUI.indentLevel++;
                        SerializedProperty moveProp = element.FindPropertyRelative("move");
                        EditorGUI.BeginChangeCheck();
                        Move move = EditorGUILayout.ObjectField("Move", moveProp.objectReferenceValue, typeof(Move), false) as Move;
                        moveProp.objectReferenceValue = move;
                        EditorGUI.BeginDisabledGroup(move == null);
                        element.FindPropertyRelative("powerPointsUpgrades").intValue = EditorGUILayout.IntSlider("PP Upgrades", element.FindPropertyRelative("powerPointsUpgrades").intValue, 0, 3);
                        EditorGUI.EndDisabledGroup();
                        if(EditorGUI.EndChangeCheck()) {
                            if(move == null) {
                            element.FindPropertyRelative("powerPointsUpgrades").intValue = 0;
                            element.FindPropertyRelative("maxPowerPoints").intValue = 0;
                            element.FindPropertyRelative("powerPoints").intValue = 0;
                            } else {
                            int upgrades = element.FindPropertyRelative("powerPointsUpgrades").intValue;
                            element.FindPropertyRelative("maxPowerPoints").intValue = move.PowerPoints + (int) (0.2f * move.PowerPoints * upgrades);
                            element.FindPropertyRelative("powerPoints").intValue = element.FindPropertyRelative("maxPowerPoints").intValue;                                
                            }
                        }
                        EditorGUI.BeginDisabledGroup(true);
                        int maxPP = EditorGUILayout.IntField("Max PP", element.FindPropertyRelative("maxPowerPoints").intValue);
                        EditorGUI.EndDisabledGroup();
                        element.FindPropertyRelative("powerPoints").intValue = EditorGUILayout.IntSlider("Power Points", element.FindPropertyRelative("powerPoints").intValue, 0, maxPP);

                        EditorGUI.indentLevel--;
                    }
                }             
                EditorGUI.indentLevel--;
            }

            DrawUILine(Color.grey);
            showIVs = EditorGUILayout.Foldout(showIVs, "IVs", true);
            if (showIVs)
            {
                EditorGUI.indentLevel++;
                serializedObject.FindProperty("ivs.hp").intValue = EditorGUILayout.IntSlider("HP", serializedObject.FindProperty("ivs.hp").intValue, 0, 31);
                serializedObject.FindProperty("ivs.attack").intValue = EditorGUILayout.IntSlider("Attack", serializedObject.FindProperty("ivs.attack").intValue, 0, 31);
                serializedObject.FindProperty("ivs.defense").intValue = EditorGUILayout.IntSlider("Defense", serializedObject.FindProperty("ivs.defense").intValue, 0, 31);
                serializedObject.FindProperty("ivs.spAtk").intValue = EditorGUILayout.IntSlider("SpAtk", serializedObject.FindProperty("ivs.spAtk").intValue, 0, 31);
                serializedObject.FindProperty("ivs.spDef").intValue = EditorGUILayout.IntSlider("SpDef", serializedObject.FindProperty("ivs.spDef").intValue, 0, 31);
                serializedObject.FindProperty("ivs.speed").intValue = EditorGUILayout.IntSlider("Speed", serializedObject.FindProperty("ivs.speed").intValue, 0, 31);
                EditorGUI.indentLevel--;
            }
            DrawUILine(Color.grey);
            showEVs = EditorGUILayout.Foldout(showEVs, "EVs", true);
            if (showEVs)
            {
                EditorGUI.indentLevel++;
                pokemon.SetHPEVs(EditorGUILayout.IntSlider("HP", serializedObject.FindProperty("evs.hp").intValue, 0, 252));
                pokemon.SetAttackEVs(EditorGUILayout.IntSlider("Attack", serializedObject.FindProperty("evs.attack").intValue, 0, 252));
                pokemon.SetDefenseEVs(EditorGUILayout.IntSlider("Defense", serializedObject.FindProperty("evs.defense").intValue, 0, 252));
                pokemon.SetSpAtkEVs(EditorGUILayout.IntSlider("SpAtk", serializedObject.FindProperty("evs.spAtk").intValue, 0, 252));
                pokemon.SetSpDefEVs(EditorGUILayout.IntSlider("SpDef", serializedObject.FindProperty("evs.spDef").intValue, 0, 252));
                pokemon.SetSpeedEVs(EditorGUILayout.IntSlider("Speed", serializedObject.FindProperty("evs.speed").intValue, 0, 252));
                EditorGUI.BeginDisabledGroup(true);
                EditorGUILayout.IntField("Total", pokemon.GetTotalEVs());
                EditorGUI.EndDisabledGroup();
                EditorGUI.indentLevel--;
            }
            DrawUILine(Color.grey);
            showStats = EditorGUILayout.Foldout(showStats, "Stats", true);
            if (showStats)
            {
                EditorGUI.indentLevel++;
                EditorGUI.BeginDisabledGroup(true);
                EditorGUILayout.IntField("HPs", pokemon.Stats.HP);
                EditorGUILayout.IntField("Attack", pokemon.Stats.Attack);
                EditorGUILayout.IntField("Defense", pokemon.Stats.Defense);
                EditorGUILayout.IntField("SpAtk", pokemon.Stats.SpAtk);
                EditorGUILayout.IntField("SpDef", pokemon.Stats.SpDef);
                EditorGUILayout.IntField("Speed", pokemon.Stats.Speed);
                EditorGUI.EndDisabledGroup();
                EditorGUI.indentLevel--;
            }
        } 

        serializedObject.ApplyModifiedProperties();
    }

    public static void DrawUILine(Color color, int thickness = 2, int padding = 10)
    {
        Rect r = EditorGUILayout.GetControlRect(GUILayout.Height(padding + thickness));
        r.height = thickness;
        r.y += padding / 2;
        r.x -= 2;
        r.width += 6;
        EditorGUI.DrawRect(r, color);
    }


}
