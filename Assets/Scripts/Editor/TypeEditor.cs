﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using PokemonEngine.Data;

[CustomEditor(typeof(Type))]
public class TypeEditor : Editor {

    Type type;

    private void OnEnable()
    {
        type = target as Type;
    }

    #region GUI Preview
    public override Texture2D RenderStaticPreview(string assetPath, UnityEngine.Object[] subAssets, int width, int height)
    {
        if (type.Icon != null)
        {
            Texture2D newIcon = new Texture2D(width, height);
            EditorUtility.CopySerialized(type.Icon.texture, newIcon);
            return newIcon;
        }
        return base.RenderStaticPreview(assetPath, subAssets, width, height);
    }

    public override bool HasPreviewGUI()
    {
        return true;
    }

    public override void OnInteractivePreviewGUI(Rect r, GUIStyle background)
    {
        if(type.Icon == null)
        {
            base.OnInteractivePreviewGUI(r, background);
        } else
        {
            GUI.DrawTexture(r, type.Icon.texture, ScaleMode.ScaleToFit);
        }
    }

    public override void OnPreviewGUI(Rect r, GUIStyle background)
    {
        if (type.Icon == null)
        {
            base.OnPreviewGUI(r, background);
        }
        else
        {
            GUI.DrawTexture(r, type.Icon.texture, ScaleMode.ScaleToFit);
        }
    }
    #endregion


}
