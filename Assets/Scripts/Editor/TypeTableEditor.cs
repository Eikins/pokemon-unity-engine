using UnityEngine;
using UnityEditor;
using UnityEditor.Callbacks;
using PokemonEngine.Data;
using System.Collections.Generic;

public class TypeTableEditor : EditorWindow {
    
    #region EditorWindow Callbacks

		[OnOpenAsset(1)]
		public static bool OpenWindow(int instanceID, int line) {
			var obj = EditorUtility.InstanceIDToObject(instanceID);
			if (obj is TypeTable) {
				var window = GetWindow<TypeTableEditor>();
				window.typeTable = (TypeTable)obj;
				window.titleContent = new GUIContent(obj.name);
				return true;
			} else {
				return false;
			}
		}

        private TypeTable typeTable;

        private List<Type> types;
        private float[][] multipliers;
        private bool reversed;

        private GUIStyle multiplierStyle;
        private GUIStyle labelStyle;
        private GUIStyle cornerStyle;
        private Texture2D cornerIcon;

        private Texture2D cornerTexture;
        private Texture2D cornerTextureReversed;

        private static readonly string labelAttackerSide = "A\nt\nt\na\nc\nk\ni\nn\ng\n \nT\ny\np\ne";
        private static readonly string labelTargetSide = "T\na\nr\ng\ne\nt\n \nT\ny\np\ne";
        private static readonly string labelAttackerTop = "Attacking Type";
        private static readonly string labelTargetTop = "Target Type";

        private string labelTop;
        private string labelSide;

		private void OnEnable() {
            types = new List<Type>();

            string[] guids = AssetDatabase.FindAssets("t:Type", new string[] {"Assets/Data/Types"});

            foreach (string guid in guids) {
                Type type = AssetDatabase.LoadAssetAtPath(AssetDatabase.GUIDToAssetPath(guid), typeof(Type)) as Type;
                types.Add(type);
            }

            cornerTexture = EditorGUIUtility.Load("Textures/type_table_corner.png") as Texture2D;
            cornerTextureReversed = EditorGUIUtility.Load("Textures/type_table_corner_reversed.png") as Texture2D;
            reversed = false;
            labelSide = labelAttackerSide;
            labelTop = labelTargetTop;

            cornerIcon = cornerTexture;
            labelStyle = new GUIStyle();
            labelStyle.fontSize = 20;
            labelStyle.alignment = TextAnchor.MiddleCenter;

            minSize = new Vector2(44 * (types.Count + 1), 42 * (types.Count + 1)) + new Vector2(32, 32);
            maxSize = minSize;
            wantsMouseMove = true;

		}


		private void OnDisable() {
			EditorUtility.SetDirty(typeTable);
			AssetDatabase.SaveAssets();
			AssetDatabase.Refresh();
		}

		private void OnDestroy() {
			EditorUtility.SetDirty(typeTable);
			AssetDatabase.SaveAssets();
			AssetDatabase.Refresh();
		}

		private void OnGUI() {


            if(multiplierStyle == null) {
                multiplierStyle = new GUIStyle(GUI.skin.GetStyle("textField"));
                multiplierStyle.fontSize = 16;
                multiplierStyle.alignment = TextAnchor.MiddleCenter;
            }

            Vector2 mousePosition = Event.current.mousePosition;

            GUILayout.BeginHorizontal();
            GUILayout.Label(labelSide, labelStyle, GUILayout.Width(30), GUILayout.Height(42 * (types.Count + 1) + 32));
            GUILayout.BeginVertical();
            GUILayout.Label(labelTop, labelStyle, GUILayout.Width(32 + 44 * (types.Count + 1)), GUILayout.Height(30));
            GUILayout.BeginHorizontal();
            if(GUILayout.Button(cornerIcon, GUILayout.Width(40), GUILayout.Height(40))) {
                Reverse();
            }
            for(int x = 0; x < types.Count; x++) {
                if(mousePosition.x >= (x + 1) * 44 + 32 && mousePosition.x < (x + 2) * 44 + 32) {
                    GUI.backgroundColor = Color.yellow;
                }
                GUILayout.Box(GetTypeTexture(types[x]), GUILayout.Width(40), GUILayout.Height(40));
                GUI.backgroundColor = Color.white;
            }
            GUILayout.EndHorizontal();

            for(int y = 0; y < types.Count; y++) {

                GUILayout.BeginHorizontal();
                if(mousePosition.y >= (y + 1) * 42 + 32 && mousePosition.y < (y + 2) * 42 + 32) {
                    GUI.backgroundColor = Color.yellow;
                }
                GUILayout.Box(GetTypeTexture(types[y]), GUILayout.Width(40), GUILayout.Height(40));
                GUI.backgroundColor = Color.white;
                for(int x = 0; x < types.Count; x++) {
                    Type attacker = reversed ? types[x] : types[y];
                    Type target = reversed ? types[y] : types[x];
                    float mult = typeTable.Get(attacker, target);
                    if(mult <= 0f) {
                        GUI.backgroundColor = Color.gray;
                    } else if(mult < 1f) {
                        GUI.backgroundColor = Color.red;
                    } else if(mult > 1f) {
                        GUI.backgroundColor = Color.green;
                    }
                    typeTable.Set(attacker, target, EditorGUILayout.FloatField(mult, multiplierStyle, GUILayout.Width(40), GUILayout.Height(40)));
                    GUI.backgroundColor = Color.white;
                }
                GUILayout.EndHorizontal();
            }

            GUILayout.EndVertical();
            GUILayout.EndHorizontal();
		}

		#endregion

        private Texture2D GetTypeTexture(Type type) {
            Texture2D icon = AssetDatabase.LoadAssetAtPath("Assets/Textures/Types/" + type.InternalName + ".png", typeof(Texture2D)) as Texture2D;
            return icon;
        }

        private void Reverse() {
            reversed = ! reversed;
            if(reversed) {
                cornerIcon = cornerTextureReversed;
                labelSide = labelTargetSide;
                labelTop = labelAttackerTop;
            } else {
                cornerIcon = cornerTexture;
                labelSide = labelAttackerSide;
                labelTop = labelTargetTop;
            }
        }
}