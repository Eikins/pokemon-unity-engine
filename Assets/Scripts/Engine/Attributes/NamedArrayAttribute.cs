﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NamedArrayAttribute : PropertyAttribute
{

    public readonly string name;

    public NamedArrayAttribute(string name)
    {
        this.name = name;
    }
}