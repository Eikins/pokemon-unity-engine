using System;
using UnityEngine;
using UnityEngine.Playables;

namespace Pokemon3D.Animation {

    public class PokemonAnimationAsset : PlayableAsset
    {

        public PokemonAnimation animation;

        public override double duration {
            get {
                return animation.GetDuration();
            }
        }

        public override Playable CreatePlayable (PlayableGraph graph, GameObject owner)
        {
            var playable = ScriptPlayable<PokemonAnimationBehaviour>.Create(graph);
            
            var behaviour = playable.GetBehaviour();
            behaviour.firstFrame = true;
            behaviour.animation = animation;
        
            return playable;   
        }


    }

    [Serializable]
    public enum PokemonAnimation {
        PhysicalAttack,
        PhysicalAttackVariant,
        SpecialAttack,
        SpecialAttackVariant,
        Status,
        Hurt
    }

    public static class PokemonAnimationMethods
    {

        public static double GetDuration(this PokemonAnimation animation)
        {
            switch (animation)
            {
                case PokemonAnimation.Hurt:
                    return 1d / 3d;
                default:
                    return 4d / 3d;
            }
        }

    }

}

