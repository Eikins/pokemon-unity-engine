using UnityEngine;
using System;
using UnityEngine.Playables;

namespace Pokemon3D.Animation {

    public class PokemonAnimationBehaviour : PlayableBehaviour {

        public bool firstFrame;
        public PokemonAnimation animation;
        
        public override void ProcessFrame(Playable playable, FrameData info, object playerData) {

            if(firstFrame) {
                var animator = playerData as Animator;
                if(animator != null) {
                    animator.SetTrigger(Enum.GetName(typeof(PokemonAnimation), animation));
                }

            }

            firstFrame = false;
        }
    }

}

