using UnityEngine;
using UnityEngine.Timeline;

namespace Pokemon3D.Animation {

    [TrackClipType(typeof(PokemonAnimationAsset))]
    [TrackBindingType(typeof(Animator))]
    public class PokemonAnimationTrack : TrackAsset {}
    
}

