using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

namespace Pokemon3D.Animation {

    public class VFXControlAsset : PlayableAsset
    {

        public ExposedReference<GameObject> prefab;
        public Anchor attached;
        public PokemonAnchor anchor;
        public bool followMovement;
        public bool staticRotation;
        public Vector3 offset;

        public override Playable CreatePlayable (PlayableGraph graph, GameObject owner)
        {
            var playable = ScriptPlayable<VFXControlBehaviour>.Create(graph);
            
            var behaviour = playable.GetBehaviour();
            behaviour.firstFrame = true;
            behaviour.prefab = prefab.Resolve(graph.GetResolver());
            behaviour.offset = offset;
            behaviour.anchor = anchor;
            behaviour.attached = attached;
            behaviour.staticRotation = staticRotation;
        
            return playable;   
        }

    }

    [Serializable]
    public enum Anchor {
        Source,
        Target
    }

    [Serializable]
    public enum PokemonAnchor {
        None,
        Waist,
        Head
    }

    public static class PokemonAnchorMethods
    {

        public static Transform Get(this PokemonAnchor anchor, Transform parent)
        {
            switch (anchor)
            {
                case PokemonAnchor.Waist:
                    return parent.FindChildRecursively("Waist");
                case PokemonAnchor.Head:
                    return parent.FindChildRecursively("Head");
                default:
                    return parent;
            }
        }

        public static Transform FindChildRecursively(this Transform aParent, string aName)
        {
            Queue<Transform> queue = new Queue<Transform>();
            queue.Enqueue(aParent);
            while (queue.Count > 0)
            {
                var c = queue.Dequeue();
                if (c.name == aName)
                    return c;
                foreach(Transform t in c)
                    queue.Enqueue(t);
            }
            return null;
        } 

    }

}

