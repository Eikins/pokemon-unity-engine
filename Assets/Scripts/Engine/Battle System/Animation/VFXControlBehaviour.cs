using UnityEngine;
using System;
using System.Collections.Generic;
using UnityEngine.Playables;

namespace Pokemon3D.Animation {

    public class VFXControlBehaviour : PlayableBehaviour {

        public bool firstFrame;
        public GameObject prefab;
        public Vector3 offset;
        public PokemonAnchor anchor;
        public Anchor attached;
        public bool followMovement;
        public bool staticRotation;
        
        public override void ProcessFrame(Playable playable, FrameData info, object playerData) {

            if(firstFrame) {
                var parent = playerData as Transform;
                if(parent != null) {
                    if(attached == Anchor.Target) {
                        var p = parent.parent;
                        foreach (Transform child in p) {
                            if(child != parent) {
                                parent = child;
                                break;
                            }
                        }
                    }
                    var transform = anchor.Get(parent);
                    GameObject go;

                    if(followMovement) {
                        go = UnityEngine.Object.Instantiate(prefab, transform);
                    } else {
                        go = UnityEngine.Object.Instantiate(prefab, transform.position, staticRotation ? parent.rotation : transform.rotation);
                    }
                    go.transform.localPosition += go.transform.rotation * offset;
                    GameObject.Destroy(go, (float) playable.GetDuration());
                }
            }

            firstFrame = false;
        }
   
    }    

}

