using UnityEngine;
using UnityEngine.Timeline;

namespace Pokemon3D.Animation {

    [TrackClipType(typeof(VFXControlAsset))]
    [TrackBindingType(typeof(Transform))]
    public class VFXControlTrack : ControlTrack {}
    
}

