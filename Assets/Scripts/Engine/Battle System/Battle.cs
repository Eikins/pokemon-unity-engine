using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PokemonEngine.BattleSystem.Events;
using PokemonEngine.Data;
using PokemonEngine.Localization;

namespace PokemonEngine.BattleSystem
{

    public class Battle
    {

        private delegate void StateUpdate();
        private StateUpdate UpdateCurrentState { get; set; }

        public EventManager EventManager { get; }
        public TypeTable TypeTable { get; }

        private SortedList<int, Effect> effects; 

        public IEnumerable<Effect> Effects => effects.Values;

        // TODO: private void TerrainWeather terrainWeather;

        public MonoBehaviour Parent { get; }
        public BattleCallbackReceiver Animator { get; }
        private BattleActor Player { get; }
        private BattleActor Opponent { get; }
        public IReadOnlyList<TargetPoint> TargetPoints => targetPoints;
        public Dictionary<BattleActor, IReadOnlyList<TargetPoint>> ActorTargetPoints => actorTargetPoints;
        public Dictionary<PokemonInstance, TargetPoint> PokemonTargetPoints => pokemonTargetPoints;

        private IEnumerator<BattleAction> turnActions;
        private Dictionary<TargetPoint, BattleAction> actions;

        private List<TargetPoint> targetPoints;
        private Dictionary<PokemonInstance, TargetPoint> pokemonTargetPoints;
        private Dictionary<BattleActor, IReadOnlyList<TargetPoint>> actorTargetPoints;

        private bool pause, end;

        public Battle(MonoBehaviour parent, BattleCallbackReceiver receiver, BattleActor player, BattleActor opponent, TypeTable typeTable) {
            EventManager = new EventManager(this);
            EventManager.RegisterListener(this);
            TypeTable = typeTable;
            Parent = parent;
            Animator = receiver;
            Player = player;
            Opponent = opponent;

            actions = new Dictionary<TargetPoint, BattleAction>();
            pokemonTargetPoints = new Dictionary<PokemonInstance, TargetPoint>();
            actorTargetPoints = new Dictionary<BattleActor, IReadOnlyList<TargetPoint>>();

            targetPoints = new List<TargetPoint>();
            var playerTargetPoint = new TargetPoint(player);
            var opponentTargetPoint = new TargetPoint(opponent); 

            targetPoints.Add(playerTargetPoint);
            targetPoints.Add(opponentTargetPoint);
            
            actorTargetPoints.Add(player, new List<TargetPoint> {playerTargetPoint});
            actorTargetPoints.Add(opponent, new List<TargetPoint> {opponentTargetPoint});

            effects = new SortedList<int, Effect>();
            pause = false;
            UpdateCurrentState = UpdateInitState;
        }

        public void Resume() {
            pause = false;
        }

        public void Pause() {
            pause = true;
        }

        public void Update() {
            if(!pause) {
                UpdateCurrentState();
            }
        }

        public void UpdateInitState() {
            StartBlockingCoroutine(Init());
            UpdateCurrentState = UpdateSelectionState;
        }

        public void UpdateSelectionState() {
            // Wait for player to choose move

            var complete = true;
            foreach (var targetPoint in targetPoints) {
                if(!targetPoint.IsEmpty() && !actions.ContainsKey(targetPoint)) {
                    var action = targetPoint.Actor.GetAction(this);
                    if(action != null) {
                        action.SetSource(targetPoint);
                        if (action.IsValid(this)) {
                            actions[targetPoint] = action;
                        } else {
                            targetPoint.Actor.NotifyInvalidAction(action);
                            complete = false;
                        }
                    } else {
                        complete = false;
                    }
                }
            }

            if(complete) {
                UpdateCurrentState = UpdateTurnBeginState;
            }
        }

        public void UpdateTurnBeginState() {
            DetermineTurnOrder();
            UpdateCurrentState = UpdateMoveState;
        }
        
        public void UpdateTurnEndState() {
            StartBlockingCoroutine(TurnEndSequence());
            UpdateCurrentState = UpdateTurnPostEndState;
        }

        public void UpdateTurnPostEndState() {
            if(end) {
                UpdateCurrentState = UpdateBattleEndState;
            } else {
                actions.Clear();
                Player.OnSelectionStart();
                UpdateCurrentState = UpdateSelectionState;
            }
        }
        
        public void UpdateMoveState() {
            var action = turnActions.Current;
            if(!action.Source.IsEmpty()) {
                StartBlockingCoroutine(action.Execute(this));
            }
            UpdateCurrentState = UpdateMovePerformedState;
        }

        public void UpdateMovePerformedState() {
            if(turnActions.MoveNext()) {
                UpdateCurrentState = UpdateMoveState;
            } else {
                UpdateCurrentState = UpdateTurnEndState;
            }
        }

        public void UpdateBattleEndState() {
            var handler = BattleHandler.Instance;
            handler.EndBattle();
            Pause();
        }

        private void StartBlockingCoroutine(IEnumerator routine) {
            pause = true;
            Parent.StartCoroutine(BlockingRoutine(routine));
        }

        private IEnumerator BlockingRoutine(IEnumerator routine) {
            yield return routine;
            Resume();
        }

        private void DetermineTurnOrder() {
            var order = new List<BattleAction>(actions.Values);
            // Reverse sort
            order.Sort((a, b) => -1 * a.CompareTo(b));
            turnActions = order.GetEnumerator();
            turnActions.MoveNext();
        }

        public bool CanSwitch(BattleActor actor, TargetPoint targetPoint, int index) {
            return index >= 0 && 
            index < 6 && actor.GetTeam()[index] != null && 
            !actor.GetTeam()[index].IsKnockout() && 
            (targetPoint.IsEmpty() || actor.GetTeam()[index] != targetPoint.Pokemon);
        }

        public IEnumerator Switch(BattleActor actor, TargetPoint targetPoint, int index) {
            if (CanSwitch(actor, targetPoint, index)) {
                var e = new BeforeSwitchEvent(actor, targetPoint, index);
                EventManager.TriggerEvent(e);
                yield return e.GetSequence();
                if(!e.Canceled) {
                    var pokemon = targetPoint.PokemonInstance;
                    yield return targetPoint.SetPokemon(new PokemonInstance(this, actor.GetTeam()[index]));
                    if (pokemon != null) {
                        pokemonTargetPoints.Remove(pokemon);
                    }
                    pokemonTargetPoints[targetPoint.PokemonInstance] = targetPoint;

                    yield return Animator.PerformSwitchAnimation(actor, index, targetPoint.Pokemon);
                }
            }
        }

        public IEnumerator Init() {
            int index = FirstAvalaiblePokemon(Opponent);
            yield return Switch(Opponent, actorTargetPoints[Opponent][0], index);
            index = FirstAvalaiblePokemon(Player);
            yield return Switch(Player, actorTargetPoints[Player][0], index);
            Player.OnSelectionStart();
        }

        public int FirstAvalaiblePokemon(BattleActor actor) {
            for (int i = 0; i < 6; i++) {
                if (actor.GetTeam()[i] != null && !actor.GetTeam()[i].IsKnockout()) return i;
            }
            return 0;
        }

        [EventReceiver]
        public void OnPokemonKnockout(PokemonKnockoutEvent e) {
            e.AppendSequence(Animator.PerformDeathAnimation(e.PokemonInstance.Pokemon));
            e.AppendSequence(e.PokemonInstance.TargetPoint.SetPokemon(null));
        }

        [EventReceiver]
        public void OnPokemonSpawn(PokemonSpawnEvent e) {
            var pokemon = e.TargetPoint.Pokemon;
            if(pokemon.Status != Status.None) {
                e.TargetPoint.AddEffect(pokemon.Status.CreateBattleEffect(pokemon.StatusState));
            }
        }

        public IEnumerator TurnEndSequence() {
            foreach (var targetPoint in targetPoints) {
                var e = new TurnEvent.End(targetPoint);
                EventManager.TriggerEvent(e);
                yield return e.GetSequence();
            }
            foreach (var targetPoint in targetPoints) {

                if(targetPoint.IsEmpty()) {
                    if(targetPoint.Actor.GetTeam().HasPokemonAlive()) {
                        int i;
                        do {
                            while((i = targetPoint.Actor.GetNextPokemon()) == -1) {
                                yield return null;
                            }
                        } while (!CanSwitch(targetPoint.Actor, targetPoint, i));
                        yield return Switch(targetPoint.Actor, actorTargetPoints[targetPoint.Actor][0], i);
                    } else {
                        end = true;
                    }
                }
            }
        }

        [EventReceiver]
        public void OnPokemonDamage(PokemonDamageEvent.Post e) {
            e.AppendSequence(DamageSequence(e.PokemonInstance, e.DamageSource));
        }

        public IEnumerator DamageSequence(PokemonInstance pokemon, DamageSource source) {
            yield return Animator.PerformHurtAnimation(pokemon.Pokemon);

            if (source.TypeMultiplier > 1.0f) {
                yield return Animator.ShowFormattedText(I18n.Translate("Battle.VeryEffective"));
            } else if (source.TypeMultiplier == 0.0f) {
                yield return Animator.ShowFormattedText(I18n.Translate("Battle.Uneffective"), pokemon.Pokemon.DisplayName);
            } else if (source.TypeMultiplier < 1.0f) {
                yield return Animator.ShowFormattedText(I18n.Translate("Battle.NotEffective"));
            }

            if (source.Critical) {
                yield return Animator.ShowFormattedText(I18n.Translate("Battle.Critical"));
            }
        }

    }

}
