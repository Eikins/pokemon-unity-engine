using System;
using System.Collections;
using PokemonEngine.Data;
using PokemonEngine.BattleSystem.Events;
using PokemonEngine.Localization;
using UnityEngine;

namespace PokemonEngine.BattleSystem {

    public abstract class BattleAction : IComparable<BattleAction> {

        private bool interrupted = false;
        private TargetPoint source;
        private BattleActor actor;

        public TargetPoint Source => source;
        public BattleActor Actor => source.Actor;
        public TargetPoint Target;

        public BattleAction(TargetPoint target) {
            Target = target;
        }

        public IEnumerator Execute(Battle battle) {
                var sequence = Sequence(battle);
                while(sequence.MoveNext()) {
                    if(interrupted) break;
                    yield return sequence.Current;
                }
        }

        public virtual void SetSource(TargetPoint source) {
            this.source = source;
        }

        public void SetActor(BattleActor actor) {
            this.actor = actor;
        }

        public void Interrupt() {
            interrupted = true;
        }

        public abstract bool IsValid(Battle battle);

        public abstract int GetPriority();

        public abstract IEnumerator Sequence(Battle battle);

        public int CompareTo(BattleAction other) {
            var priority = GetPriority().CompareTo(other.GetPriority());
            if (priority == 0) {
                var speed = Source.PokemonInstance.Statistics.Speed.CompareTo(Target.PokemonInstance.Statistics.Speed);
                if(speed == 0) {
                    return UnityEngine.Random.value < 0.5f ? -1 : 1;
                } else {
                    return speed;
                }
            } else {
                return priority;
            }
        }
    }

    public class UseItemAction : BattleAction {

        public UseItemAction(TargetPoint target) : base(target) {}

        public override int GetPriority() {
            return 100;
        }

        public override bool IsValid(Battle battle) {
            return true;
        }

        public override IEnumerator Sequence(Battle battle) {
            return null;
        }
    }

    public class SwitchPokemonAction : BattleAction {

        public int Index { get; }

        public SwitchPokemonAction(TargetPoint target, int index) : base(target) {
            Index = index;
        }

        public override int GetPriority() {
            return 200;
        }

        public override bool IsValid(Battle battle) {
            return battle.CanSwitch(Source.Actor, Target, Index);
        }

        public override IEnumerator Sequence(Battle battle) {
            return battle.Switch(Actor, Target, Index);
        }
    }

    public class UseMoveAction : BattleAction {

        public MoveInfo MoveInfo => moveInfo;
        public Move Move => moveInfo.Move;
        public MoveBehaviour Behaviour => behaviour;
        public int Index { get; }
        public Battle Battle => battle;

        private Battle battle;
        private MoveInfo moveInfo;
        private MoveBehaviour behaviour;

        public UseMoveAction(int index, TargetPoint target) : base(target) {
            Index = index;
        }

        public override void SetSource(TargetPoint source) {
            base.SetSource(source);
            moveInfo = source.Pokemon.Moveset[Index];
            System.Type moveClassType = System.Type.GetType("PokemonEngine.BattleSystem.Moves." + Move.BehaviourClass);
            if(!(typeof(MoveBehaviour)).IsAssignableFrom(moveClassType)) {
                // TODO handle exception
            }
            behaviour = (MoveBehaviour) System.Activator.CreateInstance(moveClassType, new object[] { this });
        }

        public override bool IsValid(Battle battle) {
            return true;
        }

        public override int GetPriority() {
            return Move.Priority;
        }

        public override IEnumerator Sequence(Battle battle) {
            this.battle = battle;
            // Check if the move should be overrided. For example, caused by "Encore" move effect
            var overrideMoveEvent = new OverrideMoveEvent(Source, Target, Move);
            battle.EventManager.TriggerEvent(overrideMoveEvent);
            if(overrideMoveEvent.NewMove != overrideMoveEvent.Move) {
                // Handle
            }

            // Called before the move execution. Execution can be aborted.
            var beforeMoveEvent = new BeforeMoveEvent(Source, Target, Move);
            battle.EventManager.TriggerEvent(beforeMoveEvent);
            yield return beforeMoveEvent.GetSequence();
            if(beforeMoveEvent.Aborted) {
                // Called when a move is aborted.
                var moveAbortedEvent = new MoveAbortedEvent(Source, Target, Move);
                battle.EventManager.TriggerEvent(moveAbortedEvent);
                yield break;
            }

            // Call to check if the move should be locked.
            var lockMoveEvent = new LockMoveEvent(Source, Target, Move);
            battle.EventManager.TriggerEvent(lockMoveEvent);
            if(lockMoveEvent.Lock) {
                // TODO: Handle lock move event
            }

            yield return battle.Animator.ShowFormattedText(I18n.Translate("Battle.UseMove"), Source.PokemonInstance.Pokemon.DisplayName, Move.DisplayName);

            moveInfo.PowerPoints--;

            var hitResult = Behaviour.CanHit();

            if(hitResult == HitResult.SUCCESS) {
                yield return Behaviour.RunSequence();
            } else if(hitResult == HitResult.DODGED) {
                yield return battle.Animator.ShowFormattedText(I18n.Translate("Battle.Dodge"), Target.PokemonInstance.Pokemon.DisplayName);
            } else if(hitResult == HitResult.FAILED) {
                yield return battle.Animator.ShowFormattedText(I18n.Translate("Battle.Fail"));
            }
        }

    }

}