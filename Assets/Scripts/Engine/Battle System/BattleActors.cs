using System.Collections;
using UnityEngine;
using PokemonEngine.Data;

namespace PokemonEngine.BattleSystem {

    public interface BattleCallbackReceiver {

        IEnumerator ShowText(string key);

        IEnumerator ShowFormattedText(string key, params string[] formats);

        IEnumerator PerformMoveAnimation(Move move, Pokemon attacker, Pokemon target);

        IEnumerator PerformHurtAnimation(Pokemon target);

        IEnumerator PerformDeathAnimation(Pokemon target);

        IEnumerator PerformStatModificationAnimation(Pokemon pokemon, params StatModifier[] modifiers);

        IEnumerator PerformSwitchAnimation(BattleActor actor, int index, Pokemon newPokemon);

        void RefreshPokemonStatus(Pokemon pokemon);

    }

    public interface BattleActor {

        Team GetTeam();

        void OnSelectionStart();

        BattleAction GetAction(Battle battle);

        int GetNextPokemon();

        void NotifyInvalidAction(BattleAction action);

    }

    public class BattleAISimple : BattleActor {

        private Team pokemons;

        public BattleAISimple(Team pokemons) {
            this.pokemons = pokemons;
        }

        public Team GetTeam() {
            return pokemons;
        }

        public void OnSelectionStart() {}

        public BattleAction GetAction(Battle battle) {
            var pokemon = battle.ActorTargetPoints[this][0].Pokemon;
            int moveCount = 0;
            while(moveCount < 4 && pokemon.Moveset[moveCount].Move != null) {
                moveCount++;
            }
            TargetPoint target = null;
            foreach (var targetPoint in battle.TargetPoints) {
                if(targetPoint.Actor != this) {
                    target = targetPoint;
                    break;
                }
            }
            return new UseMoveAction(Random.Range(0, moveCount), target);
        }

        public int GetNextPokemon() {
            for (int i = 0; i < 6; i++) {
                if (pokemons[i] != null && !pokemons[i].IsKnockout()) return i;
            }
            return -1; // never happens anyway
        }

        public void NotifyInvalidAction(BattleAction action) {}
    }

}