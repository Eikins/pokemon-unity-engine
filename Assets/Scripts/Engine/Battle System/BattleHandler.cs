﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using PokemonEngine.Data;
using Cinemachine;

namespace PokemonEngine.BattleSystem {

    public class BattleHandler : MonoBehaviour
    {

        private static BattleHandler shared;

        public static BattleHandler Instance => shared;

        public Team allyTeam;
        public Trainer trainer;
        [HideInInspector] public int previousSceneIndex;

        private Vector3 position;
        private Quaternion rotation;
        private Vector2 cameraPosition;

        public void StartBattle() {
            trainer.Team.Heal();
            GameObject.Find("AudioMaster").GetComponent<AudioSource>().Stop();
            GetComponent<AudioSource>().Play();
            var player = GameObject.FindGameObjectWithTag("Player");
            var freelook = player.GetComponentInChildren<CinemachineFreeLook>();
            cameraPosition = new Vector2(freelook.m_XAxis.Value, freelook.m_YAxis.Value);
            position = player.transform.position;
            rotation = player.transform.GetChild(0).rotation;
            DontDestroyOnLoad(this);
            shared = this;
            previousSceneIndex = SceneManager.GetActiveScene().buildIndex;
            Cursor.lockState = CursorLockMode.None;
            Initiate.Fade("BattleScene", Color.black, 0.5f);

        }

        public void EndBattle() {
            SceneManager.sceneLoaded += ResetPlayerTransformAndDestroy;
            Initiate.Fade(NameFromIndex(previousSceneIndex), Color.black, 1.5f);
        }

        public void ResetPlayerTransformAndDestroy(Scene scene, LoadSceneMode mode) {
            SceneManager.sceneLoaded -= ResetPlayerTransformAndDestroy;
            var player = GameObject.FindGameObjectWithTag("Player");
            player.transform.position = position;
            player.transform.GetChild(0).rotation = rotation;
            var freelook = player.GetComponentInChildren<CinemachineFreeLook>();
            freelook.m_XAxis.Value = cameraPosition.x;
            freelook.m_YAxis.Value = cameraPosition.y;
            GetComponent<AudioSource>().Stop();
            player.GetComponent<PlayerController>().Controllable = true;
            Destroy(gameObject);
        }

        private string NameFromIndex(int index) {
            string path = SceneUtility.GetScenePathByBuildIndex(index);
            int slash = path.LastIndexOf('/');
            string name = path.Substring(slash + 1);
            int dot = name.LastIndexOf('.');
            return name.Substring(0, dot);
        }

    }

}
