using PokemonEngine.Data;
using PokemonEngine.BattleSystem.Events;
using UnityEngine;

namespace PokemonEngine.BattleSystem {

    public class BattleStatistics : Statistics {

        public override int Attack {
            get {
                return ReadStat(Stats.Attack, base.Attack);
            }
        }

        public override int Defense {
            get {
                return ReadStat(Stats.Defense, base.Defense);
            }
        }

        public override int SpAtk {
            get {
                return ReadStat(Stats.SpAtk, base.SpAtk);
            }
        }

        public override int SpDef {
            get {
                return ReadStat(Stats.SpDef, base.SpDef);
            }
        }

        public override int Speed {
            get {
                return ReadStat(Stats.Speed, base.Speed);
            }
        }

        private PokemonInstance pokemonInstance;

        public BattleStatistics(PokemonInstance pokemonInstance) {
            this.pokemonInstance = pokemonInstance;
        }

        private int ReadStat(Stats stat, int value) {
            var e = new StatReadEvent(pokemonInstance, stat, value);
            pokemonInstance.Battle.EventManager.TriggerEvent(e);
            return e.Value;
        }

    }

}