﻿using PokemonEngine.Data;

namespace PokemonEngine.BattleSystem
{
    public class DamageSource
    {

        public int BaseDamage { get; set; }
        public int TotalDamage { get; set; }
        public float TypeMultiplier { get; set; }
        public bool Stab { get; set; }
        public bool Critical { get; set; }
        public Type EffectiveType {get;}

        public DamageSource(Type type, int damage)
        {
            BaseDamage = damage;
            Critical = false;
            TotalDamage = damage;
            TypeMultiplier = 1.0f;
            Stab = false;
            EffectiveType = type;
        }

        public DamageSource(int damage) : this(null, damage) {}

    }
}
