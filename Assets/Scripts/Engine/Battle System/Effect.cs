using System.Collections;
using PokemonEngine.Data;
using PokemonEngine.BattleSystem.Events;

namespace PokemonEngine.BattleSystem {

    public abstract class Effect {

        public Battle Battle => battle;
        public TargetPoint Target => target;

        private Battle battle;
        private TargetPoint target;

        public void SetBattle(Battle battle) {
            this.battle = battle;
        }

        public void SetTarget(TargetPoint target) {
            this.target = target;
        }

        public void Destroy() {
            Battle.EventManager.UnregisterListener(this);
        }

        public virtual void OnAdded() {}

    }

    public abstract class StatusEffect : Effect {

        public int State => state;

        private int state;

        public void SetState(int state) {
            this.state = state;
            Target.Pokemon.StatusState = state;
        }

        public virtual void OnStatusAdded() {}

        [EventReceiver]
        public void _OnTurnEnd(TurnEvent.End e) {
            if(e.TargetPoint == Target && e.TargetPoint.IsEmpty()) {
                Destroy();
            }
        }

        [EventReceiver(Priority = -10)]
        public void _OnBeforeSwitch(BeforeSwitchEvent e) {
            if(e.TargetPoint == Target) {
                Destroy();
            }
        }

        public IEnumerator Remove() {
            Destroy();
            yield return Target.RemoveStatus();
        }

    }

}