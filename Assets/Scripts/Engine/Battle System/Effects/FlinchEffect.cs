﻿using PokemonEngine.BattleSystem.Events;
using PokemonEngine.Localization;

namespace PokemonEngine.BattleSystem.Effects {

    public class FlinchEffect : Effect {

        [EventReceiver(Priority = 1)]
        public void OnBeforeMove(BeforeMoveEvent e) {
            if(e.Attacker == Target) {
                // Annule le coup
                e.Aborted = true;
                // Annule les autre beforeMoveEvent avec une priorité plus basse
                e.Canceled = true;
                e.AppendSequence(Battle.Animator.ShowFormattedText(I18n.Translate("Battle.Flinch"), e.Attacker.Pokemon.DisplayName));
                Destroy();
            }
        }

        [EventReceiver(Priority = 1)]
        public void OnTurnEnd(TurnEvent.End e) {
            Destroy();
        }

    }

}