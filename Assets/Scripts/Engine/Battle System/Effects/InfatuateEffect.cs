﻿using PokemonEngine.BattleSystem.Events;
using PokemonEngine.Localization;
using UnityEngine;

namespace PokemonEngine.BattleSystem.Effects
{

    public class InfatuateEffect : Effect
    {

        [EventReceiver(Priority = 1)]
        public void OnBeforeMove(BeforeMoveEvent e)
        {
            if (e.Attacker == Target && Random.value < 0.5)
            {
                // Annule le coup
                e.Aborted = true;
                // Annule les autre beforeMoveEvent avec une priorité plus basse
                e.Canceled = true;
                e.AppendSequence(Battle.Animator.ShowFormattedText(I18n.Translate("Battle.Infatuate"), e.Attacker.Pokemon.DisplayName));
                Destroy();
            }
        }

        [EventReceiver(Priority = 1)]
        public void BeforeSwitchEvent(BeforeSwitchEvent e)
        {
            Destroy();
        }

    }

}