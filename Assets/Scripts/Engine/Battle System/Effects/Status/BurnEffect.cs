﻿using UnityEngine;
using PokemonEngine.BattleSystem.Events;
using PokemonEngine.Localization;

namespace PokemonEngine.BattleSystem.Effects
{
    public class BurnEffect : StatusEffect
    {

        [EventReceiver(Priority = 1)]
        public void OnAttackReadValue(StatReadEvent e)
        {
            if (e.PokemonInstance == Target.PokemonInstance && e.Statistic == Stats.Attack) {
                e.Value /= 2;
            } 
        }

        [EventReceiver(Priority = 1)]
        public void OnTurnEnd(TurnEvent.End e)
        {
            if (!e.TargetPoint.IsEmpty() && e.TargetPoint == Target) {
                var pokemonInstance = Target.PokemonInstance;
                var damage = ((float) pokemonInstance.Statistics.HP) / 16f;
                e.AppendSequence(pokemonInstance.Hit(new DamageSource(Mathf.RoundToInt(damage))));
                e.AppendSequence(Battle.Animator.ShowFormattedText(I18n.Translate("Battle.Status.Burn.Damage"), pokemonInstance.Pokemon.DisplayName));
            }
        }
    }
}
