﻿using UnityEngine;
using PokemonEngine.BattleSystem.Events;
using PokemonEngine.Localization;

namespace PokemonEngine.BattleSystem.Effects
{
    public class FreezeEffect : StatusEffect
    {

        [EventReceiver(Priority = 1)]
        public void BeforeMove(BeforeMoveEvent e)
        {
            if (e.Attacker == Target) {
                var pokemonInstance = Target.PokemonInstance;
                if(Random.value < 0.2f) {
                    e.AppendSequence(Remove());
                } else {
                    e.Aborted = true;
                    e.Canceled = true;
                    e.AppendSequence(Battle.Animator.ShowFormattedText(I18n.Translate("Battle.Status.Freeze.Effect"), e.Attacker.Pokemon.DisplayName));
                }
            }
        }

        [EventReceiver(Priority = 1)]
        public void OnDamage(PokemonDamageEvent.Post e)
        {
            if (e.PokemonInstance == Target.PokemonInstance) {
                var type = e.DamageSource.EffectiveType;
                if(type != null && type.InternalName.Equals("Fire") && Random.value < 0.2f) {
                    e.AppendSequence(Remove());
                }
            }
        }

    }
}
