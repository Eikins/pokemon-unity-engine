﻿using UnityEngine;
using PokemonEngine.BattleSystem.Events;
using PokemonEngine.Localization;

namespace PokemonEngine.BattleSystem.Effects
{
    public class ParalysisEffect : StatusEffect
    {

        [EventReceiver(Priority = 1)]
        public void OnSpeedReadValue(StatReadEvent e)
        {
            if (e.PokemonInstance == Target.PokemonInstance && e.Statistic == Stats.Speed) {
                e.Value /= 2;
            } 
        }

        [EventReceiver(Priority = 1)]
        public void OnBeforeMove(BeforeMoveEvent e)
        {
            if (e.Attacker == Target && Random.value < 0.25f)
            {
                e.Aborted = true;
                e.Canceled = true;
                e.AppendSequence(Battle.Animator.ShowFormattedText(I18n.Translate("Battle.Status.Paralysis.Effect"), e.Attacker.Pokemon.DisplayName));
            }
        }
    }
}
