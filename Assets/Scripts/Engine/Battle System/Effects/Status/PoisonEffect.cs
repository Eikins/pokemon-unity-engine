﻿using UnityEngine;
using PokemonEngine.BattleSystem.Events;
using PokemonEngine.Localization;

namespace PokemonEngine.BattleSystem.Effects
{
    public class PoisonEffect : StatusEffect
    {

        [EventReceiver(Priority = 1)]
        public void OnTurnEnd(TurnEvent.End e)
        {
            if (!e.TargetPoint.IsEmpty() && e.TargetPoint == Target) {
                var pokemonInstance = Target.PokemonInstance;
                var damage = ((float) pokemonInstance.Statistics.HP) / 8f;
                e.AppendSequence(pokemonInstance.Hit(new DamageSource(Mathf.RoundToInt(damage))));
                e.AppendSequence(Battle.Animator.ShowFormattedText(I18n.Translate("Battle.Status.Poison.Damage"), pokemonInstance.Pokemon.DisplayName));
            }
        }
    }
}
