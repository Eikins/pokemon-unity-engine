﻿using UnityEngine;
using PokemonEngine.BattleSystem.Events;
using PokemonEngine.Localization;

namespace PokemonEngine.BattleSystem.Effects
{
    public class SleepEffect : StatusEffect
    {

        private int turnCounter;

        public SleepEffect() {
            turnCounter = 0;
        }

        public override void OnStatusAdded() {
            SetState(Random.Range(1, 4));
        }

        [EventReceiver(Priority = 1)]
        public void BeforeMove(BeforeMoveEvent e)
        {
            if (e.Attacker == Target) {
                var pokemonInstance = Target.PokemonInstance;
                if(turnCounter == State) {
                    e.AppendSequence(Remove());
                } else {
                    e.Aborted = true;
                    e.Canceled = true;
                    e.AppendSequence(Battle.Animator.ShowFormattedText(I18n.Translate("Battle.Status.Sleep.Effect"), e.Attacker.Pokemon.DisplayName));
                    turnCounter++;
                }
            }
        }

    }
}
