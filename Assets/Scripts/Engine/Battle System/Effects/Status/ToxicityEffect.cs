﻿using UnityEngine;
using PokemonEngine.BattleSystem.Events;
using PokemonEngine.Localization;

namespace PokemonEngine.BattleSystem.Effects
{
    public class ToxicityEffect : StatusEffect
    {

        [EventReceiver(Priority = 1)]
        public void OnTurnEnd(TurnEvent.End e)
        {
            if (e.TargetPoint == Target) {
                var pokemonInstance = Target.PokemonInstance;
                var damage = State * ((float) pokemonInstance.Statistics.HP) / 16f;
                e.AppendSequence(pokemonInstance.Hit(new DamageSource(Mathf.RoundToInt(damage))));
                e.AppendSequence(Battle.Animator.ShowFormattedText(I18n.Translate("Battle.Status.Toxicity.Damage"), pokemonInstance.Pokemon.DisplayName));
                SetState(State + 1);
            }
        }

        public override void OnAdded() {
            SetState(1);
        }
    }
}
