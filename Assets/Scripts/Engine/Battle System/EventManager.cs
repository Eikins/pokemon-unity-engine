﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Collections;
using System.Reflection;
using UnityEngine;

namespace PokemonEngine.BattleSystem.Events
{

    public class Event {

        public bool IsCancelable = false;
        public bool Canceled {
            get {
                return canceled;
            }
            set {
                if (IsCancelable) {
                    canceled = true;
                }
            }
        }

        private bool canceled;
        private List<IEnumerator> sequence = new List<IEnumerator>();
        

        public void AppendSequence(IEnumerator routine) {
            sequence.Add(routine);
        }

        public IEnumerator GetSequence() {
            foreach (var s in sequence) {
                yield return s;
            }
        }

    }

    [AttributeUsage(AttributeTargets.Method)]
    public class EventReceiverAttribute : Attribute
    {

        private int priority;

        public EventReceiverAttribute() {
            priority = 0;
        }

        public virtual int Priority {
            get {
                return priority;
            }
            set {
                priority = value;
            }
        }
    }

    public class EventListener {

        private object listener;
        private MethodInfo method;
        public Type EventType { get; }
        public int Priority { get; }

        public EventListener(object listener, MethodInfo method, Type eventType, EventReceiverAttribute attribute) {
            this.listener = listener;
            this.method = method;
            EventType = eventType;
            Priority = attribute.Priority;
        }

        public void Invoke(Event e) {
            method.Invoke(listener, new object[] {e});
        }

    }

    public class EventManager
    {

        public Battle Battle { get; }

        private bool lockRegistration;
        private Dictionary<object, bool> toRegister;
        private Dictionary<object, List<EventListener>> listeners;
        private Dictionary<Type, List<EventListener>> eventListeners;

        public EventManager(Battle battle) {
            Battle = battle;
            listeners = new Dictionary<object, List<EventListener>>();
            eventListeners = new Dictionary<Type, List<EventListener>>();
            toRegister = new Dictionary<object, bool>();
        }

        public void TriggerEvent(Event e) {
            LockRegistration();
            List<EventListener> list;
            if(eventListeners.TryGetValue(e.GetType(), out list))  {
                foreach (var l in list) {
                    l.Invoke(e);
                    if(e.Canceled) break;
                }
            }
            UnlockRegistration();
        }

        private void LockRegistration() {
            lockRegistration = true;
        }

        private void UnlockRegistration() {
            lockRegistration = false;
            foreach (var entry in toRegister) {
                if (entry.Value) {
                    RegisterListener(entry.Key);
                } else {
                    UnregisterListener(entry.Key);
                }
            }
            toRegister.Clear();
        }

        public void RegisterListener(object obj) {
            if (lockRegistration) {
                toRegister.Add(obj, true);
                return;
            }
            if(listeners.ContainsKey(obj)) return;

            var methods = obj.GetType().GetMethods().Where(method => method.GetCustomAttributes(typeof(EventReceiverAttribute), true).Length > 0);
            foreach (var method in methods) {
                var parameters = method.GetParameters();
                if(parameters.Length != 1) {
                    throw new System.ArgumentException(String.Format("EventReceiver method {0} should have only one paramater.", method.Name));
                }
                var attribute = (EventReceiverAttribute) method.GetCustomAttributes(typeof(EventReceiverAttribute), true)[0];
                var paramType = parameters[0].ParameterType;
                if(!typeof(Event).IsAssignableFrom(paramType)) {
                    throw new System.ArgumentException(String.Format("Method {0} has EventReceiver attribute but takes an argument that is not an Event. {1}", method.Name, parameters[0].GetType().Name));
                }
                RegisterListeningMethod(obj, method, attribute, paramType);
            }
        }

        private void RegisterListeningMethod(object obj, MethodInfo method, EventReceiverAttribute attribute, Type paramType) {
            var listener = new EventListener(obj, method, paramType, attribute);
            List<EventListener> list;
            if(listeners.TryGetValue(obj, out list)) {
                list.Add(listener);
            } else {
                list = new List<EventListener>();
                list.Add(listener);
                listeners.Add(obj, list);
            }

            if(eventListeners.TryGetValue(paramType, out list)) {
                list.Add(listener);
                // TODO : Swap to Priority Queue algorithm for faster registration.
                // But to be honest, this list will never have more than 20 elements and insertion is discret. (~1 every 5 seconds)
                // So... Who cares ? ¯\_(ツ)_/¯
                list.Sort((a, b) => -1 * a.Priority.CompareTo(b.Priority));
            } else {
                list = new List<EventListener>();
                list.Add(listener);
                eventListeners.Add(paramType, list);
            }
        }

        public void UnregisterListener(object obj) {
            if (lockRegistration) {
                toRegister.Add(obj, false);
                return;
            }
            List<EventListener> list;
            if(listeners.TryGetValue(obj, out list)) {
                foreach (var e in list) {
                    List<EventListener> l;
                    if(eventListeners.TryGetValue(e.EventType, out l)) {
                        l.Remove(e);
                    }
                }
            }

        }
    
    }

}

