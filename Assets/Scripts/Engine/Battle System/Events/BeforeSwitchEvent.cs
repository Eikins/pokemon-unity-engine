﻿namespace PokemonEngine.BattleSystem.Events
{
    public class BeforeSwitchEvent : Event {

        public TargetPoint TargetPoint {get;}
        public int SwitchIndex {get; set;}
        public BattleActor Actor;

        public BeforeSwitchEvent(BattleActor actor, TargetPoint targetPoint, int index) {
            TargetPoint = targetPoint;
            SwitchIndex = index;
            Actor = actor;
            IsCancelable = true;
        }

    }

}
