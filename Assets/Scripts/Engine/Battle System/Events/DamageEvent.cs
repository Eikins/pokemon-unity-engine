﻿namespace PokemonEngine.BattleSystem.Events
{

    public class PokemonDamageEvent {

        public class Pre : Event {

            public PokemonInstance PokemonInstance { get; }
            public DamageSource DamageSource { get; }

            public Pre(PokemonInstance pokemon, DamageSource source) {
                PokemonInstance = pokemon;
                DamageSource = source;
                IsCancelable = true;
            }

        }

        public class Post : Event {

            public PokemonInstance PokemonInstance { get; }
            public DamageSource DamageSource { get; }

            public Post(PokemonInstance pokemon, DamageSource source) {
                PokemonInstance = pokemon;
                DamageSource = source;
            }

        }

    }

    public class PokemonKnockoutEvent : Event {

        public PokemonInstance PokemonInstance { get; }

        public PokemonKnockoutEvent(PokemonInstance pokemon) {
            PokemonInstance = pokemon;
            IsCancelable = true;
        }

    }
}
