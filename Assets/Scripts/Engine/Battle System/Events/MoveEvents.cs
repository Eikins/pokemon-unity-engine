using PokemonEngine.Data;

namespace PokemonEngine.BattleSystem.Events {

    #region Events
    public class MoveEvent : Event {
        public TargetPoint Attacker { get; }
        public TargetPoint Target { get; }
        public Move Move { get; }

        public MoveEvent(TargetPoint attacker, TargetPoint target, Move move) {
            Attacker = attacker;
            Target = target;
            Move = move;
        }
    }

    public class BeforeMoveEvent : MoveEvent {
        public bool Aborted { get; set; }

        public BeforeMoveEvent(TargetPoint attacker, TargetPoint target, Move move) : base(attacker, target, move) {
            Aborted = false;
        }
    }

    public class MoveAbortedEvent : MoveEvent {
        public MoveAbortedEvent(TargetPoint attacker, TargetPoint target, Move move) : base(attacker, target, move) {}
    }

    public class OverrideMoveEvent : MoveEvent {

        public Move NewMove { get; set; }

        public OverrideMoveEvent(TargetPoint attacker, TargetPoint target, Move move) : base(attacker, target, move) {
            NewMove = move;
        }
    }

    public class LockMoveEvent : MoveEvent {
        public bool Lock { get; set; }

        public LockMoveEvent(TargetPoint attacker, TargetPoint target, Move move) : base(attacker, target, move) {
            Lock = false;
        }
    }

    public class MoveHitEvent : MoveEvent {

        public DamageSource DamageSource { get; }

        public MoveHitEvent(TargetPoint attacker, TargetPoint target, Move move, DamageSource damageSource) : base(attacker, target, move) {
            DamageSource = damageSource;
        }
    }
    #endregion
}
