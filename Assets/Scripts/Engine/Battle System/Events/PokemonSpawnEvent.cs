﻿namespace PokemonEngine.BattleSystem.Events
{
    public class PokemonSpawnEvent : Event {

        public TargetPoint TargetPoint {get;}

        public PokemonSpawnEvent(TargetPoint targetPoint) {
            TargetPoint = targetPoint;
        }

    }

}
