﻿namespace PokemonEngine.BattleSystem.Events
{
    public class StatReadEvent : Event {

        public PokemonInstance PokemonInstance { get; }
        public Stats Statistic { get; }
        public int Value { get; set; }

        public StatReadEvent(PokemonInstance pokemonInstance, Stats stat, int value) {
            PokemonInstance = pokemonInstance;
            Statistic = stat;
            Value = value;
        }
        
    }

}
