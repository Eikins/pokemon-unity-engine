﻿namespace PokemonEngine.BattleSystem.Events
{
    public class TurnEvent
    {

        public class Begin : Event {}

        public class End : Event {
            
            public TargetPoint TargetPoint { get; }

            public End(TargetPoint targetPoint) {
                TargetPoint = targetPoint;
            }
        }

    }

}
