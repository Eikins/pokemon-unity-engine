using System;
using System.Collections;
using UnityEngine;
using PokemonEngine.Data;
using PokemonEngine.Maths;
using PokemonEngine.BattleSystem.Moves;
using PokemonEngine.BattleSystem.Events;
using PokemonEngine.Localization;

namespace PokemonEngine.BattleSystem {

    public enum HitResult {
        SUCCESS,
        DODGED,
        FAILED
    }

    public abstract class MoveBehaviour
    {
        protected UseMoveAction Executor { get; }
        protected Move Move {
            get {
                return Executor.Move;
            }
        }
        protected TargetPoint Source {
            get {
                return Executor.Source;
            }
        }
        protected TargetPoint Target {
            get {
                return Executor.Target;
            }
        }

        private bool breakSequence;

        public MoveBehaviour(UseMoveAction executor)
        {
            Executor = executor;
            breakSequence = false;

            SetProperties();
        }

        private void SetProperties() {
            foreach(var entry in Move.MoveProperties) {
                try {
                    var field = GetType().GetField(entry.Key);
                    if(field.FieldType == typeof(string)) {
                        field.SetValue(this, entry.Value.StringValue);
                    } else if (field.FieldType == typeof(int)) {
                        field.SetValue(this, entry.Value.IntValue);
                    } else if(field.FieldType == typeof(float)) {
                        field.SetValue(this, entry.Value.FloatValue);
                    } else if(field.FieldType == typeof(bool)) {
                        field.SetValue(this, entry.Value.BoolValue);
                    } else if(typeof(Enum).IsAssignableFrom(field.FieldType)) {
                        field.SetValue(this, Enum.ToObject(field.FieldType, entry.Value.EnumValueIndex));
                    }
                } catch {

                }
            }
 
        }

        public virtual HitResult CanHit()
        {
            float accuracy = Formulas.GetAccuracyValue(Source.PokemonInstance.Modifiers.Accuracy);
            float evasion = Formulas.GetEvasionValue(Source.PokemonInstance.Modifiers.Evasion);
            return UnityEngine.Random.value < (Move.Accuracy * accuracy / evasion) ? HitResult.SUCCESS : HitResult.DODGED;
        }

        public virtual DamageSource CalcMoveDamage()
        {
            var attacker = Source.PokemonInstance;
            var target = Target.PokemonInstance;
            int damage;
            if(Move.Category.CategoryName.Equals("Special")) {
                damage = Formulas.CalculateDefaultDamage(Move.Power, attacker.Statistics.SpAtk, target.Statistics.SpDef, attacker.Pokemon.Level);
            } else {
                damage = Formulas.CalculateDefaultDamage(Move.Power, attacker.Statistics.Attack, target.Statistics.Defense, attacker.Pokemon.Level);
            }

            TypeTable typeTable = Executor.Battle.TypeTable;

            if(damage == 0) damage = 1;

            var damageSource = new DamageSource(Move.Type, damage);
            damageSource.TypeMultiplier = typeTable.Get(Move.Type, target.Pokemon.PokemonData.PrimaryType) * typeTable.Get(Move.Type, target.Pokemon.PokemonData.SecondaryType);
            damageSource.Stab = attacker.Pokemon.PokemonData.PrimaryType == Move.Type ||  attacker.Pokemon.PokemonData.SecondaryType == Move.Type;

            damageSource.TotalDamage = (int) (damage * damageSource.TypeMultiplier * (damageSource.Stab ? Formulas.STAB_MULTIPLIER : 1f) * (damageSource.Critical ? 1.5f : 1f));
            if(damageSource.TotalDamage == 0) damageSource.TotalDamage = 1;

            return damageSource;
        }

        public void BreakSequence() {
            breakSequence = true;
        }

        public IEnumerator RunSequence() {
                var sequence = MoveSequence();
                while(sequence.MoveNext()) {
                    if(breakSequence) break;
                    yield return sequence.Current;
                }
        }

        protected IEnumerator Damage(PokemonInstance pokemon, DamageSource damageSource) {
            return pokemon.Hit(damageSource);
        }

        protected IEnumerator ModifyStats(PokemonInstance target, params StatModifier[] modifiers) {
            foreach (var modifier in modifiers) {
                target.Modifiers.Merge(modifier);
            }
            yield return Executor.Battle.Animator.PerformStatModificationAnimation(target.Pokemon, modifiers);
        }

        protected abstract IEnumerator MoveSequence();

    }




}