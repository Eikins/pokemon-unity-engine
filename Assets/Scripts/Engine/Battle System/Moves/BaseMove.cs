﻿using System.Collections;
using PokemonEngine.BattleSystem.Events;
using PokemonEngine.Data;
using UnityEngine;
using PokemonEngine.Maths;
using PokemonEngine.BattleSystem.Effects;

namespace PokemonEngine.BattleSystem.Moves
{

    public class BaseMove : MoveBehaviour
    {
        public BaseMove(UseMoveAction action) : base(action) {}

        protected override IEnumerator MoveSequence() {
            yield return Executor.Battle.Animator.PerformMoveAnimation(Move, Source.PokemonInstance.Pokemon, Target.PokemonInstance.Pokemon);
            yield return Damage(Target.PokemonInstance, CalcMoveDamage());
        }
    }

}
