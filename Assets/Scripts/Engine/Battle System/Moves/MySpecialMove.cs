﻿using PokemonEngine.BattleSystem.Events;
using PokemonEngine.Data;
using UnityEngine;
using System.Collections;
using PokemonEngine.Localization;

namespace PokemonEngine.BattleSystem.Moves
{
    public class BurnTestMove : MoveBehaviour
    {

        public BurnTestMove(UseMoveAction executor) : base (executor) {}

        protected override IEnumerator MoveSequence() {
            yield return Executor.Battle.Animator.PerformMoveAnimation(Move, Source.PokemonInstance.Pokemon, Target.PokemonInstance.Pokemon);
            yield return Damage(Target.PokemonInstance, CalcMoveDamage());
            yield return Target.SetStatus(Status.Burn);
        }

    }
}
