﻿using System;
using System.Collections;
using System.Collections.Generic;
using PokemonEngine.BattleSystem.Events;
using PokemonEngine.Data;
using UnityEngine;
using PokemonEngine.Maths;
using PokemonEngine.Utils;

namespace PokemonEngine.BattleSystem.Moves
{

    [Serializable]
    public class JSONStatModifier {
        public string stat;
        public int value;
    }

    public class StatMove : MoveBehaviour
    {
        public List<StatModifier> modifiers;

        [MoveProperty(Name = "Self")]
        public bool self;

        [MoveProperty(Name = "Attack")]
        public int attack;

        [MoveProperty(Name = "Defense")]
        public int defense;

        [MoveProperty(Name = "SpAtk")]
        public int spAtk;

        [MoveProperty(Name = "SpDef")]
        public int spDef;

        [MoveProperty(Name = "Speed")]
        public int speed;

        [MoveProperty(Name = "Accuracy")]
        public int accuracy;

        [MoveProperty(Name = "Evasion")]
        public int evasion;

        public StatMove(UseMoveAction executor) : base(executor) {
            SetModifiers();
        }

        private void SetModifiers() {
            var negatives = new List<StatModifier>();
            var positives = new List<StatModifier>();
            SetModifier(negatives, positives, Stats.Attack, attack);
            SetModifier(negatives, positives, Stats.Defense, defense);
            SetModifier(negatives, positives, Stats.SpAtk, spAtk);
            SetModifier(negatives, positives, Stats.SpDef, spDef);
            SetModifier(negatives, positives, Stats.Speed, speed);
            SetModifier(negatives, positives, Stats.Accuracy, accuracy);
            SetModifier(negatives, positives, Stats.Evasion, evasion);
            modifiers = new List<StatModifier>(negatives);
            modifiers.AddRange(positives);
        }

        private void SetModifier(List<StatModifier> negatives, List<StatModifier> positives, Stats stat, int value) {
            if(value < 0) {
                negatives.Add(new StatModifier(stat, value));
            } else if (value > 0) {
                positives.Add(new StatModifier(stat, value));
            }
        }

        protected override IEnumerator MoveSequence() {
            var target = self ? Source : Target;
            yield return Executor.Battle.Animator.PerformMoveAnimation(Move, Source.PokemonInstance.Pokemon, target.PokemonInstance.Pokemon);
            yield return ModifyStats(target.PokemonInstance, modifiers.ToArray());
        }
    }

}