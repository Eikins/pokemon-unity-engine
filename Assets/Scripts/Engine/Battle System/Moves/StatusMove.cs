﻿using System.Collections;
using PokemonEngine.BattleSystem.Events;
using PokemonEngine.Data;
using UnityEngine;
using PokemonEngine.Maths;
using PokemonEngine.BattleSystem.Effects;
using PokemonEngine.Localization;

namespace PokemonEngine.BattleSystem.Moves
{

    public class StatusMove : MoveBehaviour
    {

        [MoveProperty(Name = "Status")]
        public Status status;

        public StatusMove(UseMoveAction action) : base(action) { }

        protected override IEnumerator MoveSequence()
        {
            yield return Executor.Battle.Animator.PerformMoveAnimation(Move, Source.PokemonInstance.Pokemon, Target.PokemonInstance.Pokemon);
            yield return Target.SetStatus(status);
        }

        public override HitResult CanHit() {
            if(Target.Pokemon.Status != Status.None) {
                return HitResult.FAILED;
            } else {
                return base.CanHit();
            }
        }
    }

}