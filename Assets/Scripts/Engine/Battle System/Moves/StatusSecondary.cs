﻿using System.Collections;
using PokemonEngine.BattleSystem.Events;
using PokemonEngine.Data;
using UnityEngine;
using PokemonEngine.Maths;
using PokemonEngine.BattleSystem.Effects;
using PokemonEngine.Localization;

namespace PokemonEngine.BattleSystem.Moves
{

    public class StatusSecondary : MoveBehaviour
    {

        [MoveProperty(Name = "Status")]
        public Status status;
        [MoveProperty(Name = "Status Probability")]
        public float statusProbability;

        public StatusSecondary(UseMoveAction action) : base(action) { }

        protected override IEnumerator MoveSequence()
        {
            yield return Executor.Battle.Animator.PerformMoveAnimation(Move, Source.PokemonInstance.Pokemon, Target.PokemonInstance.Pokemon);
            yield return Damage(Target.PokemonInstance, CalcMoveDamage());
            if (Random.value < statusProbability)
            {
                yield return Target.SetStatus(status);
            }
        }
    }

}