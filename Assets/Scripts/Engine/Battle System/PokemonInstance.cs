﻿using System.Collections;
using PokemonEngine.BattleSystem.Events;
using PokemonEngine.Data;
using PokemonEngine.Maths;
using PokemonEngine.Provider;

namespace PokemonEngine.BattleSystem
{

    public class PokemonInstance
    {

        public Pokemon Pokemon { get; }

        public StatModifiers Modifiers { get; set; }
        public Battle Battle { get; }
        public BattleStatistics Statistics => battleStatistics;
        public TargetPoint TargetPoint => targetPoint;
        
        private BattleStatistics battleStatistics;
        private TargetPoint targetPoint;

        public PokemonInstance(Battle battleInstance, Pokemon pokemon)
        {
            Battle = battleInstance;
            Pokemon = pokemon;
            pokemon.RecalculateStats();

            Modifiers = new StatModifiers(this);

            battleStatistics = new BattleStatistics(this);
            RecalculateBattleStats();

        }

        public void RecalculateBattleStats() {
            battleStatistics.HP = Pokemon.Stats.HP;
            battleStatistics.Attack = Formulas.CalculateStatChange(Pokemon.Stats.Attack, Modifiers.Attack);
            battleStatistics.Defense = Formulas.CalculateStatChange(Pokemon.Stats.Defense, Modifiers.Defense);
            battleStatistics.SpAtk = Formulas.CalculateStatChange(Pokemon.Stats.SpAtk, Modifiers.SpAtk);
            battleStatistics.SpDef = Formulas.CalculateStatChange(Pokemon.Stats.SpDef, Modifiers.SpDef);
            battleStatistics.Speed = Formulas.CalculateStatChange(Pokemon.Stats.Speed, Modifiers.Speed);
        }

        public IEnumerator Hit(DamageSource damageSource)
        {

            var preDamageEvent = new PokemonDamageEvent.Pre(this, damageSource);
            Battle.EventManager.TriggerEvent(preDamageEvent);
            yield return preDamageEvent.GetSequence();
            if(preDamageEvent.Canceled) {
                yield break;
            }

            Pokemon.Damage(damageSource.TotalDamage);

            var postDamageEvent = new PokemonDamageEvent.Post(this, damageSource);
            Battle.EventManager.TriggerEvent(postDamageEvent);
            yield return postDamageEvent.GetSequence();

            if(Pokemon.CurrentHealth == 0) {
                var koEvent = new PokemonKnockoutEvent(this);
                Battle.EventManager.TriggerEvent(koEvent);
                if(koEvent.Canceled) {
                    Pokemon.Heal(1);
                }
                yield return koEvent.GetSequence();
            }
        }

        public void SetTargetPoint(TargetPoint targetPoint) {
            this.targetPoint = targetPoint;
        }

    }
}
