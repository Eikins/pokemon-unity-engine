﻿using System;
using UnityEngine;
using PokemonEngine.Data;

namespace PokemonEngine.BattleSystem
{

    public enum Stats {
        Attack,
        Defense,
        SpAtk,
        SpDef,
        Speed,
        Accuracy,
        Evasion
    }

    [Serializable]
    public class StatModifier {

        [SerializeField] private Stats statistic;
        [SerializeField] private int value;
        
        public Stats Statistic => statistic;
        public int Value => value;

        public StatModifier(Stats statistic, int value) {
            this.statistic = statistic;
            this.value = value < -6 ? -6 : (value > 6 ? 6 : value);
        }

        public bool IsRising() {
            return Value > 0;
        }
    }

    public class StatModifiers
    {

        private PokemonInstance pokemon; 

        private int attack = 0;
        private int defense = 0;
        private int spAtk = 0;
        private int spDef = 0;
        private int speed = 0;
        private int accuracy = 0;
        private int evasion = 0;

        
        public int Attack => attack;
        public int Defense => defense;
        public int SpAtk => spAtk;
        public int SpDef => spDef;
        public int Speed => speed;

        public int Accuracy => accuracy;
        public int Evasion => evasion;

        public StatModifiers(PokemonInstance pokemon, int attack, int defense, int spAtk, int spDef, int speed, int accuracy, int evasion)
        {
            this.pokemon = pokemon;
            this.attack   = Clamp(attack);
            this.defense  = Clamp(defense);
            this.spAtk    = Clamp(spAtk);
            this.spDef    = Clamp(spDef);
            this.speed    = Clamp(speed);
            this.accuracy = Clamp(accuracy);
            this.evasion  = Clamp(evasion);
        }

        public StatModifiers(PokemonInstance pokemon) {
            this.pokemon = pokemon;
        }

        private int Clamp(int stat) {
            return stat < -6 ? -6 : (stat > 6 ? 6 : stat);
        }

        public void Merge(StatModifier modifier) {
            switch (modifier.Statistic) {
                case Stats.Attack:
                    attack = Clamp(attack + modifier.Value);
                    break;
                case Stats.Defense:
                    defense = Clamp(defense + modifier.Value);
                    break;
                case Stats.SpAtk:
                    spAtk = Clamp(spAtk + modifier.Value);
                    break;
                case Stats.SpDef:
                    spDef = Clamp(spDef + modifier.Value);
                    break;
                case Stats.Speed:
                    speed = Clamp(speed + modifier.Value);
                    break;
                case Stats.Accuracy:
                    accuracy = Clamp(accuracy + modifier.Value);
                    break;
                case Stats.Evasion:
                    evasion = Clamp(evasion + modifier.Value);
                    break;
                default:
                    break;
            }
            pokemon.RecalculateBattleStats();
        }

        /* 
        public static StatModifiers operator + (StatModifiers m1, StatModifiers m2) {
            return new StatModifiers(
                m1.Attack + m2.Attack,
                m1.Defense + m2.Defense,
                m1.SpAtk + m2.SpAtk,
                m1.SpDef + m2.SpDef,
                m1.Speed + m2.Speed,
                m1.Accuracy + m2.Accuracy,
                m1.Evasion + m2.Evasion);
        }

        public static StatModifiers operator - (StatModifiers m1, StatModifiers m2) {
            return new StatModifiers(
                m1.Attack - m2.Attack,
                m1.Defense - m2.Defense,
                m1.SpAtk - m2.SpAtk,
                m1.SpDef - m2.SpDef,
                m1.Speed - m2.Speed,
                m1.Accuracy - m2.Accuracy,
                m1.Evasion - m2.Evasion);
        }

           public static StatModifiers operator - (StatModifiers modifier) {
            return new StatModifiers(
                -modifier.Attack,
                -modifier.Defense,
                -modifier.SpAtk,
                -modifier.SpDef,
                -modifier.Speed,
                -modifier.Accuracy,
                -modifier.Evasion);
        }

        public static StatModifiers operator * (int mult, StatModifiers modifier) {
            return new StatModifiers(
                mult * modifier.Attack,
                mult * modifier.Defense,
                mult * modifier.SpAtk,
                mult * modifier.SpDef,
                mult * modifier.Speed,
                mult * modifier.Accuracy,
                mult * modifier.Evasion);
        }

        public static readonly StatModifiers NO_MODIFIER       = new StatModifiers(0, 0, 0, 0, 0, 0, 0);
        public static readonly StatModifiers ATTACK_MODIFIER   = new StatModifiers(1, 0, 0, 0, 0, 0, 0);
        public static readonly StatModifiers DEFENSE_MODIFIER  = new StatModifiers(0, 1, 0, 0, 0, 0, 0);
        public static readonly StatModifiers SPATK_MODIFIER    = new StatModifiers(0, 0, 1, 0, 0, 0, 0);
        public static readonly StatModifiers SPDEF_MODIFIER    = new StatModifiers(0, 0, 0, 1, 0, 0, 0);
        public static readonly StatModifiers SPEED_MODIFIER    = new StatModifiers(0, 0, 0, 0, 1, 0, 0);
        public static readonly StatModifiers ACCURACY_MODIFIER = new StatModifiers(0, 0, 0, 0, 0, 1, 0);
        public static readonly StatModifiers EVASION_MODIFIER  = new StatModifiers(0, 0, 0, 0, 0, 0, 1);*/

    }
    
}
