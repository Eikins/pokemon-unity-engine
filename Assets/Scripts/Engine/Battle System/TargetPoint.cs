using System.Collections;
using PokemonEngine.Data;
using PokemonEngine.BattleSystem.Events;
using PokemonEngine.Localization;

namespace PokemonEngine.BattleSystem {

    public class TargetPoint {

        private PokemonInstance pokemon;

        public BattleActor Actor { get; }
        public PokemonInstance PokemonInstance => pokemon;
        public Pokemon Pokemon => pokemon.Pokemon;

        public TargetPoint(BattleActor actor) {
            Actor = actor;
        }

        public IEnumerator SetPokemon(PokemonInstance pokemonInstance) {
            if(pokemon != null) pokemon.SetTargetPoint(null);
            pokemon = pokemonInstance;
            if(pokemon != null) {
                pokemon.SetTargetPoint(this);
                var e = new PokemonSpawnEvent(this);
                PokemonInstance.Battle.EventManager.TriggerEvent(e);
                yield return e.GetSequence();
            }
        }

        public bool IsEmpty() {
            return pokemon == null;
        }

        public void AddEffect(Effect effect) {
            if(pokemon == null) return;
            effect.SetBattle(pokemon.Battle);
            effect.SetTarget(this);
            pokemon.Battle.EventManager.RegisterListener(effect);
            effect.OnAdded();
        }

        public IEnumerator SetStatus(Status status) {
            if (Pokemon.Status == Status.None) {
                Pokemon.SetStatus(status);
                var effect = status.CreateBattleEffect(Pokemon.StatusState);
                AddEffect(effect);
                effect.OnStatusAdded();
                yield return pokemon.Battle.Animator.ShowFormattedText(I18n.Translate("Battle.Status." + status.GetName() + ".Add"), Pokemon.DisplayName);
                pokemon.Battle.Animator.RefreshPokemonStatus(Pokemon);
            }
        }

        public IEnumerator RemoveStatus() {
            var status = Pokemon.Status;
            Pokemon.RemoveStatus();
            yield return pokemon.Battle.Animator.ShowFormattedText(I18n.Translate("Battle.Status." + status.GetName() + ".Remove"), Pokemon.DisplayName);
            pokemon.Battle.Animator.RefreshPokemonStatus(Pokemon);
        }

    }
}