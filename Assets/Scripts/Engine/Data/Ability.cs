﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Pokemon Engine/Data/Ability", fileName = "New Ability", order = 2)]
public class Ability : ScriptableObject {

    [SerializeField] private string unlocalizedName;

    public string UnlocalizedName => unlocalizedName;
}
