using UnityEngine;
using UnityEditor;
using System.Json;
using System.Net.Http;
using System.Net.Http.Headers; 
using System;

namespace PokemonEngine.Data.Utils {

    public static class PokemonDataUtilities {

        const string api_url = "https://pokeapi.co/api/v2/pokemon/";

        [MenuItem("CONTEXT/PokemonData/Fill from PokeAPI", false, 0)]
        static void GetStatsFromPokeAPI(){
            
            var objs = Selection.objects;
            if(objs != null && objs.Length > 0) {

                foreach(var obj in objs) {
                    if(typeof(PokemonData).IsAssignableFrom(obj.GetType())) {
                        Fill((PokemonData) obj);
                    }
                }
            }
        }

        private async static void Fill(PokemonData pokemonData) {
                var serializedData = new UnityEditor.SerializedObject(pokemonData);

                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(api_url + int.Parse(serializedData.FindProperty("unlocalizedName").stringValue));

                // Add an Accept header for JSON format.
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                // List data response.
                HttpResponseMessage response = await client.GetAsync("");  // Blocking call! Program will wait here until a response is received or a timeout occurs.
                if (response.IsSuccessStatusCode)
                {
                    // Parse the response body.
                    var body = await response.Content.ReadAsStringAsync();  //Make sure to add a reference to System.Net.Http.Formatting.dll
                    var jsonObject = JsonObject.Parse(body);

                    serializedData.FindProperty("weight").floatValue = (float) jsonObject["weight"] / 10f;
                    serializedData.FindProperty("height").floatValue = (float) jsonObject["height"] / 10f;

                    serializedData.FindProperty("baseExp").intValue = (int) jsonObject["base_experience"];
                    
                    var statsObject = jsonObject["stats"];
                    int[] stats = new int[6];
                    int[] efforts = new int[6];
                    for(int i = 0; i < 6; i++) {
                        var statObj = statsObject[i];
                        stats[i] = int.Parse(statObj["base_stat"].ToString());
                        efforts[i] = int.Parse(statObj["effort"].ToString());
                    }
                    serializedData.FindProperty("baseStats.hp").intValue = stats[5];
                    serializedData.FindProperty("baseStats.attack").intValue = stats[4];
                    serializedData.FindProperty("baseStats.defense").intValue = stats[3];
                    serializedData.FindProperty("baseStats.spAtk").intValue = stats[2];
                    serializedData.FindProperty("baseStats.spDef").intValue = stats[1];
                    serializedData.FindProperty("baseStats.speed").intValue = stats[0];

                    serializedData.FindProperty("evYields.hp").intValue = efforts[5];
                    serializedData.FindProperty("evYields.attack").intValue = efforts[4];
                    serializedData.FindProperty("evYields.defense").intValue = efforts[3];
                    serializedData.FindProperty("evYields.spAtk").intValue = efforts[2];
                    serializedData.FindProperty("evYields.spDef").intValue = efforts[1];
                    serializedData.FindProperty("evYields.speed").intValue = efforts[0];

                    serializedData.ApplyModifiedProperties();
                }
                else
                {
                    Debug.LogWarning(String.Format("{0} ({1})", (int)response.StatusCode, response.ReasonPhrase));
                }

                //Make any other calls using HttpClient here.

                //Dispose once all HttpClient calls are complete. This is not necessary if the containing object will be disposed of; for example in this case the HttpClient instance will be disposed automatically when the application terminates so the following call is superfluous.
                client.Dispose();
        }

    }

    
}