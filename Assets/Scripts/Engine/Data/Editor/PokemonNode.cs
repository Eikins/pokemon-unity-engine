using System;
using UnityEditor;
using UnityEngine;
using PokemonEngine.Data;
 

namespace PokemonEngine.Data.Editor {
    public class PokemonNode
    {

        private const float snapDistance = 100f;

        public Rect rect;
        public string title;
        public bool isDragged;
        public int index;
        public Pokemon pokemon;
        public Texture2D icon;
        public GUIStyle style;

        public Vector2 position;

        public Vector2 Position {
            get {
                return position;
            }
            set {
                position = value;
                rect.position = value;
            }
        }
    
        private TrainerEditorWindow parent;
    
        public PokemonNode(TrainerEditorWindow parent, int index, Pokemon pokemon, Rect rect, GUIStyle style)
        {
            this.parent = parent;
            this.rect = rect;
            position = rect.position;
            var skin = EditorGUIUtility.GetBuiltinSkin(EditorSkin.Inspector);
            this.style = style;
            this.index = index;
            icon = AssetDatabase.LoadAssetAtPath("Assets/Textures/Pokemons/Icons/" + pokemon.PokemonData.InternalName + ".png", typeof(Texture2D)) as Texture2D;
            if(icon == null) {
                icon = new Texture2D(32, 32);
            }
            this.pokemon = pokemon;
        }
    
        public void Drag(Vector2 delta)
        {
            rect.position += delta;
        }
    
        public void Draw()
        {
            GUILayout.BeginArea(rect, style);
            GUILayout.BeginVertical();

            GUILayout.Label("Pokemon " + (index + 1), EditorStyles.boldLabel);
            GUILayout.Label(pokemon.PokemonData.name);
            GUILayout.Label(icon);
            GUILayout.Label("Overview", EditorStyles.boldLabel);
            GUILayout.Label("Lv. " + pokemon.Level);
            GUILayout.Label("Gender : " + (pokemon.PokemonData.Genderless ? "Genderless" : pokemon.Female ? "Female" : "Male"));

            GUILayout.EndVertical();
            GUILayout.EndArea();
        }
    
        public bool ProcessEvents(Event e)
        {
            switch (e.type)
            {
                case EventType.MouseDown:
                    if (e.button == 0)
                    {
                        if (rect.Contains(e.mousePosition))
                        {
                            Selection.objects = new UnityEngine.Object[] { pokemon }; 
                            isDragged = true;
                            GUI.changed = true;
                        }
                        else
                        {
                            GUI.changed = true;
                        }
                    }
                    break;
    
                case EventType.MouseUp:
                    if(isDragged) {
                        PokemonNode nodeToSwap = null;
                        foreach (var node in parent.nodes) {
                            if(node == this) continue;
                            var dist = (rect.position - node.rect.position).magnitude;
                            if(dist < snapDistance) {
                                nodeToSwap = node;
                                break;
                            }
                        }
                        if(nodeToSwap == null) {
                            rect.position = position;
                        } else {
                            parent.SwapNodes(this, nodeToSwap);
                        }
                        GUI.changed = true;
                        isDragged = false;
                    }

                    break;
    
                case EventType.MouseDrag:
                    if (e.button == 0 && isDragged)
                    {
                        Drag(e.delta);
                        e.Use();
                        return true;
                    }
                    break;
            }
    
            return false;
        }
    }
}
