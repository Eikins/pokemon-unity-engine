

using UnityEngine;
using UnityEditor;

namespace PokemonEngine.Data.Editor {

    public class SerializedTrainer {

        private SerializedObject serializedObject;

        public SerializedTrainer(Trainer trainer) {
            serializedObject = new SerializedObject(trainer);
        }

        public void Save() {
            serializedObject.ApplyModifiedProperties();
        }

        public SerializedProperty UnlocalizedName => serializedObject.FindProperty("unlocalizedName");

    }

}