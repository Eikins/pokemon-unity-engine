using System.Collections.Generic;
using UnityEditor;
using PokemonEngine.Data;

namespace PokemonEngine.Data.Editor {

    public class TrainerAssetModificationProcessor : UnityEditor.AssetModificationProcessor {

        public static List<string> newTrainers = new List<string>();

        static void OnWillCreateAsset(string assetName)
        {
            // remove ".meta" from path
            string assetPath = assetName.Substring(0, assetName.Length - 5);
            newTrainers.Add(assetPath);
        }

    }

    public class TrainerAssetPostProcessor : AssetPostprocessor {

        static void OnPostprocessAllAssets(string[] importedAssets,string[] deletedAssets,string[] movedAssets,string[] movedFromAssetPaths) {
            var trainers = TrainerAssetModificationProcessor.newTrainers;
            if (trainers.Count == 0) return;
            foreach (var str in importedAssets) {
                if (trainers.Contains(str)) {
                    var trainer = AssetDatabase.LoadAssetAtPath<Trainer>(str);
                    if (trainer != null) {
                        Team team = new Team();
                        team.name = "Team";
                        AssetDatabase.AddObjectToAsset(team, str);
                        typeof(Trainer).GetField("team", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance).SetValue(trainer, team); 
                        AssetDatabase.ImportAsset(str, ImportAssetOptions.ForceUpdate);
                    }

                }
            }
            trainers.Clear();
        }
    }

}