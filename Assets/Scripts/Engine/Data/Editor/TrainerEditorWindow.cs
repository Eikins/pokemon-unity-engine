﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using PokemonEngine.Data;

namespace PokemonEngine.Data.Editor {
    public class TrainerEditorWindow : EditorWindow
    {

        [UnityEditor.Callbacks.OnOpenAsset(1)]
        public static bool OnOpenAsset(int instanceID, int line)
        {
            var trainer = Selection.activeObject as Trainer;
            if (trainer != null) {
                var window = EditorWindow.GetWindow<TrainerEditorWindow>("Trainer Editor");
                window.Trainer = trainer;
                return true;
            }        
            return false;
        }

        private Trainer trainer;
        private SerializedTrainer serializedTrainer;
        public Trainer Trainer {
            get {
                return trainer;
            }
            set {
                trainer = value;
                serializedTrainer = new SerializedTrainer(trainer);
                focusedNode = null;
                nodes = null;
            }
        }
        private SerializedTrainer SerializedTrainer => serializedTrainer;

        public GUIStyle pokemonBoxStyle, teamPanelStyle;

        public List<PokemonNode> nodes;
        private Dictionary<Pokemon, PokemonNode> pokemonNodes;

        public PokemonNode focusedNode;

        public void OnEnable() {
            var skin = EditorGUIUtility.GetBuiltinSkin(EditorSkin.Inspector);
            pokemonBoxStyle = skin.FindStyle("OL box");
            pokemonBoxStyle.margin = new RectOffset(10, 10, 10, 10);
            teamPanelStyle = skin.FindStyle("ShurikenEffectBg");
        }


        public void OnSelectionChange() {
            var pokemon = Selection.activeObject as Pokemon;
            if(pokemon != null) {
                if(pokemonNodes.TryGetValue(pokemon, out focusedNode)) {
                    Repaint();
                }
            }
        }

        public void OnGUI() {

            if(trainer != null && nodes == null) {
                nodes = new List<PokemonNode>();
                pokemonNodes = new Dictionary<Pokemon, PokemonNode>();
                if(trainer.Team.Count > 0) {
                    for (int i = 0; i < trainer.Team.Count; i++) {
                        var node = new PokemonNode(this, i, trainer.Team[i], NodeRect(i), pokemonBoxStyle);
                        nodes.Add(node);
                        pokemonNodes.Add(node.pokemon, node);
                    }
                }
            }

            if(trainer == null || serializedTrainer == null) {
                GUILayout.Label("Select a trainer");
            } else {
                DrawSidePanel();
                DrawTeamPanel();
                ProcessEvents(Event.current);
            }

            if(GUI.changed) Repaint();
        }

        private void DrawSidePanel() {
            GUILayout.BeginVertical(GUILayout.MinWidth(290), GUILayout.MaxWidth(290));
            GUILayout.Label("Trainer", EditorStyles.boldLabel);
            var unlocalizedName = serializedTrainer.UnlocalizedName;
            unlocalizedName.stringValue = EditorGUILayout.TextField("Unlocalized Name", unlocalizedName.stringValue);
            serializedTrainer.Save();
            GUILayout.EndVertical();
        }

        private void ProcessEvents(Event e) {
            if(e.type == EventType.ExecuteCommand) {
                string commandName = e.commandName;
                if (commandName == "ObjectSelectorClosed") {
                    var selectedPokemonData = EditorGUIUtility.GetObjectPickerObject () as PokemonData;
                    if(selectedPokemonData != null) {
                        AddPokemon(selectedPokemonData);
                    }
                    e.Use();
                }
            }
            if(e.type == EventType.KeyDown) {
                if(e.keyCode == KeyCode.Delete && focusedNode != null) {
                    RemovePokemon(focusedNode.index);
                    focusedNode = null;
                }
            }
        }

        public Rect NodeRect(int index) {
            var x = (index % 3) * 210 + 10;
            var y = ((index >= 3) ? 320 : 10);
            return new Rect(x, y, 200, 300);
        }


        private void DrawTeamPanel() {

            GUILayout.BeginArea(new Rect(300, 0, Screen.width - 300, Screen.height), teamPanelStyle);
            foreach (var node in nodes) {
                if(node != focusedNode) node.Draw();
            }

            if(nodes.Count < 6) {
                var x = (nodes.Count % 3 * 210) + 10 + 70;
                var y = ((nodes.Count >= 3) ? 320 : 10) + 120;
                if(GUI.Button(new Rect(x, y, 60, 60), "+")) {
                    int controlID = EditorGUIUtility.GetControlID (FocusType.Passive);
                    EditorGUIUtility.ShowObjectPicker<PokemonData> (null, true, "", controlID);
                }
            }
            
            if(focusedNode != null) {
                var rectOffset = new RectOffset(5, 5, 5, 5);
                EditorGUI.DrawRect(rectOffset.Add(focusedNode.rect), new Color(0.588f, 0.882f, 1f, 0.5f));
                focusedNode.Draw();
            }

            var nodeCopy = new List<PokemonNode>(nodes);
            foreach (var node in nodeCopy) {
               node.ProcessEvents(Event.current);
            }
            GUILayout.EndArea();
        }

        public void SwapNodes(PokemonNode source, PokemonNode target) {
            var tmp = nodes[source.index];
            nodes[source.index] = nodes[target.index];
            nodes[target.index] = tmp;
            var tmpPos = source.Position;
            source.Position = target.Position;
            target.Position = tmpPos;
            var tmpIndex = source.index;
            source.index = target.index;
            target.index = tmpIndex;
            trainer.Team.Swap(source.index, target.index);
            // Todo : swap in the team

            var path = AssetDatabase.GetAssetPath(Trainer);
            Object[] assets = UnityEditor.AssetDatabase.LoadAllAssetsAtPath(path);
            string tmpName = null;
            Object tmpAsset = null;
            for (int i = 0; i < assets.Length; i++) {
                Object asset = assets[i];
                if (!UnityEditor.AssetDatabase.IsMainAsset(asset)) {
                    
                    if(asset.name == "Pokemon " + (source.index + 1)) {
                        if(tmpName == null) {
                            tmpName = "Pokemon " + (source.index + 1);
                            asset.name = "_Pokemon " + (target.index + 1);
                            tmpAsset = asset;
                        } else {
                            asset.name = tmpName;
                        }

                    } else if (asset.name == "Pokemon " + (target.index + 1)) {
                        if(tmpName == null) {
                            tmpName = "Pokemon " + (target.index + 1);
                            asset.name = "_Pokemon " + (source.index + 1);
                            tmpAsset = asset;
                        } else {
                            asset.name = tmpName;
                        }
                    }
                }
            }
            tmpAsset.name = tmpAsset.name.Remove(0, 1);
            EditorUtility.SetDirty(Trainer);
            AssetDatabase.ImportAsset(path);
        }

        public void AddPokemon(PokemonData pokemonData) {
            var pokemon = new Pokemon();
            var index = trainer.Team.Count;
            typeof(Pokemon).GetField("pokemonData", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance).SetValue(pokemon, pokemonData); 
            
            pokemon.name = "Pokemon " + (index + 1);
            AssetDatabase.AddObjectToAsset(pokemon, Trainer);
            Trainer.Team.Pokemons.Add(pokemon);
            AssetDatabase.ImportAsset(AssetDatabase.GetAssetPath(pokemon));
            Texture2D icon = AssetDatabase.LoadAssetAtPath("Assets/Textures/Pokemons/Icons/" + pokemon.PokemonData.InternalName + ".png", typeof(Texture2D)) as Texture2D;
            if(icon == null) {
                icon = new Texture2D(32, 32);
            }
            var node = new PokemonNode(this, index, pokemon, NodeRect(index), pokemonBoxStyle);
            nodes.Add(node);
            pokemonNodes.Add(node.pokemon, node);
        }

        public void RemovePokemon(int index) {
            
            if(index != nodes.Count - 1) {
                for(int i = index; i < nodes.Count - 1; i++) {
                    SwapNodes(nodes[i], nodes[i+1]);
                }
            }

            var finalIndex = nodes.Count - 1;
            nodes.RemoveAt(finalIndex);
            trainer.Team.Pokemons.RemoveAt(finalIndex);
            
            var path = AssetDatabase.GetAssetPath(Trainer);
            Object[] assets = UnityEditor.AssetDatabase.LoadAllAssetsAtPath(path);
            for (int i = 0; i < assets.Length; i++) {
                Object asset = assets[i];
                if (!UnityEditor.AssetDatabase.IsMainAsset(asset)) {
                    if(asset.name == "Pokemon " + (finalIndex + 1)) {
                        Object.DestroyImmediate(asset, true);
                    }
                }
            }
            EditorUtility.SetDirty(Trainer);
            AssetDatabase.ImportAsset(path);
        } 

        public void ClearTeam() {
            var path = AssetDatabase.GetAssetPath(Trainer);
            Object[] assets = UnityEditor.AssetDatabase.LoadAllAssetsAtPath(path);
            for (int i = 0; i < assets.Length; i++) {
                Object asset = assets[i];
                if (!UnityEditor.AssetDatabase.IsMainAsset(asset)) {
                    Object.DestroyImmediate(asset, true);
                }
            }
            AssetDatabase.SaveAssets();
        }

    }
}
