﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PokemonEngine.Data
{
    [CreateAssetMenu(menuName = "Pokemon Engine/Data/Egg Group", fileName = "New Egg Group", order = 3)]
    public class EggGroup : ScriptableObject
    {
        [SerializeField] private string identifier;
        public List<PokemonData> pokemons;

        public string Identifier => identifier;
    }
}
