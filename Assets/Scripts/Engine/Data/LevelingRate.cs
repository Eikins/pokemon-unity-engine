﻿using UnityEngine;

namespace PokemonEngine.Data
{
    [CreateAssetMenu(menuName = "Pokemon Engine/Data/Leveling Rate", fileName = "New Leveling Rate", order = 10)]
    public class LevelingRate : ScriptableObject
    {
        [NamedArray("Level")]
        [SerializeField] private int[] nextLevelExperience = new int[100];

        [NamedArray("Level")]
        [SerializeField] private int[] totalExperience = new int[100];

        public int GetTotalExp(int level)
        {
            if(level > 0) return totalExperience[level - 1];
            return 0;
        }

        public int GetNextLevelExp(int level)
        {
            if (level > 0) return nextLevelExperience[level - 1];
            return 0;
        }
    }
}
