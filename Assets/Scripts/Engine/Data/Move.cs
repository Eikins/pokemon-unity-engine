﻿using System;
using System.Collections.Generic;
using UnityEngine;
using PokemonEngine.Localization;
using PokemonEngine.Utils;
using UnityEngine.Timeline;

namespace PokemonEngine.Data
{

    [Serializable]
    public class MovePropertyDictionnary : SerializableDictionary<string, MovePropertyValue> { }

    [AttributeUsage(AttributeTargets.Field)]
    public class MovePropertyAttribute : Attribute {

        private string name;
        private string defaultValue;

        public virtual string Name {
            get {
                return name;
            }
            set {
                name = value;
            }
        }

        public virtual string Default {
            get {
                return defaultValue;
            }
            set {
                defaultValue = value;
            }
        }

        public MovePropertyAttribute() {
            name = null;
            defaultValue = null;
        }

    }

    [Serializable]
    public class MovePropertyValue {

        [SerializeField] private string value;

        public string StringValue => value;
        public int IntValue => int.Parse(value);
        public float FloatValue => float.Parse(value);
        public bool BoolValue => bool.Parse(value);
        public int EnumValueIndex => int.Parse(value);

        public MovePropertyValue(string value) {
            this.value = value;
        }

        public MovePropertyValue(int value) {
            this.value = value.ToString();
        }

        public MovePropertyValue(float value) {
            this.value = value.ToString();
        }

        public MovePropertyValue(bool value) {
            this.value = value.ToString();
        }

        public MovePropertyValue(Enum value) {
            this.value = Convert.ToInt32(value).ToString();
        }

    }

    [Flags]
    public enum MoveFlags
    {
        Authentic = (1 << 0),
        Bite = (1 << 1),
        Bullet = (1 << 2),
        Charge = (1 << 3),
        Contact = (1 << 4),
        Dance = (1 << 5),
        Defrost = (1 << 6),
        Distance = (1 << 7),
        Gravity = (1 << 8),
        Heal = (1 << 9),
        Mirror = (1 << 10),
        Mystery = (1 << 11),
        Nonsky = (1 << 12),
        Power = (1 << 13),
        Protected = (1 << 14),
        Pulse = (1 << 15),
        Punch = (1 << 16),
        Recharge = (1 << 17),
        Reflectable = (1 << 18),
        Snatch = (1 << 19),
        Sound = (1 << 20),
    }

    [CreateAssetMenu(menuName = "Pokemon Engine/Data/Move", fileName = "New Move", order = 1)]
    public class Move : ScriptableObject
    {

        [SerializeField] private string unlocalizedName;
        [SerializeField] private Type type;
        [SerializeField] private MoveCategory category;
        [SerializeField] private int powerPoints;
        [SerializeField] private int power;
        [SerializeField] [Range(0f, 1f)] private float accuracy;
        [SerializeField] private int priority;
        [SerializeField] [EnumFlags] private MoveFlags flags;
        [SerializeField] private TimelineAsset animationTimeline;
        [SerializeField] private string behaviourClass;

        [HideInInspector] [SerializeField] private MovePropertyDictionnary moveProperties;

        public string UnlocalizedName => "Moves." + unlocalizedName + ".Name";
        public string InternalName => unlocalizedName;
        public string DisplayName => I18n.Translate(UnlocalizedName);
        public Type Type => type;
        public MoveCategory Category => category;
        public int PowerPoints => powerPoints;
        public int Power => power;
        public float Accuracy => accuracy;
        public int Priority => priority;
        public MoveFlags Flags => flags;
        public string BehaviourClass => behaviourClass;
        public TimelineAsset AnimationTimeline => animationTimeline;
        public MovePropertyDictionnary MoveProperties => moveProperties;

    }
}

