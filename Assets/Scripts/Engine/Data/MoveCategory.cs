﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PokemonEngine.Data {

    [CreateAssetMenu(menuName = "Pokemon Engine/Data/Move Category", fileName = "New Move Category", order = 20)]
    public class MoveCategory : ScriptableObject
    {
        [SerializeField] private string categoryName;

        public string CategoryName => categoryName;
    }

}
