using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PokemonEngine.Data {

    [CreateAssetMenu(menuName = "Pokemon Engine/Data/Nature", fileName = "New Nature", order = 11)]
    public class Nature : ScriptableObject {

        [SerializeField] private string unlocalizedName;

        [SerializeField] [Range(0.9f, 1.1f)] private float attackMultiplier = 1f;
        [SerializeField] [Range(0.9f, 1.1f)] private float defenseMultiplier = 1f;
        [SerializeField] [Range(0.9f, 1.1f)] private float spAtkMultiplier = 1f;
        [SerializeField] [Range(0.9f, 1.1f)] private float spDefkMultiplier = 1f;
        [SerializeField] [Range(0.9f, 1.1f)] private float speedMultiplier = 1f;

        public string UnlocalizedName => "natures." + unlocalizedName + ".name";

        public float AttackMultiplier => attackMultiplier;
        public float DefenseMultiplier => defenseMultiplier;
        public float SpAtkMultiplier => spAtkMultiplier;
        public float SpDefkMultiplier => spDefkMultiplier;
        public float SpeedMultiplier => speedMultiplier;

    }
}