﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PokemonEngine.Maths;
using PokemonEngine.Localization;

namespace PokemonEngine.Data
{
    [CreateAssetMenu(menuName = "Pokemon Engine/Entity/Pokemon", fileName = "New Pokemon", order = 0)]
    public class Pokemon : ScriptableObject
    {

        #region Fields
        [SerializeField] private PokemonData pokemonData;

        [SerializeField] private bool hasNickname;
        [SerializeField] private string nickname;
        [SerializeField] private int level;
        [SerializeField] private int totalExp;
        [SerializeField] private int currentExp;
        [SerializeField] private int friendship;

        [SerializeField] private bool female;
        [SerializeField] private bool isShiny;

        [SerializeField] private int currentHealth;
        [SerializeField] private Status status = Status.None;
        [SerializeField] private int statusState;

        [SerializeField] private Statistics evs;
        [SerializeField] private Statistics ivs;

        [SerializeField] private Statistics stats;

        [SerializeField] private Ability ability;

        [SerializeField] private MoveInfo[] moveset;

        #endregion

        #region Properties

        public PokemonData PokemonData => pokemonData;

        public string Nickname
        {
            get
            {
                return nickname;
            }
            set
            {
                hasNickname = true;
                nickname = value;
            }
        }
        public bool HasNickname => hasNickname;

        public string DisplayName => hasNickname ? Nickname : I18n.Translate(PokemonData.UnlocalizedName);

        public int Level
        {
            get
            {
                return level;
            }
            set
            {
                level = value;
                totalExp = currentExp + pokemonData.LevelingRate.GetTotalExp(level);
            }
        }
        public int CurrentExp
        {
            get
            {
                return currentExp;
            }
            set
            {
                currentExp = value;
                totalExp = currentExp + pokemonData.LevelingRate.GetTotalExp(level);
            }
        }
        public int TotalExp => totalExp;

        public bool IsShiny{
            get {
                return isShiny;
            }
            set
            {
                isShiny = value;

            }
        }

        public bool Female => female;

        public Status Status => status;
        public int StatusState {
            get {
                return statusState;
            }
            set {
                statusState = value;
            }
        }
        public int CurrentHealth => currentHealth;

        public Statistics EVs => evs;
        public Statistics IVs => ivs;

        public Statistics Stats => stats;

        public Ability Ability => ability;

        public MoveInfo[] Moveset => moveset;

        public int GetTotalEVs() {
            return evs.GetTotal();
        }
        #endregion

        #region EVs Setters
        public void SetHPEVs(int value)
        {
            int newHP = value > 252 ? 252 : (value < 0 ? 0 : value);
            if (evs.GetTotal() - evs.HP + newHP > 510)
            {
                evs.HP += 510 - evs.GetTotal();
            }
            else
            {
                evs.HP = newHP;
            }
            RecalculateHPs();
        }

        public void SetAttackEVs(int value)
        {
            int newAttack = value > 252 ? 252 : (value < 0 ? 0 : value);
            if (evs.GetTotal() - evs.Attack + newAttack > 510)
            {
                evs.Attack += 510 - evs.GetTotal();
            }
            else
            {
                evs.Attack = newAttack;
            }
            RecalculateAttack();
        }

        public void SetDefenseEVs(int value)
        {
            int newDefense = value > 252 ? 252 : (value < 0 ? 0 : value);
            if (evs.GetTotal() - evs.Defense + newDefense > 510)
            {
                evs.Defense += 510 - evs.GetTotal();
            }
            else
            {
                evs.Defense = newDefense;
            }
            RecalculateDefense();
        }

        public void SetSpAtkEVs(int value)
        {
            int newSpAtk = value > 252 ? 252 : (value < 0 ? 0 : value);
            if (evs.GetTotal() - evs.SpAtk + newSpAtk > 510)
            {
                evs.SpAtk += 510 - evs.GetTotal();
            }
            else
            {
                evs.SpAtk = newSpAtk;
            }
            RecalculateSpAtk();
        }

        public void SetSpDefEVs(int value)
        {
            int newSpDef = value > 252 ? 252 : (value < 0 ? 0 : value);
            if (evs.GetTotal() - evs.SpDef + newSpDef > 510)
            {
                evs.SpDef += 510 - evs.GetTotal();
            }
            else
            {
                evs.SpDef = newSpDef;
            }
            RecalculateSpDef();
        }

        public void SetSpeedEVs(int value)
        {
            int newSpeed = value > 252 ? 252 : (value < 0 ? 0 : value);
            if (evs.GetTotal() - evs.Speed + newSpeed > 510)
            {
                evs.Speed += 510 - evs.GetTotal();
            }
            else
            {
                evs.Speed = newSpeed;
            }
            RecalculateSpeed();
        }
        #endregion

        #region Stats Formulas

        public void RecalculateStats() {
            if(PokemonData == null) return;
            RecalculateHPs();
            RecalculateAttack();
            RecalculateDefense();
            RecalculateSpAtk();
            RecalculateSpDef();
            RecalculateSpeed();
        }

        // Formula of Gen III => https://bulbapedia.bulbagarden.net/wiki/Statistic#Hit_Points
        private void RecalculateHPs() {
            stats.HP = Formulas.CalculateHPs(PokemonData.BaseStats.HP, IVs.HP, EVs.HP, level);
        }

        private void RecalculateAttack() {
            stats.Attack = Formulas.CalculateStat(PokemonData.BaseStats.Attack, IVs.Attack, EVs.Attack, level);
        }

        private void RecalculateDefense() {
            stats.Defense = Formulas.CalculateStat(PokemonData.BaseStats.Defense, IVs.Defense, EVs.Defense, level);
        }

        private void RecalculateSpAtk() {
            stats.SpAtk = Formulas.CalculateStat(PokemonData.BaseStats.SpAtk, IVs.SpAtk, EVs.SpAtk, level);
        }

        private void RecalculateSpDef() {
            stats.SpDef = Formulas.CalculateStat(PokemonData.BaseStats.SpDef, IVs.SpDef, EVs.SpDef, level);
        }

        private void RecalculateSpeed() {
            stats.Speed = Formulas.CalculateStat(PokemonData.BaseStats.Speed, IVs.Speed, EVs.Speed, level);
        }

        #endregion

        public bool IsKnockout() {
            return CurrentHealth == 0;
        }

        public void Damage(int damage)
        {
            if(damage > currentHealth)
            {
                currentHealth = 0;
            } else
            {
                currentHealth -= damage;
            }
        }

        public void Heal() {
            currentHealth = Stats.HP;
            foreach (var moveInfo in Moveset) {
                if(moveInfo != null) {
                    moveInfo.PowerPoints = moveInfo.MaxPowerPoints;
                }
            }
            status = Status.None;
        }

        public void Heal(int value) {
            currentHealth = value;
            if (currentHealth > Stats.HP) currentHealth = Stats.HP;
        }

        public void SetStatus(Status status) {
            if(status == Status.None || this.status == Status.None) {
                this.status = status;
                this.statusState = 0;
            }
        }

        public void RemoveStatus() {
            SetStatus(Status.None);
        }

    }

    [Serializable]
    public class MoveInfo {

        [SerializeField] private Move move;
        [SerializeField] private int powerPoints;
        [SerializeField] private int maxPowerPoints;
        [SerializeField] private int powerPointsUpgrades;

        public Move Move => move;
        public int MaxPowerPoints => maxPowerPoints;
        public bool NotNull => move != null;

        public int PowerPoints {
            get {
                return powerPoints; 
            }
            set {
                powerPoints = value;
                if(powerPoints < 0) powerPoints = 0;
                if(powerPoints > maxPowerPoints) powerPoints = maxPowerPoints;
            }
        }

        public int PowerPointsUpgrades {
            get {
                return powerPointsUpgrades;
            }
            set {
                powerPointsUpgrades = value;
                if(powerPointsUpgrades < 0) powerPointsUpgrades = 0;
                if(powerPointsUpgrades > 3) powerPointsUpgrades = 3;
                maxPowerPoints = move.PowerPoints + (int) (move.PowerPoints * 0.2f * powerPointsUpgrades); 
            }
        }
    }
}
