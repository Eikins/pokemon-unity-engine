﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace PokemonEngine.Data
{
    [CreateAssetMenu(menuName = "Pokemon Engine/Data/Pokemon", fileName = "New Pokemon", order = 0)]
    public class PokemonData : ScriptableObject
    {

        #region Fields

        [SerializeField] private string unlocalizedName;
        [SerializeField] private int nationalId;

        [SerializeField] private Type primaryType;
        [SerializeField] private Type secondaryType;

        [SerializeField] private Ability primaryAbility;
        [SerializeField] private Ability secondaryAbility;
        [SerializeField] private Ability hiddenAbility;

        [SerializeField] private float height;
        [SerializeField] private float weight;
        [SerializeField] private bool genderless;
        [SerializeField] [Range(0f, 1f)] private float femaleRatio;
        [SerializeField] [Range(0, 255)] private int catchRate;

        [SerializeField] private EggGroup primaryEggGroup;
        [SerializeField] private EggGroup secondaryEggGroup;
        [SerializeField] private int hatchCycles;

        [SerializeField] private int baseExp;
        [SerializeField] private LevelingRate levelingRate;
        [SerializeField] private int baseFriendship;

        [SerializeField] private Statistics baseStats;
        [SerializeField] private Statistics evYields;

        [SerializeField] private LearnSet learnSet;

        #endregion

        #region Fields Getters

        public string UnlocalizedName => "Pokemons." + unlocalizedName + ".Name";
        public string InternalName => unlocalizedName;
        public int NationalId => nationalId;

        public Type PrimaryType => primaryType;
        public Type SecondaryType => secondaryType;

        public Ability PrimaryAbility => primaryAbility;
        public Ability SecondaryAbility => secondaryAbility;
        public Ability HiddenAbility => hiddenAbility;

        public float Height => height;
        public float Weight => weight;
        public bool Genderless => genderless;
        public float FemaleRatio => femaleRatio;
        public int CatchRate => catchRate;

        public EggGroup PrimaryEggGroup => primaryEggGroup;
        public EggGroup SecondaryEggGroup => secondaryEggGroup;
        public int HatchCycles => hatchCycles;

        public int BaseExp => baseExp;
        public LevelingRate LevelingRate => levelingRate;
        public int BaseFriendship => baseFriendship;

        public Statistics BaseStats => baseStats;
        public Statistics EVYields => evYields;

        public LearnSet LearnSet => learnSet;
        #endregion

    }

    #region Learn Sets
    [Serializable]
    public class LearnSet
    {
        [SerializeField] private LevelLearnSet levelLearnSet;
        [SerializeField] private BasicLearnSet tmLearnSet;
        [SerializeField] private BasicLearnSet eggLearnSet;
        [SerializeField] private BasicLearnSet tutorLearnSet;
        [SerializeField] private BasicLearnSet dreamWorldLearnSet;
        [SerializeField] private BasicLearnSet eventLearnSet;

        public LevelLearnSet LevelLearnSet => levelLearnSet;
        public BasicLearnSet TMLearnSet => tmLearnSet;
        public BasicLearnSet EggLearnSet => eggLearnSet;
        public BasicLearnSet TutorLearnSet => tutorLearnSet;
        public BasicLearnSet DreamWorldLearnSet => dreamWorldLearnSet;
        public BasicLearnSet EventLearnSet => eventLearnSet;
    }

    [Serializable]
    public class LevelLearnSet
    {

        [Serializable]
        private class LLSEntry
        {
            public int level;
            public Move move;
        }

        [SerializeField] List<LLSEntry> moves;

        public Move[] GetMoves(int level)
        {
            List<Move> ret = new List<Move>();
            foreach (var entry in moves)
            {
                if (entry.level == level)
                {
                    ret.Add(entry.move);
                }
            }
            return ret.ToArray();
        }
    }

    [Serializable]
    public class BasicLearnSet
    {

        [SerializeField] List<Move> moves;

        public bool Contains(Move move)
        {
            return moves != null && moves.Contains(move);
        }

    }
    #endregion
}

