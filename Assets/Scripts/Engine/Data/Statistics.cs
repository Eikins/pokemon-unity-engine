﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace PokemonEngine.Data {

    [Serializable]
    public class Statistics
    {

        [SerializeField] private int hp;
        [SerializeField] private int attack;
        [SerializeField] private int defense;
        [SerializeField] private int spAtk;
        [SerializeField] private int spDef;
        [SerializeField] private int speed;

        public virtual int HP {
            get {
                return hp;
            }
            set {
                hp = value;
            }
        }
        public virtual int Attack {
            get {
                return attack;
            }
            set {
                attack = value;
            }
        }
        public virtual int Defense {
            get {
                return defense;
            }
            set {
                defense = value;
            }
        }
        public virtual int SpAtk {
            get {
                return spAtk;
            }
            set {
                spAtk = value;
            }
        }
        public virtual int SpDef {
            get {
                return spDef;
            }
            set {
                spDef = value;
            }
        }
        public virtual int Speed {
            get {
                return speed;
            }
            set {
                speed = value;
            }
        }

        public int GetTotal()
        {
            return HP + Attack + Defense + SpAtk + SpDef + Speed;
        }
    }
}
