using System;
using UnityEngine;
using PokemonEngine.BattleSystem;
using PokemonEngine.BattleSystem.Effects;
using PokemonEngine.Localization;

namespace PokemonEngine.Data {

    public enum Status {
        None,
        Burn,
        Freeze,
        Paralysis,
        Poison,
        Sleep,
        Toxicity
    }

    public static class StatusMethods {

        public static StatusEffect CreateBattleEffect(this Status status, int statusState) {
            switch (status) {
                case Status.Burn: return new BurnEffect();
                case Status.Freeze: return new FreezeEffect();
                case Status.Paralysis: return new ParalysisEffect(); 
                case Status.Poison: return new PoisonEffect();
                case Status.Sleep: return new SleepEffect();
                case Status.Toxicity: return new ToxicityEffect();
                default: return null;
            }
        }

        public static string GetName(this Status status) {
            return System.Enum.GetName(typeof(Status), status);
        }

        public static string GetUnlocalizedName(this Status status) {
            return "Status." + status.GetName() + ".Name";
        }

        public static string GetDisplayName(this Status status) {
            return I18n.Translate(status.GetUnlocalizedName());
        }
    }

}