﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PokemonEngine.Data {

    [CreateAssetMenu(menuName = "Pokemon Engine/Entity/Team", fileName = "New Team", order = 1)]
    public class Team : ScriptableObject
    {
     
        [SerializeField] private List<Pokemon> pokemons = new List<Pokemon>();

        public List<Pokemon> Pokemons => pokemons;
        public Pokemon this[int i] => pokemons[i];
        public int Count => pokemons.Count;

        public void Heal() {
            foreach (var pokemon in pokemons) {
                pokemon.Heal();
            }
        }

        public bool HasPokemonAlive() {
            foreach (var pokemon in pokemons) {
                if(!pokemon.IsKnockout()) {
                    return true;
                }
            }
            return false;
        }

        public void Swap(int i1, int i2) {
            if(i1 >= Count || i1 < 0 || i2 >= Count || i2 < 0) return;
            var tmp = pokemons[i1];
            pokemons[i1] = pokemons[i2];
            pokemons[i2] = tmp;
        }
    }

}

