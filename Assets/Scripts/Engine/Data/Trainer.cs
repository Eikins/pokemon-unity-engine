﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PokemonEngine.Localization;

namespace PokemonEngine.Data {

    [CreateAssetMenu(menuName = "Pokemon Engine/Entity/Trainer", fileName = "New Trainer", order = 2)]
    public class Trainer : ScriptableObject
    {
        
        [SerializeField] private string unlocalizedName;
        [SerializeField] private Team team;
        [SerializeField] private int reward;


        public string UnlocalizedName => "Trainers." + unlocalizedName + ".Name";
        public string DisplayName => I18n.Translate("Trainers." + unlocalizedName + ".Name");
        public Team Team => team;
        public int Reward => reward;

    }

}

