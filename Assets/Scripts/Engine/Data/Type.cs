﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PokemonEngine.Data {

    [CreateAssetMenu(menuName = "Pokemon Engine/Data/Type", fileName = "New Type", order = 4)]
    public class Type : ScriptableObject {

        [Serializable]
        public class TypeMultiplier
        {
            public Type target;
            public float multiplier;

            public TypeMultiplier(Type target, float multiplier)
            {
                this.target = target;
                this.multiplier = multiplier;
            }
        }

        [SerializeField] private string unlocalizedName;
        public string UnlocalizedName => "Types." + unlocalizedName + ".Name";
        public string InternalName => unlocalizedName;

        [SerializeField] private Color color = Color.white;
        public Color Color => color;

        [SerializeField] private Sprite icon;
        public Sprite Icon => icon;

        [SerializeField] private Sprite alternativeIcon;
        public Sprite AlternativeIcon => alternativeIcon;

    }
}

