using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PokemonEngine.Data {

    [CreateAssetMenu(menuName = "Pokemon Engine/Data/TypeTable", fileName = "New Type Table", order = 100)]
    public class TypeTable : ScriptableObject {

        // Hacky workaround for unity...
        [Serializable]
        private class TypeEfficienciesDict : SerializableDictionary<Type, float> { }

        [Serializable]
        private class TypeTableDict : SerializableDictionary<Type, TypeEfficienciesDict> { }

        [HideInInspector] [SerializeField] private TypeTableDict typeTable;

        public float Get(Type attacker, Type target) {
            if(attacker == null || target == null) return 1f;

            TypeEfficienciesDict efficiencies;
            if(typeTable.TryGetValue(attacker, out efficiencies)) {
                float multiplier;
                if(efficiencies.TryGetValue(target, out multiplier)) {
                    return multiplier;
                } else {
                    return 1f;
                }
            } else {
                return 1f;
            }
        }

        public void Set(Type attacker, Type target, float multiplier) {
            if(typeTable == null) {
                typeTable = new TypeTableDict();
            }

            multiplier = Mathf.Clamp(multiplier, 0f, 10f);

            if(attacker == null || target == null) return;

            TypeEfficienciesDict efficiencies;
            if(typeTable.TryGetValue(attacker, out efficiencies)) {
                efficiencies[target] = multiplier;
            } else {
                efficiencies = new TypeEfficienciesDict();
                efficiencies[target] = multiplier;
                typeTable[attacker] = efficiencies;
            }
        }
    }
}