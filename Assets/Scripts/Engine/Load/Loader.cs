using System.Threading.Tasks;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.AddressableAssets.ResourceLocators;
using UnityEngine.ResourceManagement.AsyncOperations;
using PokemonEngine.Data;
using Pokemon3D.Data;

namespace PokemonEngine.Load {

    public static class Loader {

        public async static Task LoadAssets<T>(string label, Dictionary<string, T> dictionary) {
            var locations = await Addressables.LoadResourceLocationsAsync(label).Task;
            foreach (var loc in locations) {
                var obj = await Addressables.LoadAssetAsync<T>(loc).Task;
                dictionary.Add(loc.PrimaryKey, obj);
            }
        }

    }   

}