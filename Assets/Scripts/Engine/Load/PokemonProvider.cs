using System.Collections.Generic;
using UnityEngine;
using PokemonEngine.Data;
using PokemonEngine.Load;
using Pokemon3D.Data;

namespace PokemonEngine.Provider {

    public class PokemonProvider : Provider {

        private static PokemonProvider s_Instance;

        public static PokemonProvider Instance => s_Instance;

        [RuntimeInitializeOnLoadMethod]
        public static void LoadPokemons() {
            s_Instance = new PokemonProvider();
            s_Instance.Initialize();
        }

        #region Static Shorteners
        public static PokemonRenderAsset PokemonRenderAsset(Pokemon pokemon) {
            return Instance.GetPokemonRenderAsset(pokemon);
        }
        #endregion

        private Dictionary<string, PokemonRenderAsset> pokemonPrefabs;

        private async void Initialize() {
            pokemonPrefabs = new Dictionary<string, PokemonRenderAsset>();
            await Loader.LoadAssets<PokemonRenderAsset>("Pokemons", pokemonPrefabs);
            InitDone();
        }

        public PokemonRenderAsset GetPokemonRenderAsset(Pokemon pokemon) {
            return pokemonPrefabs[pokemon.PokemonData.InternalName];
        }


    }

}