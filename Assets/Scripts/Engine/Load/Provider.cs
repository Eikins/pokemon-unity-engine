namespace PokemonEngine.Provider {

    public class Provider {

        private bool s_Initialized = false;

        public bool Ready
        {
            get { return s_Initialized; }
        }

        protected void InitDone()
        { 
            s_Initialized = true;
        }

    }
 
}