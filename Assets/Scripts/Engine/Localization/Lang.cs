﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PokemonEngine.Localization {

    public interface LocaleDatabase {

        string Translate(string key);

    }

}

