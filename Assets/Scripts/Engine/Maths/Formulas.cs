﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PokemonEngine.Maths {

public static class Formulas
{

        public static readonly float STAB_MULTIPLIER = 1.5f;

        public static int CalculateStat(int baseStat, int iv, int ev, int level) {
            // TODO : Add Nature
            float nature = 1;
            int stat = Mathf.FloorToInt((2 * baseStat + iv + Mathf.FloorToInt(ev / 4)) * level / 100);
            return Mathf.FloorToInt(nature * (5 + stat));
        }

        public static int CalculateHPs(int baseHP, int iv, int ev, int level) {
            int num = (2 * baseHP + iv + Mathf.FloorToInt(ev / 4)) * level;
            return level + 10 + Mathf.FloorToInt(num / 100);
        }

        public static int CalculateDefaultDamage(int power, int attack, int defense, int level) {
            float num = (2f * level / 5f + 2) * power * attack / defense;
            return (int) (num / 50 + 2);
        }

        public static int CalculateStatChange(int baseStat, int modifier) {
            if(modifier >= 0) {
                return (int) (baseStat * (1 + (float) modifier / 2));
            } else {
                return (int) (baseStat * 1 / (1 - (float) modifier / 2));
            }
        }

        public static float GetAccuracyValue(int modifier) {
            if(modifier >= 0) {
                return 1f + (float) modifier / 3;
            } else {
                return 1f / (1f + (float) modifier / 3f);
            }
        }

        public static float GetEvasionValue(int modifier) {
            if(modifier >= 0) {
                return 1f + (float) modifier / 3;
            } else {
                return 1f / (1f + (float) modifier / 3f);
            }
        }
}

}

