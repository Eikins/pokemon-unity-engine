﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Flash : MonoBehaviour
{
    public float flashSpeed;
    private Image image;
    private float timer;
    void Start()
    {
        image = GetComponent<Image>();
        timer = 0.7f;
    }

    // Update is called once per frame
    void Update()
    {
        if (timer > 0)
        {
            timer -= Time.deltaTime;
        }
        else
        {
            image.color = new Color(255, 255, 255, image.color.a - flashSpeed * Time.deltaTime);
        }

        if (image.color.a <= 0)
        {
            GameObject.Find("Master").GetComponent<Init>().ShowNewPokemon();
            Destroy(gameObject);
        }
    }
}