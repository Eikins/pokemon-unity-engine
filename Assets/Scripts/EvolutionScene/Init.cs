﻿using System.Collections;
using System.Collections.Generic;
using Pokemon3D.Data;
using PokemonEngine.Data;
using UnityEditor;
using UnityEngine;
using TMPro;
using Cinemachine;
using UnityEngine.Playables;

public class Init : MonoBehaviour
{
    //pokemon
    public string pokemon;
    public string nextPokemon;
    public float lightSpeed;
    private float elevation = 0;

    private GameObject instance;

    private int state = 0;

    //camera
    public CinemachineVirtualCamera cam;
    public Light light;
    public PlayableDirector timeline;
    public ParticleSystem bubbleCreator;
    public GameObject flash;

    //text
    public float textSpeed;
    public RectTransform textBox;
    private string lastName;
    private string newName;


    // Start is called before the first frame update
    void Start()
    {
        GameObject model = ((PokemonRenderAsset) Resources.Load("3D/Pokemons/" + pokemon)).Prefab;
        instance = Instantiate(model, Vector3.zero, new Quaternion(0, 195, 0, -35));
        lastName = model.name;
        StartCoroutine(ShowText("Quoi?\n" + lastName + " evolue !"));
        cam.Follow = instance.transform;
        light.gameObject.transform.position += getOffset();
        bubbleCreator.gameObject.transform.position += getOffset();
    }

    // Update is called once per frame
    void Update()
    {
        if (state == 1)
        {
            timeline.Play();
            StateChanger();
        }
        else if (state == 2)
        {
            light.intensity += lightSpeed * Time.deltaTime;
        }
        else if (state == 3)
        {
            flash.SetActive(true);
            light.intensity = 2;
            Destroy(instance);
            GameObject model = ((PokemonRenderAsset) Resources.Load("3D/Pokemons/" + nextPokemon)).Prefab;
            instance = Instantiate(model, Vector3.zero, new Quaternion(0, 195, 0, -35));
            newName = model.name;
            StateChanger();
        } else if (state == 5)
        {
            print("fin evolution");
        }
    }

    public IEnumerator ShowText(string text)
    {
        textBox.transform.gameObject.SetActive(true);
        TextMeshProUGUI textUI = textBox.Find("Text").GetComponent<TextMeshProUGUI>();

        for (int i = 0; i < text.Length; i++)
        {
            textUI.text = text.Substring(0, i + 1);
            yield return new WaitForSeconds(1f / textSpeed);
        }

        yield return new WaitForInput("Continue");
        textUI.text = "";
        textBox.transform.gameObject.SetActive(false);
        StateChanger();
    }

    private Vector3 getOffset()
    {
        return new Vector3(0, instance.transform.GetChild(0).GetComponent<SkinnedMeshRenderer>().bounds.extents.y / 2,
            0);
    }

    public void StateChanger()
    {
        state++;
    }

    public void stopBubbleCreator()
    {
        bubbleCreator.enableEmission = false;
    }

    public void ShowNewPokemon()
    {
        StartCoroutine(ShowText("Votre " + lastName + " a evolué en " + newName + " !"));
    }
}