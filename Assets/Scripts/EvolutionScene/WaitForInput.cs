using UnityEngine;

public class WaitForInput : CustomYieldInstruction
{

    private string buttonName;

    public override bool keepWaiting
    {
        get
        {
            return !Input.GetButtonDown(buttonName);
        }
    }

    public WaitForInput(string buttonName)
    {
        this.buttonName = buttonName;
    }
}