using UnityEditor;
using UnityEngine;
using System.Collections.Generic;
using System;
using PokemonEngine.Data;

public class SerializableDictionnaryDrawer<TK, TV> : PropertyDrawer
{

    private bool firstCheck = true;
    private bool foldout;
    private const float buttonWidth = 18f;

    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
    {
        return 17f * property.FindPropertyRelative("keys").arraySize + 17f;
    }

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        
        if(firstCheck) {
            foldout = EditorPrefs.GetBool(label.text);
            firstCheck = false;
        }

        EditorGUI.BeginProperty(position, label, property);
        
        position.height = 17f;
 
        SerializedProperty keyList = property.FindPropertyRelative("keys");
        SerializedProperty valueList = property.FindPropertyRelative("values");

        // Draw Foldout
        var foldoutRect = position;
        foldoutRect.width -= 2 * buttonWidth;
        EditorGUI.BeginChangeCheck();
        foldout = EditorGUI.Foldout(foldoutRect, foldout, label, true);
        if (EditorGUI.EndChangeCheck())
            EditorPrefs.SetBool(label.text, foldout);

        // Draw Add item Button
         
        var buttonRect = position;
        buttonRect.x = position.width - buttonWidth + position.x;
        buttonRect.width = buttonWidth + 2;

        if (GUI.Button(buttonRect, new GUIContent("+", "Add item"), EditorStyles.miniButton))
        {
            keyList.InsertArrayElementAtIndex(keyList.arraySize);
            valueList.InsertArrayElementAtIndex(valueList.arraySize);

            if(!DefaultValue(keyList.GetArrayElementAtIndex(keyList.arraySize - 1))) {
                Debug.Log("Type is not supported as key yet.");
                valueList.arraySize--;
                keyList.arraySize--;
            }

        }
 
        buttonRect.x -= buttonWidth;
 
         // Draw Clear Button
        if (GUI.Button(buttonRect, new GUIContent("C", "Clear dictionary"), EditorStyles.miniButtonRight))
        {
                keyList.arraySize = 0;
                valueList.arraySize = 0;
                return;
        }
 
        if (!foldout)
            return;

        int removeIndex = -1;

        for(int i = 0; i < keyList.arraySize; i++) {
            SerializedProperty key = keyList.GetArrayElementAtIndex(i);
            SerializedProperty value = valueList.GetArrayElementAtIndex(i);

             
            position.y += 17f;
            var keyRect = position;
            keyRect.width /= 2;
            keyRect.width -= 4;
            EditorGUI.PropertyField(keyRect, key, GUIContent.none);

            var valueRect = position;
            valueRect.x = position.width / 2 + 15;
            valueRect.width = keyRect.width - buttonWidth;
            var newValue = EditorGUI.PropertyField(valueRect, value, GUIContent.none);

            var removeRect = valueRect;
            removeRect.x = valueRect.xMax + 2;
            removeRect.width = buttonWidth;
            if (GUI.Button(removeRect, new GUIContent("x", "Remove item"), EditorStyles.miniButtonRight))
            {
                removeIndex = i;
                break;
            }
        }
        if(removeIndex != -1) {
            keyList.DeleteArrayElementAtIndex(removeIndex);
            valueList.DeleteArrayElementAtIndex(removeIndex);
        }

        EditorGUI.EndProperty();
    }

    private bool DefaultValue(SerializedProperty property) {
        switch (property.propertyType) {
            case SerializedPropertyType.Integer:
                property.intValue = 0;
                break;
            case SerializedPropertyType.Float:
                property.floatValue = 0f;
                break;
            case SerializedPropertyType.String:
                property.stringValue = "";
                break;
            case SerializedPropertyType.Vector2:
                property.vector2Value = Vector2.zero;
                break;    
            case SerializedPropertyType.Vector2Int:
                property.vector2IntValue = Vector2Int.zero;
                break;      
            case SerializedPropertyType.Vector3:
                property.vector2Value = Vector3.zero;
                break;      
            case SerializedPropertyType.Vector3Int:
                property.vector3IntValue = Vector3Int.zero;
                break;
            case SerializedPropertyType.Vector4:
                property.vector4Value = Vector4.zero;
                break;    
            case SerializedPropertyType.Character:
                property.stringValue = "";
                break;      
            case SerializedPropertyType.Enum:
                property.enumValueIndex = 0;
                break;     
            case SerializedPropertyType.Color:
                property.colorValue = Color.black;
                break;     
            case SerializedPropertyType.ObjectReference:
                property.objectReferenceValue = default(UnityEngine.Object);
                break;
            default:
                return false;
        }
        return true;
    }

}