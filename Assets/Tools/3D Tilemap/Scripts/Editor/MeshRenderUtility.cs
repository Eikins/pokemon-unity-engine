using UnityEngine;
using UnityEditor;

namespace Tilemap3D {

    public static class MeshRenderUtility {

        public static readonly Vector3 baseCameraPos = new Vector3(-6, 9, 6);

        public static Texture DrawMeshToTexture(Rect r, Mesh mesh, Material material) {
            return DrawMeshToTexture(r, mesh, material, baseCameraPos, Matrix4x4.identity);
        }

        public static Texture DrawMeshToTexture(Rect r, Mesh mesh, Material material, Vector3 cameraPos, Matrix4x4 meshTRS) {
            var prevRenderer = new PreviewRenderUtility();

            prevRenderer.camera.transform.position = cameraPos;
            prevRenderer.camera.transform.LookAt(Vector3.zero, Vector3.up);
            prevRenderer.camera.farClipPlane = 30;

            prevRenderer.lights[0].intensity = 0.5f;
            prevRenderer.lights[0].transform.rotation = Quaternion.Euler(30f, 30f, 0f);
            prevRenderer.lights[1].intensity = 0.5f;

            prevRenderer.BeginPreview(r, GUIStyle.none);

            var mat = new Material(material);
            mat.enableInstancing = true;

            prevRenderer.DrawMesh(mesh, meshTRS, mat, 0);

            bool fog = RenderSettings.fog;
            Unsupported.SetRenderSettingsUseFogNoDirty(false);
            prevRenderer.camera.Render();
            Unsupported.SetRenderSettingsUseFogNoDirty(fog);

            RenderTexture texture = prevRenderer.EndPreview() as RenderTexture;

            RenderTexture currentActiveRT = RenderTexture.active;
            RenderTexture.active = texture;
            var copy = new Texture2D(texture.width, texture.height);
            copy.ReadPixels(new Rect(0, 0, texture.width, texture.height), 0, 0);
            copy.Apply();
            RenderTexture.active = currentActiveRT;

            prevRenderer.Cleanup();
            return copy;
        }

    }

}