using UnityEngine;
using UnityEditor;

namespace Tilemap3D {

    public class BoxTool : PaletteTool {

        private int pickIndex;
        private bool isSelecting;

        private Vector3Int startPos;
        private Vector3Int endPos;

        public BoxTool(Tilemap3DEditorWindow palette) : base(palette) {}

        public override bool OnMouseDown(Tilemap3DLayer layer, Vector3Int tilePos, int paletteIndex, int rotation) {
                if(Palette.Tilemap.Asset.IsInside(tilePos)) {
                    switch(Event.current.button) {
                        case 0:
                            startPos = tilePos;
                            endPos = tilePos;
                            isSelecting = true;
                            break;
                    }
                }
                return false;
        }

        public override bool OnMouseDrag(Tilemap3DLayer layer, Vector3Int tilePos, int paletteIndex, int rotation) {
            switch(Event.current.button) {
                case 0:
                    if(Palette.Tilemap.Asset.IsInside(tilePos)) endPos = tilePos;
                    break;
            }
            return false;
        }

        public override bool OnMouseUp(Tilemap3DLayer layer, Vector3Int tilePos, int paletteIndex, int rotation) {
            switch(Event.current.button) {
                case 0:
                    int startX, startZ, endX, endZ;
                    if(startPos.x < endPos.x) {
                        startX = startPos.x;
                        endX = endPos.x;
                    } else {
                        startX = endPos.x;
                        endX = startPos.x;
                    }
                    if(startPos.z < endPos.z) {
                        startZ = startPos.z;
                        endZ = endPos.z;
                    } else {
                        startZ = endPos.z;
                        endZ = startPos.z;
                    }

                    for(int x = startX; x <= endX; x++) {
                        for(int z = startZ; z <= endZ; z++) {
                            if(Palette.Erase) {
                                layer.RemoveTile(new Vector3Int(x, startPos.y, z));
                            } else {
                                layer.SetTile(new Vector3Int(x, startPos.y, z), paletteIndex, rotation);
                            }

                        }
                    }
                    startPos = endPos;
                    isSelecting = false;
                    break;
            }
            return false;
        }

        public override void DisplayVisualHelp(Vector3Int tilePos)
        {
            // Get tile position (intersect with x0z plan)
            var tilemap = Palette.Tilemap;

            if(isSelecting) {
                DrawSelection(tilemap, tilePos);
            } else if (tilemap.Asset.IsInside(tilePos)) {
                Vector3 vertex0 = (Vector3) tilePos;
                Vector3 vertex1 = (tilePos + Vector3.forward);
                Vector3 vertex2 = (tilePos + Vector3.forward + Vector3.right);
                Vector3 vertex3 = (tilePos + Vector3.right);

                // Draw preview square
                if(Palette.Erase) {
                    Handles.color = Color.red;
                } else {
                    Handles.color = Color.green;
                }

                Vector3[] lines = { vertex0, vertex1, vertex1, vertex2, vertex2, vertex3, vertex3, vertex0 };
                Handles.DrawLines(lines);

                if(!Palette.Erase && Palette.PaletteIndex >= 0) {
                    PaletteTool.DrawMesh(Palette, tilePos, 1, 1);
                }
            }


        }

        private void DrawSelection(Tilemap3D tilemap, Vector3Int tilePos) {
            int startX, startZ, endX, endZ;
            if(startPos.x < endPos.x) {
                startX = startPos.x;
                endX = endPos.x;
            } else {
                startX = endPos.x;
                endX = startPos.x;
            }
            if(startPos.z < endPos.z) {
                startZ = startPos.z;
                endZ = endPos.z;
            } else {
                startZ = endPos.z;
                endZ = startPos.z;
            }

            Vector3 vertex0 = new Vector3(startX, startPos.y, startZ);
            Vector3 vertex1 = new Vector3(startX, startPos.y, endZ + 1);
            Vector3 vertex2 = new Vector3(endX + 1, startPos.y, endZ + 1);
            Vector3 vertex3 = new Vector3(endX + 1, startPos.y, startZ);

            // Draw preview square
            if(Palette.Erase) {
                Handles.color = Color.red;
            } else {
                Handles.color = Color.green;
            }

            Vector3[] lines = { vertex0, vertex1, vertex1, vertex2, vertex2, vertex3, vertex3, vertex0 };
            Handles.DrawLines(lines);

            if(!Palette.Erase && Palette.PaletteIndex >= 0) {
                PaletteTool.DrawMesh(Palette, new Vector3Int(startX, startPos.y, startZ), endX - startX + 1, endZ - startZ + 1);
            }
        }

    }

}