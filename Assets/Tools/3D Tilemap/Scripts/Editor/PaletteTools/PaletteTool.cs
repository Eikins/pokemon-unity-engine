using UnityEngine;
using UnityEditor;
using System.Collections.Generic;

namespace Tilemap3D {

    public abstract class PaletteTool {

        public Tilemap3DEditorWindow Palette {get;}

        public PaletteTool(Tilemap3DEditorWindow palette) {
            Palette = palette;
        }

        public abstract bool OnMouseDown(Tilemap3DLayer layer, Vector3Int tilePos, int paletteIndex, int rotation);

        public abstract bool OnMouseDrag(Tilemap3DLayer layer, Vector3Int tilePos, int paletteIndex, int rotation);

        public abstract bool OnMouseUp(Tilemap3DLayer layer, Vector3Int tilePos, int paletteIndex, int rotation);

        public abstract void DisplayVisualHelp(Vector3Int tilePos);

        public static void DrawMesh(Tilemap3DEditorWindow palette, Vector3Int tilePos, int width, int length) {
            var tilemap = palette.Tilemap;
            var pos = tilemap.transform.position + Vector3.Scale(tilePos + (Vector3.forward + Vector3.right) * 0.5f + Vector3.up * 0.01f, tilemap.transform.localScale);
            var rotation = Quaternion.AngleAxis(palette.Rotation * 90f, tilemap.transform.up);
            var mat = new Material(tilemap.GetLayerMaterial(palette.CurrentLayer));
            mat.enableInstancing = true;

            var tile = palette.CurrentLayer.tileset[palette.PaletteIndex];
            var mesh = Tilemap3DMeshUtility.Clone(tile.Mesh);
            var rect = tile.UV;
            Vector2[] uvs = mesh.uv;
            for (int i = 0; i < mesh.vertexCount; i++) {
                float u = uvs[i].x * rect.width + rect.position.x;
                float v = uvs[i].y * rect.height + rect.position.y;
                uvs[i] = new Vector2(u, v);
            }
            mesh.uv = uvs;

            for(int x = 0; x < width; x++) {
                for(int z = 0; z < length; z++) {
                    var matrix = Matrix4x4.TRS(pos + new Vector3(x, 0, z), rotation, tilemap.transform.localScale);
                    Graphics.DrawMeshInstanced(mesh, 0, mat, new List<Matrix4x4>() {matrix});
                }
            }

        }

    }

}


