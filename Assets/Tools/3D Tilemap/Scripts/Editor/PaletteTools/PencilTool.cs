using UnityEngine;
using UnityEditor;

namespace Tilemap3D {

    public class PencilTool : PaletteTool {

        private int pickIndex;

        public PencilTool(Tilemap3DEditorWindow palette) : base(palette) {}

        public override bool OnMouseDown(Tilemap3DLayer layer, Vector3Int tilePos, int paletteIndex, int rotation) {

                if(Palette.Tilemap.Asset.IsInside(tilePos)) {
                    switch(Event.current.button) {
                        case 0:
                            if(Palette.Erase) {
                                layer.RemoveTile(tilePos);
                            } else {
                                layer.SetTile(tilePos, paletteIndex, rotation);
                            }
                            break;
                        case 2:
                            MeshTileData meshTileData;
                            if(layer.tiles.TryGetValue(tilePos, out meshTileData)) {
                                pickIndex = meshTileData.index;                                
                            }
                            break;
                    }
                }
                return false;
        }

        public override bool OnMouseDrag(Tilemap3DLayer layer, Vector3Int tilePos, int paletteIndex, int rotation) {
            switch(Event.current.button) {
                case 0:
                    MeshTileData meshTileData;
                    if(layer.tiles.TryGetValue(tilePos, out meshTileData)) {
                        if(Palette.Erase) {
                            layer.RemoveTile(tilePos);
                        } else if(meshTileData.index != paletteIndex) {
                            layer.SetTile(tilePos, paletteIndex, rotation);
                        }
                    } else {
                        if(!Palette.Erase) {
                            layer.SetTile(tilePos, paletteIndex, rotation);
                        }
                    }
                    break;
                case 2:
                    pickIndex = -1;
                    break;
            }
            return false;
        }

        public override bool OnMouseUp(Tilemap3DLayer layer, Vector3Int tilePos, int paletteIndex, int rotation) {
            switch(Event.current.button) {
                case 2:
                    if(pickIndex != -1) {
                        Palette.PaletteIndex = pickIndex;
                        pickIndex = -1;
                        return true;
                    }
                    break;
            }
            return false;
        }

        public override void DisplayVisualHelp(Vector3Int tilePos)
        {
            // Get tile position (intersect with x0z plan)
            var tilemap = Palette.Tilemap;

            if(tilemap.Asset.IsInside(tilePos)) {
                Vector3 vertex0 = (Vector3) tilePos;
                Vector3 vertex1 = (tilePos + Vector3.forward);
                Vector3 vertex2 = (tilePos + Vector3.forward + Vector3.right);
                Vector3 vertex3 = (tilePos + Vector3.right);

                // Draw preview square
                if(Palette.Erase) {
                    Handles.color = Color.red;
                } else {
                    Handles.color = Color.green;
                }

                Vector3[] lines = { vertex0, vertex1, vertex1, vertex2, vertex2, vertex3, vertex3, vertex0 };
                Handles.DrawLines(lines);

                if(!Palette.Erase && Palette.PaletteIndex >= 0) {
                    PaletteTool.DrawMesh(Palette, tilePos, 1, 1);
                }
            }



        }

    }

}