using UnityEngine;
using UnityEditor;
using System.Collections.Generic;

namespace Tilemap3D {

    [CustomEditor(typeof(Tilemap3DAsset))]
    public class Tilemap3DAssetEditor : Editor {

        private Tilemap3DAsset asset;

        void OnEnable() {
            asset = target as Tilemap3DAsset;
        }

        private string layerName = "";
        private Tileset layerTileset;
        private float snapDistance = 0.001f;
        private float margin = 0.01f;
        private bool usePartitioning;

        public override void OnInspectorGUI() {
            DrawDefaultInspector();

            GUILayout.Label("Add Layer", EditorStyles.boldLabel);
            layerName = EditorGUILayout.TextField("Layer Name", layerName);
            layerTileset = EditorGUILayout.ObjectField("Layer Tileset", layerTileset, typeof(Tileset), false) as Tileset;
            if(GUILayout.Button("Create")) {
                asset.AddLayer(layerName, layerTileset);
                EditorUtility.SetDirty(asset);
            }
            GUILayout.FlexibleSpace();
            margin = EditorGUILayout.Slider("UV Remap Margin", margin, 0, 0.25f);
            if(GUILayout.Button("Remap UVs")) {
                asset.RemapUVs(margin);
            }

            GUILayout.FlexibleSpace();
            snapDistance = EditorGUILayout.Slider("Snap Distance", snapDistance, 0, 0.25f);
            usePartitioning = EditorGUILayout.Toggle("Use Partitionning", usePartitioning);
            if(GUILayout.Button("Snap near vertices")) {
                SnapVertices(snapDistance, usePartitioning);
            }

        }

        public void SnapVertices(float snapDistance, bool usePartitioning) {
            EditorUtility.DisplayCancelableProgressBar("Snaping vertices", "Snaping vertices to remove bleeding edges...", 0f);
            var snap = snapDistance * snapDistance;
            int count = 0;

            foreach (var layer in asset.layers) {
                var snappedVertices = new List<Vector3>(layer.mesh.vertices);
                var snappedNormals = new List<Vector3>(layer.mesh.normals);
                if(usePartitioning) {
                    var partition = SpacePartition(layer.mesh.vertices);
                    int cellProcess = 0;
                    foreach (var cell in partition.Values) {
                        if(cell.Count <= 1) continue;
                        for (int i = 0; i < cell.Count - 1; i++) {
                            for(int j = i + 1; j < cell.Count; j++) {
                                var firstVertexIndex = cell[i];
                                var secondVertexIndex = cell[j];
                                var firstVertex = snappedVertices[firstVertexIndex];
                                var secondVertex = layer.mesh.vertices[secondVertexIndex];
                                var firstNormal = snappedNormals[firstVertexIndex];
                                var secondNormal = layer.mesh.normals[firstVertexIndex];


                                if(firstVertex != secondVertex || firstNormal != secondNormal) {
                                    if ((firstVertex - secondVertex).sqrMagnitude < snap) {
                                        snappedVertices[firstVertexIndex] = secondVertex;
                                        snappedNormals[firstVertexIndex] = secondNormal;
                                        count++;
                                    }
                                } 
                            }
                        }
                        cellProcess++;
                        if(EditorUtility.DisplayCancelableProgressBar("Snaping vertices",
                            "Layer : " + layer.name + ", Cell Processed : " + cellProcess + "/" + partition.Count,
                            (float) cellProcess / partition.Count)) {
                            EditorUtility.ClearProgressBar();
                            return;
                        }
                    }
                } else {
                    for(int i = 0; i < layer.mesh.vertexCount - 1; i++) {
                        for(int j = i + 1; j < layer.mesh.vertexCount; j++) {
                            var firstVertex = snappedVertices[i];
                            var secondVertex = layer.mesh.vertices[j];
                            if(firstVertex != secondVertex && (firstVertex - secondVertex).sqrMagnitude < snap) {
                                snappedVertices[i] = secondVertex;
                                count++;
                            }
                        }
                        if(EditorUtility.DisplayCancelableProgressBar("Snaping vertices",
                            "Layer : " + layer.name + ", Vertice : " + i + "/" + layer.mesh.vertexCount,
                            (float) i / layer.mesh.vertexCount)) {
                            EditorUtility.ClearProgressBar();
                            return;
                        }
                    }
                }

                EditorUtility.ClearProgressBar();
                //layer.mesh.vertices = snappedVertices.ToArray();
            }
            Debug.Log("Snapped " + count + " vertices");
        }

        public Dictionary<Vector3Int, List<int>> SpacePartition(Vector3[] vertices) {
            var partition = new Dictionary<Vector3Int, List<int>>();
            for (int i = 0; i < vertices.Length; i++) {
                var offset = vertices[i] + Vector3.one * 0.5f;
                var gridPos = new Vector3Int(Mathf.RoundToInt(offset.x), Mathf.RoundToInt(offset.y), Mathf.RoundToInt(offset.z));
                List<int> cell;
                if(!partition.TryGetValue(gridPos, out cell)) {
                    cell = new List<int>();
                    partition.Add(gridPos, cell);
                }
                cell.Add(i);
            }
            return partition;
        }
            
    }

    

}