using UnityEngine;
using UnityEditor;
using System.IO;

namespace Tilemap3D {

    [CustomEditor(typeof(Tilemap3D))]
    public class Tilemap3DEditor : Editor {

        [MenuItem("Tools/Generate Mesh With Current Transform")]
        private static void TransformMesh()
        {
            var go = Selection.activeGameObject;
            if(go) {
                var filter = go.GetComponent<MeshFilter>();
                if(filter) {
                    var newMesh = new Mesh();
                    var mesh = filter.sharedMesh;
                    var trs = Matrix4x4.TRS(go.transform.localPosition, go.transform.localRotation, go.transform.localScale);

                    newMesh.name = mesh.name;
                    newMesh.vertices = mesh.vertices;
                    newMesh.triangles = mesh.triangles;
                    newMesh.uv = mesh.uv;
                    newMesh.normals = mesh.normals;
                    newMesh.colors = mesh.colors;
                    newMesh.tangents = mesh.tangents;

                    var vertices = new Vector3[newMesh.vertexCount];
                    for(int i = 0; i < vertices.Length; i++) {
                        vertices[i] = trs.MultiplyPoint3x4(newMesh.vertices[i]);
                    }
                    newMesh.vertices = vertices;
                    newMesh.RecalculateNormals();
                    newMesh.RecalculateTangents();
                    newMesh.RecalculateBounds();
                    AssetDatabase.CreateAsset(newMesh, "Assets/Tools/Output/Generated Meshes/" + newMesh.name  + ".asset");
                    AssetDatabase.SaveAssets();
                }
            }
        }

        [MenuItem("Tools/Remap UV for Pokemon Models (Cannot undo !!!)")]
        private static void RemapUVs()
        {
            var mesh = Selection.activeObject as Mesh;
            if(mesh) {
                var newUVs = new Vector2[mesh.vertexCount];
                for(int i = 0; i < mesh.vertexCount; i++) {
                    var uv = mesh.uv[i];
                    float u = Mathf.Abs(uv.x); 
                    float v = 1 - Mathf.Abs(uv.y);
                    newUVs[i] = new Vector2(u, v);
                }
                mesh.uv = newUVs;
                Debug.Log("Remapping UVs of " + mesh.name + " done !");
            }
        }

        [MenuItem("Tools/Convert to PNG")]
        private static void SaveAsPNG() {
            var tex = Selection.activeObject as Texture2D;
            if(tex) {
                byte[] bytes = tex.EncodeToPNG();
                var path = AssetDatabase.GetAssetPath(tex);
                File.WriteAllBytes(path.Split('.')[0] + ".png", bytes);
            }

 
        }

        private Tilemap3D tilemap;

        void OnEnable() {
            tilemap = target as Tilemap3D;
        }

        public override void OnInspectorGUI() {
            DrawDefaultInspector();

            if(GUILayout.Button("Refresh")) {
                tilemap.RefreshTilemap();
            }
        }
            
    }

}
