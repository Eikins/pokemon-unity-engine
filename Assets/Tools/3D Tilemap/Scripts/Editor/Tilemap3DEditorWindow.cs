﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace Tilemap3D {

    public class Tilemap3DEditorWindow : EditorWindow
    {

        [MenuItem("GameObject/Tilemap 3D", false, 11)]
        private static void CreateTilemap() {
            var go = new GameObject("Tilemap 3D");
            if(Selection.activeGameObject) {
                go.transform.parent = Selection.activeGameObject.transform;
                go.transform.localPosition = Vector3.zero;
                go.transform.localRotation = Quaternion.identity;
                go.transform.localScale = Vector3.one;
            }
            var tilemap = go.AddComponent<Tilemap3D>();

        }

        [MenuItem("Window/Tilemap 3D")]
        private static void ShowWindow()
        {
            EditorWindow.GetWindow<Tilemap3DEditorWindow>("Tilemap3D Palette");
        }

        private int paletteIndex;
        private int height;
        private int rotation;

        private Vector2 paletteScroll = Vector2.zero;

        private PreviewRenderUtility prevRenderer;

        private Tilemap3D tilemap;

        private List<GUIContent> paletteIcons = new List<GUIContent>();
        private List<string> layerNames = new List<string>();

        private GUIContent[] toolIcons;

        private GUIContent gridIcon;
        private GUIContent rotateIcon;
        private GUIContent heightIncreaseIcon, heightDecreaseIcon;
        private GUIContent eraseIcon;

        private PaletteTool[] tools;

        private float paletteIconSize = 100f;

        private int pickIndex;
        private int tool;
        private bool showGrid;
        private bool erase;
        private int layer;
        private string searchFilter;

        public Tilemap3DLayer CurrentLayer => tilemap.Asset[layer];
        private PaletteTool CurrentPaintTool => tools[tool - 1];
        public int Rotation => rotation;
        public Tilemap3D Tilemap => tilemap;

        public int CurrentTool {
            get {
                return tool;
            }
            set {
                tool = value % toolIcons.Length;
            }
        }

        public bool Erase => Event.current.control || erase;

        public int PaletteIndex {
            get {
                return paletteIndex;
            }
            set {
                if (value >= paletteIcons.Count) paletteIndex = paletteIcons.Count - 1;
                else paletteIndex = value;
            }
        }

       public int LayerIndex {
            get {
                return layer;
            }
            set {
                if(value < 0 || tilemap == null) layer = 0;
                else if (value >= tilemap.Asset.LayerCount) layer = tilemap.Asset.LayerCount - 1;
                else layer = value;
            }
        }

        private string SearchFilter {
            get {
                return searchFilter;
            }
            set {
                var flag = searchFilter != value;
                searchFilter = value;
                if(flag) {
                    RefreshPalette();
                }
            }
        }

        void OnEnable() {
            toolIcons = new GUIContent[] {
                new GUIContent(EditorGUIUtility.IconContent("Grid.Default").image, "Selection"),
                new GUIContent(EditorGUIUtility.IconContent("Grid.PaintTool").image, "Paint"),
                new GUIContent(EditorGUIUtility.IconContent("Grid.BoxTool").image, "Square"),
                //new GUIContent(EditorGUIUtility.IconContent("Grid.FillTool").image, "Fill"),
            };

            tools = new PaletteTool[] {
                new PencilTool(this),
                new BoxTool(this)
            };

            gridIcon = new GUIContent(EditorGUIUtility.IconContent("WireframeBehaviour Icon").image, "Show/Hide grid");
            eraseIcon = new GUIContent(EditorGUIUtility.IconContent("Grid.EraserTool").image, "Erase [Hold Ctrl]");
            rotateIcon = new GUIContent(EditorGUIUtility.IconContent("RotateTool").image, "Rotate tile [R]");
            heightIncreaseIcon = new GUIContent(EditorGUIUtility.IconContent("d_TerrainInspector.TerrainToolRaise On").image, "Increase height [+]");
            heightDecreaseIcon = new GUIContent(EditorGUIUtility.IconContent("d_TerrainInspector.TerrainToolLower On").image, "Decrease height [-]");
            var skin = EditorGUIUtility.GetBuiltinSkin(EditorSkin.Inspector);
            showGrid = true;

            OnSelectionChange();
        }

        void OnDisable() {
            Tools.hidden = false;
            if(prevRenderer != null) {
                prevRenderer.Cleanup();
            }
        }


        void OnSelectionChange()
        {
            var go = Selection.activeGameObject;
            if(go) {
                tilemap = go.GetComponent<Tilemap3D>();
                if(!tilemap && go.transform.parent != null) {
                    tilemap = go.transform.parent.GetComponent<Tilemap3D>();
                }
                RefreshLayers();
            } else {
                tilemap = null;
            }
            Tools.hidden = tilemap != null;
            Repaint();
        }

        private void RepaintScene() {
            SceneView.RepaintAll();
        }

        private void OnGUI() {
            HandleKeyboardInputs();
            if(tilemap && tilemap.Asset) {

                GUILayout.BeginVertical();
                GUILayout.BeginHorizontal();
                GUILayout.FlexibleSpace();
                CurrentTool = GUILayout.Toolbar(CurrentTool, toolIcons, GUILayout.Height(30f));
                GUILayout.Space(10f);
                if(GUILayout.Button(rotateIcon, GUILayout.Width(30f), GUILayout.Height(30f))) {
                    rotation = (rotation + 1) % 4;
                }
                if(GUILayout.Button(heightIncreaseIcon, GUILayout.Width(30f), GUILayout.Height(30f))) {
                    height++;
                    RepaintScene();
                }
                if(GUILayout.Button(heightDecreaseIcon, GUILayout.Width(30f), GUILayout.Height(30f))) {
                    height--;
                    RepaintScene();
                }
                GUILayout.Space(10f);
                if(GUILayout.Toggle(erase, eraseIcon, "Button", GUILayout.Height(30f), GUILayout.Width(30f)) != erase) {
                    erase = !erase;
                    RepaintScene();
                }
                if(GUILayout.Toggle(showGrid, gridIcon, "Button", GUILayout.Height(30f), GUILayout.Width(30f)) != showGrid) {
                    showGrid = !showGrid;
                    RepaintScene();
                }
                GUILayout.FlexibleSpace();
                GUILayout.EndHorizontal();
                

                GUILayout.BeginHorizontal(EditorStyles.toolbar);
                int lastLayerIndex = LayerIndex;
                LayerIndex = EditorGUILayout.Popup(LayerIndex, layerNames.ToArray(), EditorStyles.toolbarPopup, GUILayout.Height(30f));
                if(lastLayerIndex != LayerIndex) RefreshPalette();
                /*if(GUILayout.Button("+", EditorStyles.toolbarButton)) {
                    Selection.activeObject = tilemap.Asset;
                }*/
                GUILayout.FlexibleSpace();
                SearchFilter = GUILayout.TextField(SearchFilter, EditorStyles.toolbarSearchField, GUILayout.Width(150f));
                if(GUILayout.Button("", "ToolbarSeachCancelButton")) {
                    SearchFilter = "";
                }
                GUILayout.EndHorizontal();

                GUILayout.EndVertical();

                paletteScroll = EditorGUILayout.BeginScrollView(paletteScroll, false, true);
                var width = position.width - 40;
                var style = new GUIStyle((GUIStyle) "Button");
                style.fixedHeight = paletteIconSize;
                style.fixedWidth = paletteIconSize;
                paletteIndex = GUILayout.SelectionGrid(paletteIndex, paletteIcons.ToArray(), Mathf.FloorToInt(width / paletteIconSize), style, GUILayout.MaxWidth(width));
                EditorGUILayout.EndScrollView();
            } else {
                GUILayout.Label(tilemap ? "Assign a Tilemap Asset to this tilemap." : "Select a Tilemap");
            }

        }

        private void OnSceneGUI(SceneView sceneView) {
            if(tilemap && tilemap.Asset) {
                HandleKeyboardInputs();
                if(showGrid) DrawWireGrid();

                if (CurrentTool != 0) {

                    Vector3Int tilePos = GetTilePos();
                    CurrentPaintTool.DisplayVisualHelp(tilePos);

                    HandleSceneViewInputs(tilePos);
                    sceneView.Repaint();
                }
            }

        }

        private void HandleKeyboardInputs() {
            int controlID = GUIUtility.GetControlID(FocusType.Keyboard);

            if (Event.current.GetTypeForControl(controlID) == EventType.KeyDown) {

                switch(Event.current.keyCode){
                case KeyCode.KeypadPlus:
                case KeyCode.Plus:
                    height++;
                    Event.current.Use();
                    break;
                case KeyCode.KeypadMinus:
                case KeyCode.Minus:
                    height--;
                    Event.current.Use();
                    break;
                case KeyCode.R:
                    rotation = (rotation + 1) % 4;
                    Event.current.Use();
                    break;
                case KeyCode.Tab:
                    CurrentTool++;
                    Event.current.Use();
                    RepaintScene();
                    Repaint();
                    break;
                case KeyCode.Escape:
                    CurrentTool = 0;
                    Event.current.Use();
                    RepaintScene();
                    Repaint();
                    break;
                }
            }
        }

        private bool IsInside(Vector3 pos) {
            return pos.x >= 0 && pos.x <= tilemap.Width - 1 && pos.z >= 0 && pos.z <= tilemap.Length - 1;
        }


        private Vector3Int GetTilePos() {
            Ray guiRay = HandleUtility.GUIPointToWorldRay(Event.current.mousePosition);
            Vector3 mousePosition = guiRay.origin - guiRay.direction * ((guiRay.origin.y - height) / guiRay.direction.y);
            mousePosition -= tilemap.transform.position;

            Vector3Int tilePosInt = new Vector3Int(Mathf.FloorToInt(mousePosition.x / tilemap.transform.localScale.x), height, Mathf.FloorToInt(mousePosition.z / tilemap.transform.localScale.z));
            return tilePosInt;
        }

        private void HandleSceneViewInputs(Vector3Int tilePos) {

            // Filter the left click so that we can't select objects in the scene

            if (Event.current.type == EventType.Layout) {
                HandleUtility.AddDefaultControl(0); // Consume the event
            }

            if(paletteIcons.Count > 0 && paletteIndex >= 0) {
                switch (Event.current.type) {
                    case EventType.MouseDown:
                        if(CurrentPaintTool.OnMouseDown(CurrentLayer, tilePos, paletteIndex, rotation)) Event.current.Use();
                        break;
                    case EventType.MouseDrag:
                        if(CurrentPaintTool.OnMouseDrag(CurrentLayer, tilePos, paletteIndex, rotation)) Event.current.Use();
                        break;
                    case EventType.MouseUp:
                        if(CurrentPaintTool.OnMouseUp(CurrentLayer, tilePos, paletteIndex, rotation)) Event.current.Use();
                        break;
                }
            }

        }

        void OnFocus()
        {
            SceneView.duringSceneGui -= this.OnSceneGUI;
            SceneView.duringSceneGui += this.OnSceneGUI;
            RefreshLayers();
        }

        private void RefreshPalette() {
            paletteIcons.Clear();
            if(tilemap && tilemap.Asset) {
            var search = string.IsNullOrWhiteSpace(SearchFilter);
                for (int i = 0; i < CurrentLayer.tileset.Count; i++) {
                    var tileData = CurrentLayer.tileset.TileDatas[i];
                    if(search || (tileData.name != null && tileData.name.IndexOf(SearchFilter, System.StringComparison.OrdinalIgnoreCase) >= 0)) {
                        var texture = DrawMeshToTexture(new Rect(0, 0, paletteIconSize, paletteIconSize), CurrentLayer.tileset[i]);
                        paletteIcons.Add(new GUIContent(texture));   
                    }
                }
            }
            if(paletteIndex >= paletteIcons.Count) paletteIndex = -1;
        }

        private void RefreshLayers() {
            layerNames.Clear();
            if(tilemap && tilemap.Asset) {
                for (int i = 0; i < tilemap.Asset.LayerCount; i++) {
                    layerNames.Add(tilemap.Asset[i].name);   
                }
                if(LayerIndex >= layerNames.Count) LayerIndex = 0;
            }
            RefreshPalette();
        }

        void OnDestroy()
        {
            SceneView.duringSceneGui -= this.OnSceneGUI;
        }

        private void DrawWireGrid() {
            Handles.matrix = tilemap.transform.localToWorldMatrix;
            Handles.color = Color.white;
            var from = new Vector3(0, height, 0);
            var to = new Vector3(0, height, tilemap.Length);
            Handles.DrawLine(from, to); 
            from = new Vector3(tilemap.Width, height, 0);
            to = new Vector3(tilemap.Width, height, tilemap.Length);
            Handles.DrawLine(from, to); 

            from = new Vector3(0, height, 0);
            to = new Vector3(tilemap.Width, height, 0); 
            Handles.DrawLine(from, to); 
            from = new Vector3(0, height, tilemap.Length);
            to = new Vector3(tilemap.Width, height, tilemap.Length);
            Handles.DrawLine(from, to); 

            Handles.color = Color.gray;
            for (int x = 1; x < tilemap.Width; x++)
            {
                from = new Vector3(x, height, 0);
                to = new Vector3(x, height, tilemap.Length);
                Handles.DrawLine(from, to); 
            }

            for (int y = 1; y < tilemap.Length; y++)
            {
                from = new Vector3(0, height, y);
                to = new Vector3(tilemap.Width, height, y);
                Handles.DrawLine(from, to); 
            }
        }


        Texture DrawMeshToTexture(Rect r, Tileset.Tile tile) {
            if (prevRenderer == null) prevRenderer = new PreviewRenderUtility();

            prevRenderer.camera.transform.position = new Vector3(-6, 9, 6);
            prevRenderer.camera.transform.LookAt(Vector3.zero, Vector3.up);
            prevRenderer.camera.farClipPlane = 30;

            prevRenderer.lights[0].intensity = 0.5f;
            prevRenderer.lights[0].transform.rotation = Quaternion.Euler(30f, 30f, 0f);
            prevRenderer.lights[1].intensity = 0.5f;

            prevRenderer.BeginPreview(r, GUIStyle.none);

            var pos = Vector3.zero;
            var rotation = Quaternion.identity; 
            var matrix = Matrix4x4.TRS(pos, rotation, Vector3.one);
            var mat = new Material(tilemap.GetLayerMaterial(CurrentLayer));
            mat.enableInstancing = true;

            var mesh = Tilemap3DMeshUtility.Clone(tile.Mesh);
            var rect = tile.UV;
            Vector2[] uvs = mesh.uv;
            for (int i = 0; i < mesh.vertexCount; i++) {
                float u = uvs[i].x * rect.width + rect.position.x;
                float v = uvs[i].y * rect.height + rect.position.y;
                uvs[i] = new Vector2(u, v);
            }
            mesh.uv = uvs;
            prevRenderer.DrawMesh(mesh, matrix, mat, 0);

            bool fog = RenderSettings.fog;
            Unsupported.SetRenderSettingsUseFogNoDirty(false);
            prevRenderer.camera.Render();
            Unsupported.SetRenderSettingsUseFogNoDirty(fog);

            RenderTexture texture = prevRenderer.EndPreview() as RenderTexture;

            RenderTexture currentActiveRT = RenderTexture.active;
            RenderTexture.active = texture;
            var copy = new Texture2D(texture.width, texture.height);
            copy.ReadPixels(new Rect(0, 0, texture.width, texture.height), 0, 0);
            copy.Apply();
            RenderTexture.active = currentActiveRT;
            return copy;
        }

    }
}