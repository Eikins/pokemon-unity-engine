using UnityEngine;
using UnityEditor; 

namespace Tilemap3D {

    [CustomEditor(typeof(Tileset))]
    public class TilesetEditor : Editor {

        private Tileset tileset;
        private int selectedMenu = 0;

        private TilesetMenu[] menus;
        private GUIContent[] menuButtons;

        public TileMenu TileMenu => menus[0] as TileMenu;
        public TextureMenu TextureMenu => menus[1] as TextureMenu;
        public MeshMenu MeshMenu => menus[2] as MeshMenu;
        public OptionMenu OptionMenu => menus[3] as OptionMenu;

        void OnEnable() {
            tileset = target as Tileset;

            tileset.Fix();
            menus = new TilesetMenu[]{
                new TileMenu(this),
                new TextureMenu(this),
                new MeshMenu(this),
                new OptionMenu(this),
            };

            menuButtons = new GUIContent[]{
                new GUIContent("Tiles", EditorGUIUtility.IconContent("d_Prefab Icon").image),
                new GUIContent("Textures", EditorGUIUtility.IconContent("Texture Icon").image),
                new GUIContent("Meshes", EditorGUIUtility.IconContent("GameObject Icon").image),
                new GUIContent("Options", EditorGUIUtility.IconContent("ClothInspector.SettingsTool").image)
            };
        }

        void OnDisable() {
            menus[selectedMenu].OnMenuExit();
        }

        public override bool UseDefaultMargins() {
            return false;
        }

        public override void OnInspectorGUI() {
            
            GUILayout.BeginVertical(EditorStyles.inspectorFullWidthMargins);

            /* Navigation bar */ 
            GUILayout.BeginHorizontal(EditorStyles.toolbar);
            int newSelectedMenu = GUILayout.Toolbar(selectedMenu, menuButtons, EditorStyles.toolbarButton);
            if(newSelectedMenu != selectedMenu) {
                menus[selectedMenu].OnMenuExit();
                menus[newSelectedMenu].OnMenuEnter();
                selectedMenu = newSelectedMenu;
            }
            GUILayout.EndHorizontal();

            menus[selectedMenu].OnGUI();

            GUILayout.EndVertical();
        }
        
    }
    
}