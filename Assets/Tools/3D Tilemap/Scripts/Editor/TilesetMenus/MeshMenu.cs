using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Tilemap3D {

    public class MeshMenu : TilesetMenu {

        private static readonly Material RenderMaterial = new Material(Shader.Find("Lightweight Render Pipeline/Lit"));
        private const float maxZoom = 2f;

        private int selection;
        private Vector2 scroll = Vector2.zero;

        /* Add Menu */
        private Mesh meshToAdd;

        /* Mesh Preview */
        private float rotation;
        private float zoom;

        private List<string> meshNames;
        public ReadOnlyCollection<string> MeshNames => meshNames.AsReadOnly();

        private Texture currentMeshTexture;

        private GUIStyle listStyle, previewStyle;

        private int SelectedMesh {
            get {
                return selection;
            }
            set {
                var flag = selection != value;
                selection = value;
                if(flag) RefreshMeshPreview();
            }
        }

        private float Rotation {
            get {
                return rotation;
            }
            set {
                var flag = rotation != value;
                rotation = value;
                if(flag) {
                    RefreshMeshPreview();
                }
            }
        }

        private float Zoom {
            get {
                return zoom;
            }
            set {
                var flag = zoom != value;
                zoom = value;
                if(flag) {
                    RefreshMeshPreview();
                }
            }
        }

        public MeshMenu(TilesetEditor editor) : base(editor) {
            meshNames = new List<string>();
            for (int i = 0; i < Tileset.Meshes.Count; i++) {
                meshNames.Add(Tileset.Meshes[i].name);
            }
 
            var skin = EditorGUIUtility.GetBuiltinSkin(EditorSkin.Inspector);
            listStyle = new GUIStyle(skin.GetStyle("GridList"));
            listStyle.alignment = TextAnchor.MiddleLeft;
            previewStyle = new GUIStyle(skin.GetStyle("Box"));
            previewStyle.alignment = TextAnchor.MiddleCenter;
            SelectedMesh = -1;
            rotation = 180f;
            zoom = 0.5f;
        }

        public override void OnGUI() {

            GUILayout.BeginHorizontal();
            /* Mesh Selectable List */
            GUI.backgroundColor = Color.grey;
            GUILayout.BeginVertical("Box");
            GUI.backgroundColor = Color.white;
            scroll = EditorGUILayout.BeginScrollView(scroll, false, false);
            SelectedMesh = GUILayout.SelectionGrid(SelectedMesh, meshNames.ToArray(), 1, listStyle);
            EditorGUILayout.EndScrollView();
            GUILayout.EndVertical();

            /* Side Menus */
            GUILayout.BeginVertical(GUILayout.Width(262f));

            /* Add Mesh Menu */

            GUILayout.BeginVertical("Box");


            meshToAdd = EditorGUILayout.ObjectField(meshToAdd, typeof(Mesh), true) as Mesh;
            if(GUILayout.Button("Add") && meshToAdd && !meshNames.Contains(meshToAdd.name)) {
                Tileset.AddMesh(meshToAdd);
                meshNames.Add(meshToAdd.name);
                meshToAdd = null;
                SelectedMesh = meshNames.Count - 1;
                EditorUtility.SetDirty(Tileset);
            }
            GUILayout.EndVertical();
            
            /* Preview */
            GUILayout.BeginVertical("Box");
            GUILayout.Label("Preview", EditorStyles.boldLabel);
            if(currentMeshTexture) {
                GUILayout.Box(new GUIContent(currentMeshTexture), previewStyle);
            } else {
                GUILayout.Box(new GUIContent("Select a Mesh"), previewStyle, GUILayout.Width(262), GUILayout.Height(262));
            }


            GUILayout.BeginVertical();
            GUILayout.BeginHorizontal();
            GUILayout.Label("Rotation", GUILayout.MaxWidth(262f / 3));
            Rotation = GUILayout.HorizontalSlider(Rotation, 0f, 360f);
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            GUILayout.Label("Zoom", GUILayout.MaxWidth(262f / 3));
            Zoom = GUILayout.HorizontalSlider(Zoom, 0f, 1f);
            GUILayout.EndHorizontal();

            if(GUILayout.Button("Remove") && SelectedMesh >= 0) {
                Tileset.RemoveMesh(SelectedMesh);
                meshNames.RemoveAt(SelectedMesh);
                if(SelectedMesh == meshNames.Count) SelectedMesh--;
                EditorUtility.SetDirty(Tileset);
            }
            GUILayout.EndVertical();

            GUILayout.EndVertical();

            GUILayout.EndVertical();
            GUILayout.EndHorizontal();
        }

        public void RefreshMeshPreview() {
            if(SelectedMesh == -1) {
                currentMeshTexture = null;
            } else {
                var cameraPos = MeshRenderUtility.baseCameraPos;
                cameraPos = Quaternion.AngleAxis(rotation + 180f, Vector3.up) * cameraPos;
                float zoomFactor;
                if(zoom <= 0.5f) {
                    zoomFactor = maxZoom - 2 * (maxZoom - 1) * zoom;
                } else {
                    zoomFactor = 1 - 2 * (1 - 1 / maxZoom) * (zoom - 0.5f);
                }
                cameraPos *= zoomFactor;
                currentMeshTexture = MeshRenderUtility.DrawMeshToTexture(new Rect(0, 0, 128, 128), Tileset.Meshes[SelectedMesh], RenderMaterial, cameraPos, Matrix4x4.identity);
            }
        }

    }

}


