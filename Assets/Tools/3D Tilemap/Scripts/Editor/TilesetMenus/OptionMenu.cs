using UnityEngine;
using UnityEditor;
using System.Collections.Generic;

namespace Tilemap3D {

    public class OptionMenu : TilesetMenu {

        public OptionMenu(TilesetEditor editor) : base(editor) {}

        public override void OnGUI() {
            GUILayout.BeginVertical(EditorStyles.inspectorDefaultMargins);
            EditorGUILayout.PropertyField(Editor.serializedObject.FindProperty("materialAtlasMapping"), true);
            if(GUILayout.Button("Reset Material")) {
                Tileset.ResetMaterial();
            }
            GUILayout.EndVertical();
            Editor.serializedObject.ApplyModifiedProperties();
        }
    }

}


