using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Tilemap3D {

    public class TextureMenu : TilesetMenu {

        private int selection;
        private Vector2 scroll = Vector2.zero;
        private Texture2D currentTexture;
        private bool hasModifications;

        /* Add Menu */
        private Texture2D textureToAdd;

        private List<string> textureNames;

        public ReadOnlyCollection<string> TextureNames => textureNames.AsReadOnly();

        private GUIStyle listStyle, textureStyle;
        private Texture2D boxBackground;

        private int SelectedTexture {
            get {
                return selection;
            }
            set {
                var flag = selection != value;
                selection = value;
                if(flag) {
                    currentTexture = selection >= 0 ? Tileset.Textures[selection] : null;
                    if(currentTexture) {
                        textureStyle.normal.background = currentTexture;
                        textureStyle.border = new RectOffset(0, 0, 0, 0);
                    } else {
                        textureStyle.normal.background = boxBackground;
                        textureStyle.border = new RectOffset(3, 3, 2, 2);
                    }
                }
            }
        }

        public TextureMenu(TilesetEditor editor) : base(editor) {
            textureNames = new List<string>();
            for (int i = 0; i < Tileset.Textures.Count; i++) {
                textureNames.Add(Tileset.Textures[i].name);
            }
 
            var skin = EditorGUIUtility.GetBuiltinSkin(EditorSkin.Inspector);
            listStyle = new GUIStyle(skin.GetStyle("GridList"));
            listStyle.alignment = TextAnchor.MiddleLeft;
            textureStyle = new GUIStyle(skin.GetStyle("Box"));
            textureStyle.alignment = TextAnchor.MiddleCenter;
            boxBackground = textureStyle.normal.background;
            SelectedTexture = -1;
        }

        public override void OnMenuEnter() {
            hasModifications = false;
        }

        public override void OnMenuExit() {
            if (hasModifications) Tileset.GenerateAtlas();
        }

        public override void OnGUI() {

            GUILayout.BeginHorizontal();
            /* Texture Selectable List */
            GUI.backgroundColor = Color.grey;
            GUILayout.BeginVertical("Box");
            GUI.backgroundColor = Color.white;
            scroll = EditorGUILayout.BeginScrollView(scroll, false, false);
            SelectedTexture = GUILayout.SelectionGrid(SelectedTexture, textureNames.ToArray(), 1, listStyle);
            EditorGUILayout.EndScrollView();
            GUILayout.EndVertical();

            /* Side Menus */
            GUILayout.BeginVertical(GUILayout.Width(262f));

            /* Add Texture Menu */

            GUILayout.BeginVertical("Box");


            textureToAdd = EditorGUILayout.ObjectField(textureToAdd, typeof(Texture2D), true) as Texture2D;
            if(GUILayout.Button("Add") && textureToAdd && !textureNames.Contains(textureToAdd.name)) {
                Tileset.AddTexture(textureToAdd);
                textureNames.Add(textureToAdd.name);
                textureToAdd = null;
                SelectedTexture = textureNames.Count - 1;
                EditorUtility.SetDirty(Tileset);
                hasModifications = true;
            }
            GUILayout.EndVertical();
            
            /* Preview */
            GUILayout.BeginVertical("Box");
            GUILayout.Label("Preview", EditorStyles.boldLabel);
            GUILayout.Box(currentTexture ? "" : "Select a Texture", textureStyle, GUILayout.Width(262), GUILayout.Height(262));

            GUILayout.BeginVertical();
            if(GUILayout.Button("Remove") && SelectedTexture >= 0) {
                Tileset.RemoveTexture(SelectedTexture);
                textureNames.RemoveAt(SelectedTexture);
                if(SelectedTexture == textureNames.Count) SelectedTexture--;
                EditorUtility.SetDirty(Tileset);
                hasModifications = true;
            }
            GUILayout.EndVertical();

            GUILayout.EndVertical();

            GUILayout.EndVertical();
            GUILayout.EndHorizontal();
        }

    }

}


