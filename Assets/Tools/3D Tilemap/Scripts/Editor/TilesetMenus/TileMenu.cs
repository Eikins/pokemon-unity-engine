using UnityEngine;
using UnityEditor;
using System.Collections.Generic;

namespace Tilemap3D {

    public class TileMenu : TilesetMenu {

        private const float maxZoom = 2f;
        private const float tileButtonSize = 64f;

        private int selection;
        private Vector2 scroll = Vector2.zero;

        /* Search field */
        private string searchFilter;

        /* Mesh Preview */
        private float rotation;
        private float zoom;
        private Texture previewTexture;

        private List<GUIContent> tileIcons;
        private List<int> tileIndexes;

        private GUIStyle searchStyle, previewStyle;

        private int SelectedTile {
            get {
                return selection;
            }
            set {
                var flag = selection != value;
                selection = value;
                if(flag) {
                    if(selection == tileIcons.Count - 1) {
                        Tileset.TileDatas.Add(new Tileset.TileObjectData(){name = searchFilter, textureIndex = 0, meshIndex = 0});
                        RefreshTiles();
                        EditorUtility.SetDirty(Tileset);
                    }
                    RefreshMeshPreview();
                }
            }
        }

        private string SearchFilter {
            get {
                return searchFilter;
            }
            set {
                var flag = searchFilter != value;
                searchFilter = value;
                if(flag) {
                    SelectedTile = -1;
                    RefreshTiles();
                }
            }
        }

        private float Rotation {
            get {
                return rotation;
            }
            set {
                var flag = rotation != value;
                rotation = value;
                if(flag) {
                    RefreshMeshPreview();
                }
            }
        }

        private float Zoom {
            get {
                return zoom;
            }
            set {
                var flag = zoom != value;
                zoom = value;
                if(flag) {
                    RefreshMeshPreview();
                }
            }
        }

        public TileMenu(TilesetEditor editor) : base(editor) {
            tileIcons = new List<GUIContent>();
            tileIndexes = new List<int>();
            RefreshTiles();
            var skin = EditorGUIUtility.GetBuiltinSkin(EditorSkin.Inspector);
            searchStyle = skin.GetStyle("ToolbarSeachCancelButton");
            previewStyle = new GUIStyle(skin.GetStyle("Box"));
            previewStyle.alignment = TextAnchor.MiddleCenter;
            SelectedTile = -1;
            rotation = 180f;
            zoom = 0.5f;
        }

        public override void OnGUI() {

            GUILayout.BeginHorizontal();

            /* Mesh Selectable List */
            GUI.backgroundColor = Color.grey;
            GUILayout.BeginVertical("Box");
            GUI.backgroundColor = Color.white;

            GUILayout.BeginHorizontal(EditorStyles.toolbar);
            
            SearchFilter = GUILayout.TextField(SearchFilter, EditorStyles.toolbarSearchField);
            if(GUILayout.Button("", searchStyle)) {
                SearchFilter = "";
            }
            GUILayout.EndHorizontal();

            scroll = EditorGUILayout.BeginScrollView(scroll, false, false);

            var width = EditorGUIUtility.currentViewWidth - 320f;
            var style = new GUIStyle((GUIStyle) "Button");
            style.fixedHeight = tileButtonSize;
            style.fixedWidth = tileButtonSize;
            SelectedTile = GUILayout.SelectionGrid(SelectedTile, tileIcons.ToArray(), Mathf.FloorToInt(width / tileButtonSize), style);
            EditorGUILayout.EndScrollView();
            GUILayout.EndVertical();

            /* Side Menus */
            GUILayout.BeginVertical(GUILayout.Width(262f));

            /* Preview */
            GUILayout.BeginVertical("Box");

            if(SelectedTile >= 0 && SelectedTile < tileIcons.Count - 1) {
                var tileData = Tileset.TileDatas[tileIndexes[SelectedTile]];
                Tileset.TileObjectData newTileData = new Tileset.TileObjectData();

                string[] textureNames = new string[Editor.TextureMenu.TextureNames.Count];
                string[] meshNames = new string[Editor.MeshMenu.MeshNames.Count];

                Editor.TextureMenu.TextureNames.CopyTo(textureNames, 0);
                Editor.MeshMenu.MeshNames.CopyTo(meshNames, 0);

                GUILayout.Label("Name", EditorStyles.boldLabel);
                newTileData.name = GUILayout.TextField(tileData.name);

                GUILayout.Label("Mesh", EditorStyles.boldLabel);
                newTileData.meshIndex = EditorGUILayout.Popup(tileData.meshIndex, meshNames);

                GUILayout.Label("Texture", EditorStyles.boldLabel);
                newTileData.textureIndex = EditorGUILayout.Popup(tileData.textureIndex, textureNames);

                if(!newTileData.Equals(tileData)) {
                    Tileset.TileDatas[tileIndexes[SelectedTile]] = newTileData;
                    EditorUtility.SetDirty(Tileset);
                    if(newTileData.textureIndex != tileData.textureIndex || newTileData.meshIndex != tileData.meshIndex) {
                        RefreshTiles();
                        RefreshMeshPreview();
                    }
                }
            }



            GUILayout.Label("Preview", EditorStyles.boldLabel);
            if(previewTexture) {
                GUILayout.Box(new GUIContent(previewTexture), previewStyle);
            } else {
                GUILayout.Box(new GUIContent("Select a Tile"), previewStyle, GUILayout.Width(262), GUILayout.Height(262));
            }


            GUILayout.BeginVertical();
            GUILayout.BeginHorizontal();
            GUILayout.Label("Rotation", GUILayout.MaxWidth(262f / 3));
            Rotation = GUILayout.HorizontalSlider(Rotation, 0f, 360f);
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            GUILayout.Label("Zoom", GUILayout.MaxWidth(262f / 3));
            Zoom = GUILayout.HorizontalSlider(Zoom, 0f, 1f);
            GUILayout.EndHorizontal();

            if(GUILayout.Button("Remove") && SelectedTile >= 0) {
                Tileset.TileDatas.RemoveAt(tileIndexes[SelectedTile]);
                tileIcons.RemoveAt(SelectedTile);
                if(SelectedTile >= tileIcons.Count - 1) SelectedTile = tileIndexes[tileIcons.Count - 2]; // Handle [+] button
                EditorUtility.SetDirty(Tileset);
            }
            GUILayout.EndVertical();

            GUILayout.EndVertical();

            GUILayout.EndVertical();
            GUILayout.EndHorizontal();
        }

        public void RefreshTiles() {
            tileIcons.Clear();
            tileIndexes.Clear();
            var search = string.IsNullOrWhiteSpace(SearchFilter);
            for (int i = 0; i < Tileset.Count; i++) {
                var tileData = Tileset.TileDatas[i];
                if(search || (tileData.name != null && tileData.name.IndexOf(SearchFilter, System.StringComparison.OrdinalIgnoreCase) >= 0)) {
                    var tile = Tileset[i];
                    var mesh = Tilemap3DMeshUtility.Clone(tile.Mesh);
                    var rect = tile.UV;
                    Vector2[] uvs = mesh.uv;
                    for (int j = 0; j < mesh.vertexCount; j++) {
                        float u = uvs[j].x * rect.width + rect.position.x;
                        float v = uvs[j].y * rect.height + rect.position.y;
                        uvs[j] = new Vector2(u, v);
                    }
                    mesh.uv = uvs;
                    var texture = MeshRenderUtility.DrawMeshToTexture(new Rect(0, 0, 100f, 100f), mesh, Tileset.Material);
                    tileIcons.Add(new GUIContent(texture));
                    tileIndexes.Add(i);
                }

            }
            tileIcons.Add(EditorGUIUtility.IconContent("Toolbar Plus"));
            if(SelectedTile >= tileIcons.Count - 1) SelectedTile = 0;
        }

        public void RefreshMeshPreview() {
            if(SelectedTile == -1) {
                previewTexture = null;
            } else {
                var cameraPos = MeshRenderUtility.baseCameraPos;
                cameraPos = Quaternion.AngleAxis(rotation + 180f, Vector3.up) * cameraPos;
                float zoomFactor;
                if(zoom <= 0.5f) {
                    zoomFactor = maxZoom - 2 * (maxZoom - 1) * zoom;
                } else {
                    zoomFactor = 1 - 2 * (1 - 1 / maxZoom) * (zoom - 0.5f);
                }
                cameraPos *= zoomFactor;
                var tile = Tileset[tileIndexes[SelectedTile]];
                var mesh = Tilemap3DMeshUtility.Clone(tile.Mesh);
                var rect = tile.UV;
                Vector2[] uvs = mesh.uv;
                for (int j = 0; j < mesh.vertexCount; j++) {
                    float u = uvs[j].x * rect.width + rect.position.x;
                    float v = uvs[j].y * rect.height + rect.position.y;
                    uvs[j] = new Vector2(u, v);
                }
                mesh.uv = uvs;
                previewTexture = MeshRenderUtility.DrawMeshToTexture(new Rect(0, 0, 128, 128), mesh, Tileset.Material, cameraPos, Matrix4x4.identity);
            }
        }

    }

}


