using UnityEngine;
using UnityEditor;
using System.Collections.Generic;

namespace Tilemap3D {

    public abstract class TilesetMenu {

        public Tileset Tileset {get;}
        public TilesetEditor Editor {get;}

        public TilesetMenu(TilesetEditor editor) {
            Editor = editor;
            Tileset = editor.target as Tileset;
        }

        public virtual void OnGUI() {}

        public virtual void OnMenuEnter() {}

        public virtual void OnMenuExit() {}
    }

}


