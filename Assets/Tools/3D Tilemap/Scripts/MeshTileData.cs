using System;

namespace Tilemap3D {

    [Serializable]
    public class MeshTileData {

        public int vertexStart, vertexCount;
        public int triangleStart, triangleCount;
        public int normalStart;
        public int uvStart;
        public int index;

    }

}