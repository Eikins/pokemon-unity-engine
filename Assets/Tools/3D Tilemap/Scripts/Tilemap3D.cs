﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace Tilemap3D {

    [AddComponentMenu("Tilemap3D")]
    [ExecuteInEditMode]
    public class Tilemap3D : MonoBehaviour
    {

        public Tilemap3DAsset tilemapAsset;

        private Mesh mesh;

        public int Width => tilemapAsset.width;
        public int Length => tilemapAsset.length;

        public Tilemap3DAsset Asset => tilemapAsset;

        void Start() {

            if(tilemapAsset != null) {
                RefreshTilemap();
            }

        }

        public void Create(Tilemap3DAsset asset) {
            tilemapAsset = asset;
        }

        public void RefreshTilemap() {
            foreach(var layer in tilemapAsset.layers) {
                var child = transform.Find(layer.name);
                GameObject go;
                if(!child) {
                    go = new GameObject(layer.name);
                    go.transform.parent = transform;
                    go.transform.localPosition = Vector3.zero;
                    go.transform.localRotation = Quaternion.identity;
                    go.transform.localScale = Vector3.one;
                    go.AddComponent<MeshRenderer>();
                    go.AddComponent<MeshFilter>();
                } else {
                    go = child.gameObject;
                }
                go.GetComponent<MeshRenderer>().sharedMaterial = layer.tileset.Material;
                go.GetComponent<MeshFilter>().mesh = layer.mesh;

            }
        }

        public Material GetLayerMaterial(Tilemap3DLayer layer) {
            var child = transform.Find(layer.name);
            if(child) {
                var renderer = child.GetComponent<Renderer>();
                if(renderer) return renderer.sharedMaterial;
            }
            return null;
        }



    }
}

