﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if (UNITY_EDITOR)
using UnityEditor;
#endif


namespace Tilemap3D {

    [CreateAssetMenu(menuName = "Tilemap 3D/Tilemap Asset", fileName = "New Tilemap 3D")]
    public class Tilemap3DAsset : ScriptableObject
    {
        
        public int width = 16;
        public int length = 16;

        public List<Tilemap3DLayer> layers = new List<Tilemap3DLayer>();

        public Tilemap3DLayer this[int index] => layers[index];
        public int LayerCount => layers.Count;

        public void AddLayer(string name, Tileset tileset) {
            // Check layer doesnt exist and tileset is not null
            if(tileset == null) return;
            foreach (var l in layers) {
                if(name == l.name) return;
            }
            // Add the layer
            var layer = new Tilemap3DLayer();
            layer.name = name;
            layer.tileset = tileset;
            layer.tiles = new TileDictionary();
            layer.mesh = new Mesh();
            layer.mesh.name = name + " Mesh";
            #if (UNITY_EDITOR)
                AssetDatabase.AddObjectToAsset(layer.mesh, this);
                AssetDatabase.ImportAsset(AssetDatabase.GetAssetPath(this));
            #endif
            layers.Add(layer);
        }

        public void RemapUVs(float margin) {
            float max = 1 - margin;
            foreach (var layer in layers) {
                Vector2[] uvs = layer.mesh.uv;
                foreach (var meshData in layer.tiles.Values) {
                    var tile = layer.tileset[meshData.index];
                    var rect = tile.UV;
                    var meshUVs = tile.Mesh.uv;

                    for (int i = 0; i < meshData.vertexCount; i++) {

                        // Avoid bleeding edge on borders
                        float fixedU = Mathf.Clamp(meshUVs[i].x, margin, max);
                        float fixedV = Mathf.Clamp(meshUVs[i].y, margin, max);

                        float u = fixedU * rect.width + rect.position.x;
                        float v = fixedV * rect.height + rect.position.y;

                        uvs[i + meshData.uvStart] = new Vector2(u, v);
                    }
                }
                layer.mesh.uv = uvs;
            }

        }

        public bool IsInside(Vector3 pos) {
            return pos.x >= 0 && pos.x <= width - 1 && pos.z >= 0 && pos.z <= length - 1;
        }

        public void SetTile(int layerIndex, Vector3Int pos, int tileIndex, int rotation) {
            layers[layerIndex].SetTile(pos, tileIndex, rotation);
        }

        public void RemoveTile(int layerIndex, Vector3Int pos) {
            layers[layerIndex].RemoveTile(pos);
        }

    }
    
}