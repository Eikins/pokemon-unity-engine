using UnityEngine;

namespace Tilemap3D {

    [System.Serializable]
    public class TileDictionary : SerializableDictionary<Vector3Int, MeshTileData> {}

    [System.Serializable]
    public struct Tilemap3DLayer {
        public string name;
        public Tileset tileset;
        public TileDictionary tiles;
        public Mesh mesh;


        public void SetTile(Vector3Int pos, int tileIndex, int rot) {
            RemoveTile(pos);

            var tile = tileset[tileIndex];
            var position = pos + new Vector3(0.5f, 0, 0.5f);
            var rotation = Quaternion.AngleAxis(90f * rot, Vector3.up);
            var matrix = Matrix4x4.TRS(position, rotation, Vector3.one);
            MeshTileData meshTileData;
            Tilemap3DMeshUtility.Merge(mesh, tile.Mesh, matrix, out meshTileData);
            meshTileData.index = tileIndex;
            var rect = tile.UV;
            Vector2[] uvs = mesh.uv;
            for (int i = meshTileData.uvStart; i < meshTileData.uvStart + meshTileData.vertexCount; i++) {
                float u = uvs[i].x * rect.width + rect.position.x;
                float v = uvs[i].y * rect.height + rect.position.y;

                // Avoid bleeding edge on borders, tested 0.0001f => still have edges
                if(uvs[i].x == 0f) u += 0.005f * rect.width;
                if(uvs[i].y == 0f) v += 0.005f * rect.height;
                if(uvs[i].x == 1f) u -= 0.005f * rect.width;
                if(uvs[i].y == 1f) v -= 0.005f * rect.height;

                uvs[i] = new Vector2(u, v);
            }
            mesh.uv = uvs;
            tiles.Add(pos, meshTileData);
        }

        public void RemoveTile(Vector3Int pos) {
            MeshTileData data;
            if(tiles.TryGetValue(pos, out data)) {
                Tilemap3DMeshUtility.Remove(mesh, data);
                tiles.Remove(pos);
                foreach (var tileData in tiles.Values) {
                    if(tileData.vertexStart > data.vertexStart) {
                        tileData.vertexStart -= data.vertexCount;
                        tileData.normalStart -= data.vertexCount;
                        tileData.uvStart -= data.vertexCount;
                    }
                    if(tileData.triangleStart > data.triangleStart) {
                        tileData.triangleStart -= data.triangleCount;
                    }
                }
            }
        }
    }

}