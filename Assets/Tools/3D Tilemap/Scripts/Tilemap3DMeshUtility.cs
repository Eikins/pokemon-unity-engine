﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Tilemap3D {

    public static class Tilemap3DMeshUtility
    {

        public static readonly float epsilon = 0.001f;

        public static Mesh Merge(Mesh mesh, Mesh other, Matrix4x4 trsMatrix, float epsilon) {
            var vertices = new List<Vector3>();
            var triangles = new List<int>();
            var normals = new List<Vector3>();

            var uvChannels = new List<Vector2>[8];
            
            vertices.AddRange(mesh.vertices);
            triangles.AddRange(mesh.triangles);
            normals.AddRange(mesh.normals);

            for(int i = 0; i < other.vertexCount; i++) {
                var vertex = trsMatrix.MultiplyPoint3x4(other.vertices[i]);

                // Fix bleeding edges
                var roundedX = Mathf.RoundToInt(vertex.x);
                var roundedY = Mathf.RoundToInt(vertex.y);
                var roundedZ = Mathf.RoundToInt(vertex.z);
                if(Mathf.Abs(vertex.x - roundedX) < epsilon) {
                    vertex.x = roundedX;
                }
                if(Mathf.Abs(vertex.y - roundedY) < epsilon) {
                    vertex.y = roundedY;
                }
                if(Mathf.Abs(vertex.z - roundedZ) < epsilon) {
                    vertex.z = roundedZ;
                }
                vertices.Add(vertex);
            }

            for(int i = 0; i < other.normals.Length; i++) {
                var normal = trsMatrix.MultiplyVector(other.normals[i]);

                normals.Add(normal);
            }

            for(int i = 0; i < other.triangles.Length; i++) {
                var triangle = other.triangles[i] + mesh.vertexCount;

                triangles.Add(triangle);
            }



        for(int i = 0; i < 8; i++) {
                uvChannels[i] = new List<Vector2>();
                mesh.GetUVs(i, uvChannels[i]);
                var otherUvs = new List<Vector2>();
                other.GetUVs(i, otherUvs);
                uvChannels[i].AddRange(otherUvs);
            }

            mesh.SetVertices(vertices);
            mesh.SetTriangles(triangles, 0);
            mesh.SetNormals(normals);
            for(int i = 0; i < 8; i++) {
                mesh.SetUVs(i, uvChannels[i]);
            }

            return mesh;
        }

        public static Mesh Merge(Mesh mesh, Mesh other, Matrix4x4 trsMatrix, out MeshTileData meshTileData) {
            MeshTileData meshTile = new MeshTileData();
            meshTile.vertexStart = mesh.vertices.Length;
            meshTile.triangleStart = mesh.triangles.Length;
            meshTile.normalStart = mesh.normals.Length;
            meshTile.uvStart = mesh.uv.Length;
            var merged = Merge(mesh, other, trsMatrix, epsilon);
            meshTile.vertexCount = mesh.vertices.Length - meshTile.vertexStart;
            meshTile.triangleCount = mesh.triangles.Length - meshTile.triangleStart;
            meshTileData = meshTile;
            return merged;
        }

        public static Mesh Remove(Mesh mesh, MeshTileData meshTileData) {
            var vertices = new List<Vector3>(mesh.vertices);
            var triangles = new List<int>(mesh.triangles);
            var normals = new List<Vector3>(mesh.normals);
            var uvs = new List<Vector2>(mesh.uv);

            vertices.RemoveRange(meshTileData.vertexStart, meshTileData.vertexCount);
            triangles.RemoveRange(meshTileData.triangleStart, meshTileData.triangleCount);
            normals.RemoveRange(meshTileData.normalStart, meshTileData.vertexCount);
            uvs.RemoveRange(meshTileData.uvStart, meshTileData.vertexCount);

            for(int i = meshTileData.triangleStart; i < triangles.Count; i++) {
                triangles[i] -= meshTileData.vertexCount;
            }

            mesh.triangles = triangles.ToArray();
            mesh.vertices = vertices.ToArray();
            mesh.normals = normals.ToArray();
            mesh.uv = uvs.ToArray();
            return mesh;
        }

        public static Mesh Clone(Mesh mesh) {
            var newMesh = new Mesh();

            newMesh.name = mesh.name;
            newMesh.vertices = mesh.vertices;
            newMesh.triangles = mesh.triangles;
            newMesh.uv = mesh.uv;
            newMesh.normals = mesh.normals;
            newMesh.colors = mesh.colors;
            newMesh.tangents = mesh.tangents;

            return newMesh;
        }

    }
}