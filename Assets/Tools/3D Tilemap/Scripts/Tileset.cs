using System.Collections.ObjectModel;
using System.Collections.Generic;
using UnityEngine;

#if (UNITY_EDITOR)
using UnityEditor;
#endif

namespace Tilemap3D {

    [CreateAssetMenu(menuName = "Tilemap 3D/Tileset", fileName = "New Tileset")]
    public class Tileset : ScriptableObject
    {

        [System.Serializable]
        public struct TileObjectData {
            public string name;
            public int meshIndex, textureIndex;
        }

        public class Tile {
            public Mesh Mesh {get;}
            public Rect UV {get;}

            public Tile(Mesh mesh, Rect uv) {
                Mesh = mesh;
                UV = uv;
            }
        }
        
        [SerializeField] private List<Mesh> meshes = new List<Mesh>();
        [SerializeField] private List<Texture2D> textures = new List<Texture2D>();
        [SerializeField] private List<TileObjectData> tileDatas = new List<TileObjectData>();

        public ReadOnlyCollection<Mesh> Meshes => meshes.AsReadOnly();
        public ReadOnlyCollection<Texture2D> Textures => textures.AsReadOnly();
        public List<TileObjectData> TileDatas => tileDatas;

        [SerializeField] private Texture2D textureAtlas;
        [SerializeField] private Material material;
        [SerializeField] private Rect[] uvMap;
        [SerializeField] private string[] materialAtlasMapping;

        [SerializeField] private Shader shader;

        public Texture2D TextureAtlas => textureAtlas;
        public Material Material => material;
        public ReadOnlyCollection<Rect> UVMap => System.Array.AsReadOnly<Rect>(uvMap);

        public int Count => tileDatas.Count;

        public Tile this[int index] {
            get {
                if(index >= tileDatas.Count || index < 0) return null;
                var tileData = tileDatas[index];
                return new Tile(meshes[tileData.meshIndex], uvMap[tileData.textureIndex]);
            }
        }

        public void ResetMaterial() {
                material = new Material(Shader.Find("Lightweight Render Pipeline/Unlit"));
                material.name = "Material";
                material.SetTexture("_BaseMap", textureAtlas);
                material.SetColor("_BaseColor", Color.white);
                if(materialAtlasMapping == null || materialAtlasMapping.Length == 0) {
                    materialAtlasMapping = new string[1];
                    materialAtlasMapping[0] = "_BaseMap";
                } else {
                    foreach (var remap in materialAtlasMapping) {
                        material.SetTexture(remap, textureAtlas);
                    }
                }
                #if (UNITY_EDITOR)
                var path = AssetDatabase.GetAssetPath(this);
                Object[] assets = UnityEditor.AssetDatabase.LoadAllAssetsAtPath(path);
                for (int i = 0; i < assets.Length; i++)
                {
                    Object asset = assets[i];
                    if (!UnityEditor.AssetDatabase.IsMainAsset(asset) && asset.name == "Material" && asset is Material) Object.DestroyImmediate(asset, true);
                }

                AssetDatabase.AddObjectToAsset(material, this);
                AssetDatabase.ImportAsset(path);
                #endif
        }

        public void GenerateAtlas() {
            textureAtlas = new Texture2D(2048, 2048, TextureFormat.ARGB32, false, false);
            textureAtlas.filterMode = FilterMode.Point;
            textureAtlas.alphaIsTransparency = true;
            textureAtlas.name = "Atlas";
            uvMap = textureAtlas.PackTextures(textures.ToArray(), 4);
            PaddingBleed.BleedEdges(textureAtlas, 4, uvMap, false);

            #if (UNITY_EDITOR)
            var path = AssetDatabase.GetAssetPath(this);
            Object[] assets = UnityEditor.AssetDatabase.LoadAllAssetsAtPath(path);
            for (int i = 0; i < assets.Length; i++)
            {
                Object asset = assets[i];
                if (!UnityEditor.AssetDatabase.IsMainAsset(asset) && asset.name == "Atlas" && asset is Texture2D) Object.DestroyImmediate(asset, true);
            }

            AssetDatabase.AddObjectToAsset(textureAtlas, this);

            if(material == null) {
                material = new Material(Shader.Find("Lightweight Render Pipeline/Unlit"));
                material.name = "Material";
                material.SetTexture("_BaseMap", textureAtlas);
                material.SetColor("_BaseColor", Color.white);
                if(materialAtlasMapping == null || materialAtlasMapping.Length == 0) {
                    materialAtlasMapping = new string[1];
                    materialAtlasMapping[0] = "_BaseMap";
                } else {
                    foreach (var remap in materialAtlasMapping) {
                        material.SetTexture(remap, textureAtlas);
                    }
                }
                AssetDatabase.AddObjectToAsset(material, this);
            } else {
                foreach (var remap in materialAtlasMapping) {
                    material.SetTexture(remap, textureAtlas);
                }
            }

            AssetDatabase.ImportAsset(path);
            #endif
        }

        public void AddMesh(Mesh mesh) {
            var meshToAdd = Tilemap3DMeshUtility.Clone(mesh);
            meshes.Add(meshToAdd);
            #if (UNITY_EDITOR)
            AssetDatabase.AddObjectToAsset(meshToAdd, this);
            AssetDatabase.ImportAsset(AssetDatabase.GetAssetPath(this));
            #endif
        }

        public void RemoveMesh(int index) {
            var mesh = meshes[index];
            meshes.RemoveAt(index);
            tileDatas.RemoveAll((tile) => tile.meshIndex == index);
            #if (UNITY_EDITOR)
            var path = AssetDatabase.GetAssetPath(this);
            Object[] assets = UnityEditor.AssetDatabase.LoadAllAssetsAtPath(path);
            for (int i = 0; i < assets.Length; i++)
            {
                Object asset = assets[i];
                if (!UnityEditor.AssetDatabase.IsMainAsset(asset) && asset.name == mesh.name && asset is Mesh) {
                    Object.DestroyImmediate(asset, true);
                    return;
                }
            }
            AssetDatabase.ImportAsset(AssetDatabase.GetAssetPath(this));
            #endif
        }

        public void AddTexture(Texture2D texture) {
            var textureToAdd = Object.Instantiate(texture) as Texture2D;
            textureToAdd.name = texture.name;
            textures.Add(textureToAdd);
            #if (UNITY_EDITOR)
            AssetDatabase.AddObjectToAsset(textureToAdd, this);
            AssetDatabase.SaveAssets();
            AssetDatabase.ImportAsset(AssetDatabase.GetAssetPath(this));
            #endif
        }

        public void RemoveTexture(int index) {
            var texture = textures[index];
            textures.RemoveAt(index);
            tileDatas.RemoveAll((tile) => tile.textureIndex == index);
            #if (UNITY_EDITOR)
            var path = AssetDatabase.GetAssetPath(this);
            Object[] assets = UnityEditor.AssetDatabase.LoadAllAssetsAtPath(path);
            for (int i = 0; i < assets.Length; i++)
            {
                Object asset = assets[i];
                if (!UnityEditor.AssetDatabase.IsMainAsset(asset) && asset.name == texture.name && asset is Texture2D) {
                    Object.DestroyImmediate(asset, true);
                    return;
                }
            }
            AssetDatabase.SaveAssets();
            AssetDatabase.ImportAsset(AssetDatabase.GetAssetPath(this));
            #endif
        }

        public void Fix() {
            tileDatas.RemoveAll((t) => t.meshIndex >= meshes.Count);
        }

    }

}
